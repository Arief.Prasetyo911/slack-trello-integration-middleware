<?php
error_reporting(0);

//For Slack Notification
    //fix
    function SendNotification_Arief($cardname, $cardURL){
        $webhookURL_Arief   = 'https://hooks.slack.com/services/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';
        //data for notification
        $channel            = '@arief prasetyo';
        $bot_name           = 'Kaigan Taskmaster';
        $icon               = ':alien:';
        $attachments        = array([
            'color'     => '#36a64f',        
            'text'      => "*<@U62TFQL1Y>, you have new card [$cardname] on Trello*\n_You'll get the notification every one hour until you remove the 'NEW' label_\n$cardURL",
            'mrkdwn'    =>  true
        ]);

        $data = array(
            'channel'     => $channel,
            'username'    => $bot_name,
            'icon_emoji'  => $icon,
            'attachments' => $attachments
        );

        $data_string = json_encode($data);

        $ch = curl_init($webhookURL_Arief);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string))
        );
        //Execute CURL
        $result = curl_exec($ch);
        curl_close($ch);
    }

    //fix
    function SendNotification_Azmi($cardname, $cardURL){
        $webhookURL_Azmi    = 'https://hooks.slack.com/services/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';
        //data for notification
        $channel            = '@imzahahs';
        $bot_name           = 'Kaigan Taskmaster';
        $icon               = ':alien:';
        $attachments        = array([
            'color'    => '#36a64f',
            'text'     => "*<@U5A2WEHEH>, you have new card [$cardname] on Trello*\n_You'll get the notification every one hour until you remove the 'NEW' label_\n$cardURL",
        ]);

        $data = array(
            'channel'     => $channel,
            'username'    => $bot_name,
            'icon_emoji'  => $icon,
            'attachments' => $attachments
        );

        $data_string = json_encode($data);
        $ch = curl_init($webhookURL_Azmi);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
            );
        //Execute CURL
        $result = curl_exec($ch);
        curl_close($ch);
    }

    //fix
    function SendNotification_Alex($cardname, $cardURL){
        $webhookURL_Alex    = 'https://hooks.slack.com/services/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';
        //data for notification
        $channel            = '@Alex Lee';
        $bot_name           = 'Kaigan Taskmaster';
        $icon               = ':alien:';
        $attachments        = array([
            'color'    => '#36a64f',
            'text'     => "*<@U9DV9JUET>, you have new card [$cardname] on Trello*\n_You'll get the notification every one hour until you remove the 'NEW' label_\n$cardURL",
        ]);

        $data = array(
            'channel'     => $channel,
            'username'    => $bot_name,
            'icon_emoji'  => $icon,
            'attachments' => $attachments
        );

        $data_string = json_encode($data);
        $ch = curl_init($webhookURL_Alex);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
            );
        //Execute CURL
        $result = curl_exec($ch);
        curl_close($ch);
    }

    //fix
    function SendNotification_LeeJenLing($cardname, $cardURL){
        $webhookURL_leeJL    = 'https://hooks.slack.com/services/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';
        //data for notification
        $channel            = '@Lee Jen Ling';
        $bot_name           = 'Kaigan Taskmaster';
        $icon               = ':alien:';
        $attachments        = array([
            'color'    => '#36a64f',
            'text'     => "*<@UFCJUGKDH>, you have new card [$cardname] on Trello*\n_You'll get the notification every one hour until you remove the 'NEW' label_\n$cardURL",
        ]);

        $data = array(
            'channel'     => $channel,
            'username'    => $bot_name,
            'icon_emoji'  => $icon,
            'attachments' => $attachments
        );

        $data_string = json_encode($data);
        $ch = curl_init($webhookURL_leeJL);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
            );
        //Execute CURL
        $result = curl_exec($ch);
        curl_close($ch);
    }

    //fix
    function SendNotification_Jeremy($cardname, $cardURL){
        $webhookURL_Jeremy  = 'https://hooks.slack.com/services/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';
        //data for notification
        $channel            = '@jeremy';
        $bot_name           = 'Kaigan Taskmaster';
        $icon               = ':alien:';
        $attachments        = array([
            'color'    => '#36a64f',
            'text'     => "*<@U59LLD0JE>, you have new card [$cardname] on Trello*\n_You'll get the notification every one hour until you remove the 'NEW' label_\n$cardURL",
        ]);

        $data = array(
            'channel'     => $channel,
            'username'    => $bot_name,
            'icon_emoji'  => $icon,
            'attachments' => $attachments
        );

        $data_string = json_encode($data);
        $ch = curl_init($webhookURL_Jeremy);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
            );
        //Execute CURL
        $result = curl_exec($ch);
        curl_close($ch);
    }

    //fix
    function SendNotification_Priya($cardname, $cardURL){
        $webhookURL_Priya   = 'https://hooks.slack.com/services/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';
        //data for notification
        $channel            = '@priya k';
        $bot_name           = 'Kaigan Taskmaster';
        $icon               = ':alien:';
        $attachments        = array([
            'color'    => '#36a64f',
            'text'     => "*<@UCGKY4P2T>, you have new card [$cardname] on Trello*\n_You'll get the notification every one hour until you remove the 'NEW' label_\n$cardURL",
        ]);

        $data = array(
            'channel'     => $channel,
            'username'    => $bot_name,
            'icon_emoji'  => $icon,
            'attachments' => $attachments
        );

        $data_string = json_encode($data);
        $ch = curl_init($webhookURL_Priya);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
            );
        //Execute CURL
        $result = curl_exec($ch);
        curl_close($ch);
    }

    //fix
    function SendNotification_Rex($cardname, $cardURL){
        $webhookURL_Rex     = 'https://hooks.slack.com/services/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';
        //data for notification
        $channel            = '@Rex Naythian';
        $bot_name           = 'Kaigan Taskmaster';
        $icon               = ':alien:';
        $attachments        = array([
            'color'    => '#36a64f',
            'text'     => "*<@UFC03D588>, you have new card [$cardname] on Trello*\n_You'll get the notification every one hour until you remove the 'NEW' label_\n$cardURL",
        ]);

        $data = array(
            'channel'     => $channel,
            'username'    => $bot_name,
            'icon_emoji'  => $icon,
            'attachments' => $attachments
        );

        $data_string = json_encode($data);
        $ch = curl_init($webhookURL_Rex);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
            );
        //Execute CURL
        $result = curl_exec($ch);
        curl_close($ch);
    }

    //fix
    function SendNotification_LeeYing($cardname, $cardURL){
        $webhookURL_LeeYing   = 'https://hooks.slack.com/services/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';
        //data for notification
        $channel            = '@leeying';
        $bot_name           = 'Kaigan Taskmaster';
        $icon               = ':alien:';
        $attachments        = array([
            'color'    => '#36a64f',
            'text'     => "*<@U591PGRJ4>, you have new card [$cardname] on Trello*\n_You'll get the notification every one hour until you remove the 'NEW' label_\n$cardURL",
        ]);

        $data = array(
            'channel'     => $channel,
            'username'    => $bot_name,
            'icon_emoji'  => $icon,
            'attachments' => $attachments
        );

        $data_string = json_encode($data);
        $ch = curl_init($webhookURL_LeeYing);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
            );
        //Execute CURL
        $result = curl_exec($ch);
        curl_close($ch);
    }

    //fix
    function SendNotification_Shah($cardname, $cardURL){
        $webhookURL_Shah    = 'https://hooks.slack.com/services/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';
        //data for notification
        $channel            = '@shah';
        $bot_name           = 'Kaigan Taskmaster';
        $icon               = ':alien:';
        $attachments        = array([
            'color'    => '#36a64f',
            'text'     => "*<@U59L90G66>, you have new card [$cardname] on Trello*\n_You'll get the notification every one hour until you remove the 'NEW' label_\n$cardURL",
        ]);

        $data = array(
            'channel'     => $channel,
            'username'    => $bot_name,
            'icon_emoji'  => $icon,
            'attachments' => $attachments
        );

        $data_string = json_encode($data);
        $ch = curl_init($webhookURL_Shah);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
            );
        //Execute CURL
        $result = curl_exec($ch);
        curl_close($ch);
    }

    //fix
    function SendNotification_Suhana($cardname, $cardURL){
        $webhookURL_Suhana   = 'https://hooks.slack.com/services/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';
        //data for notification
        $channel            = '@suhana';
        $bot_name           = 'Kaigan Taskmaster';
        $icon               = ':alien:';
        $attachments        = array([
            'color'    => '#36a64f',
            'text'     => "*<@U59MSGE12>, you have new card [$cardname] on Trello*\n_You'll get the notification every one hour until you remove the 'NEW' label_\n$cardURL",
        ]);

        $data = array(
            'channel'     => $channel,
            'username'    => $bot_name,
            'icon_emoji'  => $icon,
            'attachments' => $attachments
        );

        $data_string = json_encode($data);
        $ch = curl_init($webhookURL_Suhana);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
            );
        //Execute CURL
        $result = curl_exec($ch);
        curl_close($ch);
    }

//For Trello API
    //get data from development list && fix
    function getDataFromDevelopmentList(){
        
        $APIURI     = 'https://api.trello.com/1/lists/5c3308c14360d00d5e72d78d/cards?cards=visible&card_member_fields=all&key=cee15f8c5b0d25a32783f5139b64edb4&token=0a9207e773917d4738910c973994011eea5bc955f93e59cdff9d1b7f1ab8776d';

        $init       = curl_init();
        curl_setopt($init, CURLOPT_URL, $APIURI); //Url together with parameters
        curl_setopt($init, CURLOPT_RETURNTRANSFER, 1); //Return data instead printing directly in Browser
        curl_setopt($init, CURLOPT_CONNECTTIMEOUT , 7); //Timeout after 7 seconds
        curl_setopt($init, CURLOPT_USERAGENT , "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
        curl_setopt($init, CURLOPT_HEADER, 0);

        $exec       = curl_exec($init);
        $close      = curl_close($init);

        $decodeJSON = json_decode($exec, true);
        $countJsonArray  = count($decodeJSON);
        // echo $countJsonArray;
        $i = 0;
        while($i < $countJsonArray){
            //parsing "labels" array
            foreach($decodeJSON[$i]['labels'] as $labelsData){
                $dataLabel      = $labelsData['name'];
                //conditional to filter "new" label
                if($dataLabel == 'NEW'){
                    $dataMMB    = $decodeJSON[$i]['idMembers'];
                    //this data for card name and card url
                    $cardName   = $decodeJSON[$i]['name'];
                    $cardURL    = $decodeJSON[$i]['url'];
                    
                    //if found the data member > 1
                    if(count($dataMMB) > 1){                            
                        $id_member1 = $dataMMB[0];
                        $id_member2 = $dataMMB[1];
                        $id_member3 = $dataMMB[2];
                        $id_member4 = $dataMMB[3];
                        $id_member5 = $dataMMB[4];
                        $id_member6 = $dataMMB[5];
                        $id_member7 = $dataMMB[6];
                        $id_member8 = $dataMMB[7];
                        $id_member9 = $dataMMB[8];
                        $id_member10 = $dataMMB[9];
                        
                        //arief
                        if($id_member1 == '5959c36a55c57339a774ff8c' || $id_member2 == '5959c36a55c57339a774ff8c' || $id_member3 == '5959c36a55c57339a774ff8c' || $id_member4 == '5959c36a55c57339a774ff8c'
                        || $id_member5 == '5959c36a55c57339a774ff8c' || $id_member6 == '5959c36a55c57339a774ff8c' || $id_member7 == '5959c36a55c57339a774ff8c' || $id_member8 == '5959c36a55c57339a774ff8c'
                        || $id_member9 == '5959c36a55c57339a774ff8c' || $id_member10 == '5959c36a55c57339a774ff8c'){
                            $forArief_mn   = 1;
                        } 
                        //azmi
                        if($id_member1 == '58b66821144d4b68a5ab1c84' || $id_member2 == '58b66821144d4b68a5ab1c84' || $id_member3 == '58b66821144d4b68a5ab1c84' || $id_member4 == '58b66821144d4b68a5ab1c84'
                        || $id_member5 == '58b66821144d4b68a5ab1c84' || $id_member6 == '58b66821144d4b68a5ab1c84' || $id_member7 == '58b66821144d4b68a5ab1c84' || $id_member8 == '58b66821144d4b68a5ab1c84'
                        || $id_member9 == '58b66821144d4b68a5ab1c84' || $id_member10 == '58b66821144d4b68a5ab1c84'){
                            $forAzmi_mn    = 1;
                        }
                        //alex
                        if($id_member1 == '5b70ef61add7a23d75e11a44' || $id_member2 == '5b70ef61add7a23d75e11a44' || $id_member3 == '5b70ef61add7a23d75e11a44' || $id_member4 == '5b70ef61add7a23d75e11a44'
                        || $id_member5 == '5b70ef61add7a23d75e11a44' || $id_member6 == '5b70ef61add7a23d75e11a44' || $id_member7 == '5b70ef61add7a23d75e11a44' || $id_member8 == '5b70ef61add7a23d75e11a44'
                        || $id_member9 == '5b70ef61add7a23d75e11a44' || $id_member10 == '5b70ef61add7a23d75e11a44'){
                            $forAlex_mn    = 1;
                        }
                        //lee jen ling
                        if($id_member1 == '5c3bf62e0581ed89ee7b6580' || $id_member2 == '5c3bf62e0581ed89ee7b6580' || $id_member3 == '5c3bf62e0581ed89ee7b6580' || $id_member4 == '5c3bf62e0581ed89ee7b6580'
                        || $id_member5 == '5c3bf62e0581ed89ee7b6580' || $id_member6 == '5c3bf62e0581ed89ee7b6580' || $id_member7 == '5c3bf62e0581ed89ee7b6580' || $id_member8 == '5c3bf62e0581ed89ee7b6580'
                        || $id_member9 == '5c3bf62e0581ed89ee7b6580' || $id_member10 == '5c3bf62e0581ed89ee7b6580'){
                            $forLeeJenLing_mn    = 1;
                        }
                        //priya
                        if($id_member1 == '5b70f173191434079ebf6420' || $id_member2 == '5b70f173191434079ebf6420' || $id_member3 == '5b70f173191434079ebf6420' || $id_member4 == '5b70f173191434079ebf6420'
                        || $id_member5 == '5b70f173191434079ebf6420' || $id_member6 == '5b70f173191434079ebf6420' || $id_member7 == '5b70f173191434079ebf6420' || $id_member8 == '5b70f173191434079ebf6420'
                        || $id_member9 == '5b70f173191434079ebf6420' || $id_member10 == '5b70f173191434079ebf6420'){
                            $forPriya_mn    = 1;
                        }
                        //jeremy
                        if($id_member1 == '559cce487e609257b9b668e8' || $id_member2 == '559cce487e609257b9b668e8' || $id_member3 == '559cce487e609257b9b668e8' || $id_member4 == '559cce487e609257b9b668e8'
                        || $id_member5 == '559cce487e609257b9b668e8' || $id_member6 == '559cce487e609257b9b668e8' || $id_member7 == '559cce487e609257b9b668e8' || $id_member8 == '559cce487e609257b9b668e8'
                        || $id_member9 == '559cce487e609257b9b668e8' || $id_member10 == '559cce487e609257b9b668e8'){
                            $forJeremy_mn    = 1;
                        }
                        //rex
                        if($id_member1 == '5c3bf7a26814db37f6b41ff2' || $id_member2 == '5c3bf7a26814db37f6b41ff2' || $id_member3 == '5c3bf7a26814db37f6b41ff2' || $id_member4 == '5c3bf7a26814db37f6b41ff2'
                        || $id_member5 == '5c3bf7a26814db37f6b41ff2' || $id_member6 == '5c3bf7a26814db37f6b41ff2' || $id_member7 == '5c3bf7a26814db37f6b41ff2' || $id_member8 == '5c3bf7a26814db37f6b41ff2'
                        || $id_member9 == '5c3bf7a26814db37f6b41ff2' || $id_member10 == '5c3bf7a26814db37f6b41ff2'){
                            $forRex_mn    = 1;
                        }
                        //lee ying
                        if($id_member1 == '590c33ec597ce7d26734f355' || $id_member2 == '590c33ec597ce7d26734f355' || $id_member3 == '590c33ec597ce7d26734f355' || $id_member4 == '590c33ec597ce7d26734f355'
                        || $id_member5 == '590c33ec597ce7d26734f355' || $id_member6 == '590c33ec597ce7d26734f355' || $id_member7 == '590c33ec597ce7d26734f355' || $id_member8 == '590c33ec597ce7d26734f355'
                        || $id_member9 == '590c33ec597ce7d26734f355' || $id_member10 == '590c33ec597ce7d26734f355'){
                            $forLeeYing_mn    = 1;
                        }
                        //shah
                        if($id_member1 == '58acfc97f4b21ebff852f886' || $id_member2 == '58acfc97f4b21ebff852f886' || $id_member3 == '58acfc97f4b21ebff852f886' || $id_member4 == '58acfc97f4b21ebff852f886'
                        || $id_member5 == '58acfc97f4b21ebff852f886' || $id_member6 == '58acfc97f4b21ebff852f886' || $id_member7 == '58acfc97f4b21ebff852f886' || $id_member8 == '58acfc97f4b21ebff852f886'
                        || $id_member9 == '58acfc97f4b21ebff852f886' || $id_member10 == '58acfc97f4b21ebff852f886'){
                            $forShah_mn    = 1;
                        }
                        //mrs. suhana
                        if($id_member1 == '58abdb1a88c1ba56168f5d9e' || $id_member2 == '58abdb1a88c1ba56168f5d9e' || $id_member3 == '58abdb1a88c1ba56168f5d9e' || $id_member4 == '58abdb1a88c1ba56168f5d9e'
                        || $id_member5 == '58abdb1a88c1ba56168f5d9e' || $id_member6 == '58abdb1a88c1ba56168f5d9e' || $id_member7 == '58abdb1a88c1ba56168f5d9e' || $id_member8 == '58abdb1a88c1ba56168f5d9e'
                        || $id_member9 == '58abdb1a88c1ba56168f5d9e' || $id_member10 == '58abdb1a88c1ba56168f5d9e'){
                            $forMrsSuhana_mn    = 1;
                        }                        

                        //send notification
                        if($forArief_mn == 1){
                            SendNotification_Arief($cardName, $cardURL);
                        }

                        if($forAzmi_mn == 1){
                            SendNotification_Azmi($cardName, $cardURL);
                        }

                        if($forAlex_mn == 1){
                            SendNotification_Alex($cardName, $cardURL);
                        }

                        if($forLeeJenLing_mn == 1){
                            SendNotification_LeeJenLing($cardName, $cardURL);
                        }

                        if($forPriya_mn == 1){
                            SendNotification_Priya($cardName, $cardURL);
                        }

                        if($forJeremy_mn == 1){
                            SendNotification_Jeremy($cardName, $cardURL);
                        }

                        if($forRex_mn == 1){
                            SendNotification_Rex($cardName, $cardURL);
                        }

                        if($forLeeYing_mn == 1){
                            SendNotification_LeeYing($cardName, $cardURL);
                        }

                        if($forShah_mn == 1){
                            SendNotification_Shah($cardName, $cardURL);
                        }

                        if($forMrsSuhana_mn == 1){
                            SendNotification_Suhana($cardName, $cardURL);
                        }
                        
                    } else {
                        $id_member    = $dataMMB[0];

                        //check which memberID
                        if($id_member == '5959c36a55c57339a774ff8c'){
                            $forArief   = 1;
                        } 
                        if($id_member == '58b66821144d4b68a5ab1c84'){
                            $forAzmi    = 1;
                        } 
                        if($id_member == '5b70ef61add7a23d75e11a44'){
                            $forAlex    = 1;
                        } 
                        if($id_member == '5c3bf62e0581ed89ee7b6580'){
                            $forLeeJenLing    = 1;
                        } 
                        if($id_member == '5b70f173191434079ebf6420'){
                            $forPriya   = 1;
                        } 
                        if($id_member == '559cce487e609257b9b668e8'){
                            $forJeremy  = 1;
                        } 
                        if($id_member == '5c3bf7a26814db37f6b41ff2'){
                            $forRex     = 1;
                        } 
                        if($id_member == '590c33ec597ce7d26734f355'){
                            $forLeeYing       = 1;
                        } 
                        if($id_member == '58acfc97f4b21ebff852f886'){
                            $forShah    = 1;
                        } 
                        if($id_member == '58abdb1a88c1ba56168f5d9e'){
                            $forMrsSuhana     = 1;
                        } 

                        //send notification
                        if($forArief == 1){
                            SendNotification_Arief($cardName, $cardURL);
                        }

                        if($forAzmi == 1){
                            SendNotification_Azmi($cardName, $cardURL);
                        }

                        if($forAlex == 1){
                            SendNotification_Alex($cardName, $cardURL);
                        }

                        if($forLeeJenLing == 1){
                            SendNotification_LeeJenLing($cardName, $cardURL);
                        }

                        if($forPriya == 1){
                            SendNotification_Priya($cardName, $cardURL);
                        }

                        if($forJeremy == 1){
                            SendNotification_Jeremy($cardName, $cardURL);
                        }

                        if($forRex == 1){
                            SendNotification_Rex($cardName, $cardURL);
                        }

                        if($forLeeYing == 1){
                            SendNotification_LeeYing($cardName, $cardURL);
                        }

                        if($forShah == 1){
                            SendNotification_Shah($cardName, $cardURL);
                        }

                        if($forMrsSuhana == 1){
                            SendNotification_Suhana($cardName, $cardURL);
                        }
                    }
                }
            }
            $i++;
        }
    }

    //get data from game design list && fix
    function getDataFromGameDesignList(){
        
        $APIURI     = 'https://api.trello.com/1/lists/5bceba7f62850a31fb5160dc/cards?cards=visible&card_member_fields=all&key=cee15f8c5b0d25a32783f5139b64edb4&token=0a9207e773917d4738910c973994011eea5bc955f93e59cdff9d1b7f1ab8776d';

        $init       = curl_init();
        curl_setopt($init, CURLOPT_URL, $APIURI); //Url together with parameters
        curl_setopt($init, CURLOPT_RETURNTRANSFER, 1); //Return data instead printing directly in Browser
        curl_setopt($init, CURLOPT_CONNECTTIMEOUT , 7); //Timeout after 7 seconds
        curl_setopt($init, CURLOPT_USERAGENT , "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
        curl_setopt($init, CURLOPT_HEADER, 0);

        $exec       = curl_exec($init);
        $close      = curl_close($init);

        $decodeJSON = json_decode($exec, true);
        $countJsonArray  = count($decodeJSON);
        // echo $countJsonArray;
        $i = 0;
        while($i < $countJsonArray){
            //parsing "labels" array
            foreach($decodeJSON[$i]['labels'] as $labelsData){
                $dataLabel      = $labelsData['name'];
                //conditional to filter "new" label
                if($dataLabel == 'NEW'){
                    $dataMMB    = $decodeJSON[$i]['idMembers'];
                    //this data for card name and card url
                    $cardName   = $decodeJSON[$i]['name'];
                    $cardURL    = $decodeJSON[$i]['url'];
                    
                    //if found the data member > 1
                    if(count($dataMMB) > 1){                            
                        $id_member1 = $dataMMB[0];
                        $id_member2 = $dataMMB[1];
                        $id_member3 = $dataMMB[2];
                        $id_member4 = $dataMMB[3];
                        $id_member5 = $dataMMB[4];
                        $id_member6 = $dataMMB[5];
                        $id_member7 = $dataMMB[6];
                        $id_member8 = $dataMMB[7];
                        $id_member9 = $dataMMB[8];
                        $id_member10 = $dataMMB[9];
                        
                        //arief
                        if($id_member1 == '5959c36a55c57339a774ff8c' || $id_member2 == '5959c36a55c57339a774ff8c' || $id_member3 == '5959c36a55c57339a774ff8c' || $id_member4 == '5959c36a55c57339a774ff8c'
                        || $id_member5 == '5959c36a55c57339a774ff8c' || $id_member6 == '5959c36a55c57339a774ff8c' || $id_member7 == '5959c36a55c57339a774ff8c' || $id_member8 == '5959c36a55c57339a774ff8c'
                        || $id_member9 == '5959c36a55c57339a774ff8c' || $id_member10 == '5959c36a55c57339a774ff8c'){
                            $forArief_mn   = 1;
                        } 
                        //azmi
                        if($id_member1 == '58b66821144d4b68a5ab1c84' || $id_member2 == '58b66821144d4b68a5ab1c84' || $id_member3 == '58b66821144d4b68a5ab1c84' || $id_member4 == '58b66821144d4b68a5ab1c84'
                        || $id_member5 == '58b66821144d4b68a5ab1c84' || $id_member6 == '58b66821144d4b68a5ab1c84' || $id_member7 == '58b66821144d4b68a5ab1c84' || $id_member8 == '58b66821144d4b68a5ab1c84'
                        || $id_member9 == '58b66821144d4b68a5ab1c84' || $id_member10 == '58b66821144d4b68a5ab1c84'){
                            $forAzmi_mn    = 1;
                        }
                        //alex
                        if($id_member1 == '5b70ef61add7a23d75e11a44' || $id_member2 == '5b70ef61add7a23d75e11a44' || $id_member3 == '5b70ef61add7a23d75e11a44' || $id_member4 == '5b70ef61add7a23d75e11a44'
                        || $id_member5 == '5b70ef61add7a23d75e11a44' || $id_member6 == '5b70ef61add7a23d75e11a44' || $id_member7 == '5b70ef61add7a23d75e11a44' || $id_member8 == '5b70ef61add7a23d75e11a44'
                        || $id_member9 == '5b70ef61add7a23d75e11a44' || $id_member10 == '5b70ef61add7a23d75e11a44'){
                            $forAlex_mn    = 1;
                        }
                        //lee jen ling
                        if($id_member1 == '5c3bf62e0581ed89ee7b6580' || $id_member2 == '5c3bf62e0581ed89ee7b6580' || $id_member3 == '5c3bf62e0581ed89ee7b6580' || $id_member4 == '5c3bf62e0581ed89ee7b6580'
                        || $id_member5 == '5c3bf62e0581ed89ee7b6580' || $id_member6 == '5c3bf62e0581ed89ee7b6580' || $id_member7 == '5c3bf62e0581ed89ee7b6580' || $id_member8 == '5c3bf62e0581ed89ee7b6580'
                        || $id_member9 == '5c3bf62e0581ed89ee7b6580' || $id_member10 == '5c3bf62e0581ed89ee7b6580'){
                            $forLeeJenLing_mn    = 1;
                        }
                        //priya
                        if($id_member1 == '5b70f173191434079ebf6420' || $id_member2 == '5b70f173191434079ebf6420' || $id_member3 == '5b70f173191434079ebf6420' || $id_member4 == '5b70f173191434079ebf6420'
                        || $id_member5 == '5b70f173191434079ebf6420' || $id_member6 == '5b70f173191434079ebf6420' || $id_member7 == '5b70f173191434079ebf6420' || $id_member8 == '5b70f173191434079ebf6420'
                        || $id_member9 == '5b70f173191434079ebf6420' || $id_member10 == '5b70f173191434079ebf6420'){
                            $forPriya_mn    = 1;
                        }
                        //jeremy
                        if($id_member1 == '559cce487e609257b9b668e8' || $id_member2 == '559cce487e609257b9b668e8' || $id_member3 == '559cce487e609257b9b668e8' || $id_member4 == '559cce487e609257b9b668e8'
                        || $id_member5 == '559cce487e609257b9b668e8' || $id_member6 == '559cce487e609257b9b668e8' || $id_member7 == '559cce487e609257b9b668e8' || $id_member8 == '559cce487e609257b9b668e8'
                        || $id_member9 == '559cce487e609257b9b668e8' || $id_member10 == '559cce487e609257b9b668e8'){
                            $forJeremy_mn    = 1;
                        }
                        //rex
                        if($id_member1 == '5c3bf7a26814db37f6b41ff2' || $id_member2 == '5c3bf7a26814db37f6b41ff2' || $id_member3 == '5c3bf7a26814db37f6b41ff2' || $id_member4 == '5c3bf7a26814db37f6b41ff2'
                        || $id_member5 == '5c3bf7a26814db37f6b41ff2' || $id_member6 == '5c3bf7a26814db37f6b41ff2' || $id_member7 == '5c3bf7a26814db37f6b41ff2' || $id_member8 == '5c3bf7a26814db37f6b41ff2'
                        || $id_member9 == '5c3bf7a26814db37f6b41ff2' || $id_member10 == '5c3bf7a26814db37f6b41ff2'){
                            $forRex_mn    = 1;
                        }
                        //lee ying
                        if($id_member1 == '590c33ec597ce7d26734f355' || $id_member2 == '590c33ec597ce7d26734f355' || $id_member3 == '590c33ec597ce7d26734f355' || $id_member4 == '590c33ec597ce7d26734f355'
                        || $id_member5 == '590c33ec597ce7d26734f355' || $id_member6 == '590c33ec597ce7d26734f355' || $id_member7 == '590c33ec597ce7d26734f355' || $id_member8 == '590c33ec597ce7d26734f355'
                        || $id_member9 == '590c33ec597ce7d26734f355' || $id_member10 == '590c33ec597ce7d26734f355'){
                            $forLeeYing_mn    = 1;
                        }
                        //shah
                        if($id_member1 == '58acfc97f4b21ebff852f886' || $id_member2 == '58acfc97f4b21ebff852f886' || $id_member3 == '58acfc97f4b21ebff852f886' || $id_member4 == '58acfc97f4b21ebff852f886'
                        || $id_member5 == '58acfc97f4b21ebff852f886' || $id_member6 == '58acfc97f4b21ebff852f886' || $id_member7 == '58acfc97f4b21ebff852f886' || $id_member8 == '58acfc97f4b21ebff852f886'
                        || $id_member9 == '58acfc97f4b21ebff852f886' || $id_member10 == '58acfc97f4b21ebff852f886'){
                            $forShah_mn    = 1;
                        }
                        //mrs. suhana
                        if($id_member1 == '58abdb1a88c1ba56168f5d9e' || $id_member2 == '58abdb1a88c1ba56168f5d9e' || $id_member3 == '58abdb1a88c1ba56168f5d9e' || $id_member4 == '58abdb1a88c1ba56168f5d9e'
                        || $id_member5 == '58abdb1a88c1ba56168f5d9e' || $id_member6 == '58abdb1a88c1ba56168f5d9e' || $id_member7 == '58abdb1a88c1ba56168f5d9e' || $id_member8 == '58abdb1a88c1ba56168f5d9e'
                        || $id_member9 == '58abdb1a88c1ba56168f5d9e' || $id_member10 == '58abdb1a88c1ba56168f5d9e'){
                            $forMrsSuhana_mn    = 1;
                        }                        

                        //send notification
                        if($forArief_mn == 1){
                            SendNotification_Arief($cardName, $cardURL);
                        }

                        if($forAzmi_mn == 1){
                            SendNotification_Azmi($cardName, $cardURL);
                        }

                        if($forAlex_mn == 1){
                            SendNotification_Alex($cardName, $cardURL);
                        }

                        if($forLeeJenLing_mn == 1){
                            SendNotification_LeeJenLing($cardName, $cardURL);
                        }

                        if($forPriya_mn == 1){
                            SendNotification_Priya($cardName, $cardURL);
                        }

                        if($forJeremy_mn == 1){
                            SendNotification_Jeremy($cardName, $cardURL);
                        }

                        if($forRex_mn == 1){
                            SendNotification_Rex($cardName, $cardURL);
                        }

                        if($forLeeYing_mn == 1){
                            SendNotification_LeeYing($cardName, $cardURL);
                        }

                        if($forShah_mn == 1){
                            SendNotification_Shah($cardName, $cardURL);
                        }

                        if($forMrsSuhana_mn == 1){
                            SendNotification_Suhana($cardName, $cardURL);
                        }
                        
                    } else {
                        $id_member    = $dataMMB[0];

                        //check which memberID
                        if($id_member == '5959c36a55c57339a774ff8c'){
                            $forArief   = 1;
                        } 
                        if($id_member == '58b66821144d4b68a5ab1c84'){
                            $forAzmi    = 1;
                        } 
                        if($id_member == '5b70ef61add7a23d75e11a44'){
                            $forAlex    = 1;
                        } 
                        if($id_member == '5c3bf62e0581ed89ee7b6580'){
                            $forLeeJenLing    = 1;
                        } 
                        if($id_member == '5b70f173191434079ebf6420'){
                            $forPriya   = 1;
                        } 
                        if($id_member == '559cce487e609257b9b668e8'){
                            $forJeremy  = 1;
                        } 
                        if($id_member == '5c3bf7a26814db37f6b41ff2'){
                            $forRex     = 1;
                        } 
                        if($id_member == '590c33ec597ce7d26734f355'){
                            $forLeeYing       = 1;
                        } 
                        if($id_member == '58acfc97f4b21ebff852f886'){
                            $forShah    = 1;
                        } 
                        if($id_member == '58abdb1a88c1ba56168f5d9e'){
                            $forMrsSuhana     = 1;
                        } 

                        //send notification
                        if($forArief == 1){
                            SendNotification_Arief($cardName, $cardURL);
                        }

                        if($forAzmi == 1){
                            SendNotification_Azmi($cardName, $cardURL);
                        }

                        if($forAlex == 1){
                            SendNotification_Alex($cardName, $cardURL);
                        }

                        if($forLeeJenLing == 1){
                            SendNotification_LeeJenLing($cardName, $cardURL);
                        }

                        if($forPriya == 1){
                            SendNotification_Priya($cardName, $cardURL);
                        }

                        if($forJeremy == 1){
                            SendNotification_Jeremy($cardName, $cardURL);
                        }

                        if($forRex == 1){
                            SendNotification_Rex($cardName, $cardURL);
                        }

                        if($forLeeYing == 1){
                            SendNotification_LeeYing($cardName, $cardURL);
                        }

                        if($forShah == 1){
                            SendNotification_Shah($cardName, $cardURL);
                        }

                        if($forMrsSuhana == 1){
                            SendNotification_Suhana($cardName, $cardURL);
                        }
                    }
                }
            }
            $i++;
        }
    }

    //get data from writing list && fix
    function getDataFromWritingList(){
        
        $APIURI     = 'https://api.trello.com/1/lists/5bceba7f62850a31fb5160de/cards?cards=visible&card_member_fields=all&key=cee15f8c5b0d25a32783f5139b64edb4&token=0a9207e773917d4738910c973994011eea5bc955f93e59cdff9d1b7f1ab8776d';

        $init       = curl_init();
        curl_setopt($init, CURLOPT_URL, $APIURI); //Url together with parameters
        curl_setopt($init, CURLOPT_RETURNTRANSFER, 1); //Return data instead printing directly in Browser
        curl_setopt($init, CURLOPT_CONNECTTIMEOUT , 7); //Timeout after 7 seconds
        curl_setopt($init, CURLOPT_USERAGENT , "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
        curl_setopt($init, CURLOPT_HEADER, 0);

        $exec       = curl_exec($init);
        $close      = curl_close($init);

        $decodeJSON = json_decode($exec, true);
        $countJsonArray  = count($decodeJSON);
        // echo $countJsonArray;
        $i = 0;
        while($i < $countJsonArray){
            //parsing "labels" array
            foreach($decodeJSON[$i]['labels'] as $labelsData){
                $dataLabel      = $labelsData['name'];
                //conditional to filter "new" label
                if($dataLabel == 'NEW'){
                    $dataMMB    = $decodeJSON[$i]['idMembers'];
                    //this data for card name and card url
                    $cardName   = $decodeJSON[$i]['name'];
                    $cardURL    = $decodeJSON[$i]['url'];
                    
                    //if found the data member > 1
                    if(count($dataMMB) > 1){                            
                        $id_member1 = $dataMMB[0];
                        $id_member2 = $dataMMB[1];
                        $id_member3 = $dataMMB[2];
                        $id_member4 = $dataMMB[3];
                        $id_member5 = $dataMMB[4];
                        $id_member6 = $dataMMB[5];
                        $id_member7 = $dataMMB[6];
                        $id_member8 = $dataMMB[7];
                        $id_member9 = $dataMMB[8];
                        $id_member10 = $dataMMB[9];
                        
                        //arief
                        if($id_member1 == '5959c36a55c57339a774ff8c' || $id_member2 == '5959c36a55c57339a774ff8c' || $id_member3 == '5959c36a55c57339a774ff8c' || $id_member4 == '5959c36a55c57339a774ff8c'
                        || $id_member5 == '5959c36a55c57339a774ff8c' || $id_member6 == '5959c36a55c57339a774ff8c' || $id_member7 == '5959c36a55c57339a774ff8c' || $id_member8 == '5959c36a55c57339a774ff8c'
                        || $id_member9 == '5959c36a55c57339a774ff8c' || $id_member10 == '5959c36a55c57339a774ff8c'){
                            $forArief_mn   = 1;
                        } 
                        //azmi
                        if($id_member1 == '58b66821144d4b68a5ab1c84' || $id_member2 == '58b66821144d4b68a5ab1c84' || $id_member3 == '58b66821144d4b68a5ab1c84' || $id_member4 == '58b66821144d4b68a5ab1c84'
                        || $id_member5 == '58b66821144d4b68a5ab1c84' || $id_member6 == '58b66821144d4b68a5ab1c84' || $id_member7 == '58b66821144d4b68a5ab1c84' || $id_member8 == '58b66821144d4b68a5ab1c84'
                        || $id_member9 == '58b66821144d4b68a5ab1c84' || $id_member10 == '58b66821144d4b68a5ab1c84'){
                            $forAzmi_mn    = 1;
                        }
                        //alex
                        if($id_member1 == '5b70ef61add7a23d75e11a44' || $id_member2 == '5b70ef61add7a23d75e11a44' || $id_member3 == '5b70ef61add7a23d75e11a44' || $id_member4 == '5b70ef61add7a23d75e11a44'
                        || $id_member5 == '5b70ef61add7a23d75e11a44' || $id_member6 == '5b70ef61add7a23d75e11a44' || $id_member7 == '5b70ef61add7a23d75e11a44' || $id_member8 == '5b70ef61add7a23d75e11a44'
                        || $id_member9 == '5b70ef61add7a23d75e11a44' || $id_member10 == '5b70ef61add7a23d75e11a44'){
                            $forAlex_mn    = 1;
                        }
                        //lee jen ling
                        if($id_member1 == '5c3bf62e0581ed89ee7b6580' || $id_member2 == '5c3bf62e0581ed89ee7b6580' || $id_member3 == '5c3bf62e0581ed89ee7b6580' || $id_member4 == '5c3bf62e0581ed89ee7b6580'
                        || $id_member5 == '5c3bf62e0581ed89ee7b6580' || $id_member6 == '5c3bf62e0581ed89ee7b6580' || $id_member7 == '5c3bf62e0581ed89ee7b6580' || $id_member8 == '5c3bf62e0581ed89ee7b6580'
                        || $id_member9 == '5c3bf62e0581ed89ee7b6580' || $id_member10 == '5c3bf62e0581ed89ee7b6580'){
                            $forLeeJenLing_mn    = 1;
                        }
                        //priya
                        if($id_member1 == '5b70f173191434079ebf6420' || $id_member2 == '5b70f173191434079ebf6420' || $id_member3 == '5b70f173191434079ebf6420' || $id_member4 == '5b70f173191434079ebf6420'
                        || $id_member5 == '5b70f173191434079ebf6420' || $id_member6 == '5b70f173191434079ebf6420' || $id_member7 == '5b70f173191434079ebf6420' || $id_member8 == '5b70f173191434079ebf6420'
                        || $id_member9 == '5b70f173191434079ebf6420' || $id_member10 == '5b70f173191434079ebf6420'){
                            $forPriya_mn    = 1;
                        }
                        //jeremy
                        if($id_member1 == '559cce487e609257b9b668e8' || $id_member2 == '559cce487e609257b9b668e8' || $id_member3 == '559cce487e609257b9b668e8' || $id_member4 == '559cce487e609257b9b668e8'
                        || $id_member5 == '559cce487e609257b9b668e8' || $id_member6 == '559cce487e609257b9b668e8' || $id_member7 == '559cce487e609257b9b668e8' || $id_member8 == '559cce487e609257b9b668e8'
                        || $id_member9 == '559cce487e609257b9b668e8' || $id_member10 == '559cce487e609257b9b668e8'){
                            $forJeremy_mn    = 1;
                        }
                        //rex
                        if($id_member1 == '5c3bf7a26814db37f6b41ff2' || $id_member2 == '5c3bf7a26814db37f6b41ff2' || $id_member3 == '5c3bf7a26814db37f6b41ff2' || $id_member4 == '5c3bf7a26814db37f6b41ff2'
                        || $id_member5 == '5c3bf7a26814db37f6b41ff2' || $id_member6 == '5c3bf7a26814db37f6b41ff2' || $id_member7 == '5c3bf7a26814db37f6b41ff2' || $id_member8 == '5c3bf7a26814db37f6b41ff2'
                        || $id_member9 == '5c3bf7a26814db37f6b41ff2' || $id_member10 == '5c3bf7a26814db37f6b41ff2'){
                            $forRex_mn    = 1;
                        }
                        //lee ying
                        if($id_member1 == '590c33ec597ce7d26734f355' || $id_member2 == '590c33ec597ce7d26734f355' || $id_member3 == '590c33ec597ce7d26734f355' || $id_member4 == '590c33ec597ce7d26734f355'
                        || $id_member5 == '590c33ec597ce7d26734f355' || $id_member6 == '590c33ec597ce7d26734f355' || $id_member7 == '590c33ec597ce7d26734f355' || $id_member8 == '590c33ec597ce7d26734f355'
                        || $id_member9 == '590c33ec597ce7d26734f355' || $id_member10 == '590c33ec597ce7d26734f355'){
                            $forLeeYing_mn    = 1;
                        }
                        //shah
                        if($id_member1 == '58acfc97f4b21ebff852f886' || $id_member2 == '58acfc97f4b21ebff852f886' || $id_member3 == '58acfc97f4b21ebff852f886' || $id_member4 == '58acfc97f4b21ebff852f886'
                        || $id_member5 == '58acfc97f4b21ebff852f886' || $id_member6 == '58acfc97f4b21ebff852f886' || $id_member7 == '58acfc97f4b21ebff852f886' || $id_member8 == '58acfc97f4b21ebff852f886'
                        || $id_member9 == '58acfc97f4b21ebff852f886' || $id_member10 == '58acfc97f4b21ebff852f886'){
                            $forShah_mn    = 1;
                        }
                        //mrs. suhana
                        if($id_member1 == '58abdb1a88c1ba56168f5d9e' || $id_member2 == '58abdb1a88c1ba56168f5d9e' || $id_member3 == '58abdb1a88c1ba56168f5d9e' || $id_member4 == '58abdb1a88c1ba56168f5d9e'
                        || $id_member5 == '58abdb1a88c1ba56168f5d9e' || $id_member6 == '58abdb1a88c1ba56168f5d9e' || $id_member7 == '58abdb1a88c1ba56168f5d9e' || $id_member8 == '58abdb1a88c1ba56168f5d9e'
                        || $id_member9 == '58abdb1a88c1ba56168f5d9e' || $id_member10 == '58abdb1a88c1ba56168f5d9e'){
                            $forMrsSuhana_mn    = 1;
                        }                        

                        //send notification
                        if($forArief_mn == 1){
                            SendNotification_Arief($cardName, $cardURL);
                        }

                        if($forAzmi_mn == 1){
                            SendNotification_Azmi($cardName, $cardURL);
                        }

                        if($forAlex_mn == 1){
                            SendNotification_Alex($cardName, $cardURL);
                        }

                        if($forLeeJenLing_mn == 1){
                            SendNotification_LeeJenLing($cardName, $cardURL);
                        }

                        if($forPriya_mn == 1){
                            SendNotification_Priya($cardName, $cardURL);
                        }

                        if($forJeremy_mn == 1){
                            SendNotification_Jeremy($cardName, $cardURL);
                        }

                        if($forRex_mn == 1){
                            SendNotification_Rex($cardName, $cardURL);
                        }

                        if($forLeeYing_mn == 1){
                            SendNotification_LeeYing($cardName, $cardURL);
                        }

                        if($forShah_mn == 1){
                            SendNotification_Shah($cardName, $cardURL);
                        }

                        if($forMrsSuhana_mn == 1){
                            SendNotification_Suhana($cardName, $cardURL);
                        }
                        
                    } else {
                        $id_member    = $dataMMB[0];

                        //check which memberID
                        if($id_member == '5959c36a55c57339a774ff8c'){
                            $forArief   = 1;
                        } 
                        if($id_member == '58b66821144d4b68a5ab1c84'){
                            $forAzmi    = 1;
                        } 
                        if($id_member == '5b70ef61add7a23d75e11a44'){
                            $forAlex    = 1;
                        } 
                        if($id_member == '5c3bf62e0581ed89ee7b6580'){
                            $forLeeJenLing    = 1;
                        } 
                        if($id_member == '5b70f173191434079ebf6420'){
                            $forPriya   = 1;
                        } 
                        if($id_member == '559cce487e609257b9b668e8'){
                            $forJeremy  = 1;
                        } 
                        if($id_member == '5c3bf7a26814db37f6b41ff2'){
                            $forRex     = 1;
                        } 
                        if($id_member == '590c33ec597ce7d26734f355'){
                            $forLeeYing       = 1;
                        } 
                        if($id_member == '58acfc97f4b21ebff852f886'){
                            $forShah    = 1;
                        } 
                        if($id_member == '58abdb1a88c1ba56168f5d9e'){
                            $forMrsSuhana     = 1;
                        } 

                        //send notification
                        if($forArief == 1){
                            SendNotification_Arief($cardName, $cardURL);
                        }

                        if($forAzmi == 1){
                            SendNotification_Azmi($cardName, $cardURL);
                        }

                        if($forAlex == 1){
                            SendNotification_Alex($cardName, $cardURL);
                        }

                        if($forLeeJenLing == 1){
                            SendNotification_LeeJenLing($cardName, $cardURL);
                        }

                        if($forPriya == 1){
                            SendNotification_Priya($cardName, $cardURL);
                        }

                        if($forJeremy == 1){
                            SendNotification_Jeremy($cardName, $cardURL);
                        }

                        if($forRex == 1){
                            SendNotification_Rex($cardName, $cardURL);
                        }

                        if($forLeeYing == 1){
                            SendNotification_LeeYing($cardName, $cardURL);
                        }

                        if($forShah == 1){
                            SendNotification_Shah($cardName, $cardURL);
                        }

                        if($forMrsSuhana == 1){
                            SendNotification_Suhana($cardName, $cardURL);
                        }
                    }
                }
            }
            $i++;
        }
    }

    //get data from Art list && fix
    function getDataFromArtList(){
        
        $APIURI     = 'https://api.trello.com/1/lists/5bceba7f62850a31fb5160dd/cards?cards=visible&card_member_fields=all&key=cee15f8c5b0d25a32783f5139b64edb4&token=0a9207e773917d4738910c973994011eea5bc955f93e59cdff9d1b7f1ab8776d';

        $init       = curl_init();
        curl_setopt($init, CURLOPT_URL, $APIURI); //Url together with parameters
        curl_setopt($init, CURLOPT_RETURNTRANSFER, 1); //Return data instead printing directly in Browser
        curl_setopt($init, CURLOPT_CONNECTTIMEOUT , 7); //Timeout after 7 seconds
        curl_setopt($init, CURLOPT_USERAGENT , "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
        curl_setopt($init, CURLOPT_HEADER, 0);

        $exec       = curl_exec($init);
        $close      = curl_close($init);

        $decodeJSON = json_decode($exec, true);
        $countJsonArray  = count($decodeJSON);
        // echo $countJsonArray;
        $i = 0;
        while($i < $countJsonArray){
            //parsing "labels" array
            foreach($decodeJSON[$i]['labels'] as $labelsData){
                $dataLabel      = $labelsData['name'];
                //conditional to filter "new" label
                if($dataLabel == 'NEW'){
                    $dataMMB    = $decodeJSON[$i]['idMembers'];
                    //this data for card name and card url
                    $cardName   = $decodeJSON[$i]['name'];
                    $cardURL    = $decodeJSON[$i]['url'];
                    
                    //if found the data member > 1
                    if(count($dataMMB) > 1){                            
                        $id_member1 = $dataMMB[0];
                        $id_member2 = $dataMMB[1];
                        $id_member3 = $dataMMB[2];
                        $id_member4 = $dataMMB[3];
                        $id_member5 = $dataMMB[4];
                        $id_member6 = $dataMMB[5];
                        $id_member7 = $dataMMB[6];
                        $id_member8 = $dataMMB[7];
                        $id_member9 = $dataMMB[8];
                        $id_member10 = $dataMMB[9];
                        
                        //arief
                        if($id_member1 == '5959c36a55c57339a774ff8c' || $id_member2 == '5959c36a55c57339a774ff8c' || $id_member3 == '5959c36a55c57339a774ff8c' || $id_member4 == '5959c36a55c57339a774ff8c'
                        || $id_member5 == '5959c36a55c57339a774ff8c' || $id_member6 == '5959c36a55c57339a774ff8c' || $id_member7 == '5959c36a55c57339a774ff8c' || $id_member8 == '5959c36a55c57339a774ff8c'
                        || $id_member9 == '5959c36a55c57339a774ff8c' || $id_member10 == '5959c36a55c57339a774ff8c'){
                            $forArief_mn   = 1;
                        } 
                        //azmi
                        if($id_member1 == '58b66821144d4b68a5ab1c84' || $id_member2 == '58b66821144d4b68a5ab1c84' || $id_member3 == '58b66821144d4b68a5ab1c84' || $id_member4 == '58b66821144d4b68a5ab1c84'
                        || $id_member5 == '58b66821144d4b68a5ab1c84' || $id_member6 == '58b66821144d4b68a5ab1c84' || $id_member7 == '58b66821144d4b68a5ab1c84' || $id_member8 == '58b66821144d4b68a5ab1c84'
                        || $id_member9 == '58b66821144d4b68a5ab1c84' || $id_member10 == '58b66821144d4b68a5ab1c84'){
                            $forAzmi_mn    = 1;
                        }
                        //alex
                        if($id_member1 == '5b70ef61add7a23d75e11a44' || $id_member2 == '5b70ef61add7a23d75e11a44' || $id_member3 == '5b70ef61add7a23d75e11a44' || $id_member4 == '5b70ef61add7a23d75e11a44'
                        || $id_member5 == '5b70ef61add7a23d75e11a44' || $id_member6 == '5b70ef61add7a23d75e11a44' || $id_member7 == '5b70ef61add7a23d75e11a44' || $id_member8 == '5b70ef61add7a23d75e11a44'
                        || $id_member9 == '5b70ef61add7a23d75e11a44' || $id_member10 == '5b70ef61add7a23d75e11a44'){
                            $forAlex_mn    = 1;
                        }
                        //lee jen ling
                        if($id_member1 == '5c3bf62e0581ed89ee7b6580' || $id_member2 == '5c3bf62e0581ed89ee7b6580' || $id_member3 == '5c3bf62e0581ed89ee7b6580' || $id_member4 == '5c3bf62e0581ed89ee7b6580'
                        || $id_member5 == '5c3bf62e0581ed89ee7b6580' || $id_member6 == '5c3bf62e0581ed89ee7b6580' || $id_member7 == '5c3bf62e0581ed89ee7b6580' || $id_member8 == '5c3bf62e0581ed89ee7b6580'
                        || $id_member9 == '5c3bf62e0581ed89ee7b6580' || $id_member10 == '5c3bf62e0581ed89ee7b6580'){
                            $forLeeJenLing_mn    = 1;
                        }
                        //priya
                        if($id_member1 == '5b70f173191434079ebf6420' || $id_member2 == '5b70f173191434079ebf6420' || $id_member3 == '5b70f173191434079ebf6420' || $id_member4 == '5b70f173191434079ebf6420'
                        || $id_member5 == '5b70f173191434079ebf6420' || $id_member6 == '5b70f173191434079ebf6420' || $id_member7 == '5b70f173191434079ebf6420' || $id_member8 == '5b70f173191434079ebf6420'
                        || $id_member9 == '5b70f173191434079ebf6420' || $id_member10 == '5b70f173191434079ebf6420'){
                            $forPriya_mn    = 1;
                        }
                        //jeremy
                        if($id_member1 == '559cce487e609257b9b668e8' || $id_member2 == '559cce487e609257b9b668e8' || $id_member3 == '559cce487e609257b9b668e8' || $id_member4 == '559cce487e609257b9b668e8'
                        || $id_member5 == '559cce487e609257b9b668e8' || $id_member6 == '559cce487e609257b9b668e8' || $id_member7 == '559cce487e609257b9b668e8' || $id_member8 == '559cce487e609257b9b668e8'
                        || $id_member9 == '559cce487e609257b9b668e8' || $id_member10 == '559cce487e609257b9b668e8'){
                            $forJeremy_mn    = 1;
                        }
                        //rex
                        if($id_member1 == '5c3bf7a26814db37f6b41ff2' || $id_member2 == '5c3bf7a26814db37f6b41ff2' || $id_member3 == '5c3bf7a26814db37f6b41ff2' || $id_member4 == '5c3bf7a26814db37f6b41ff2'
                        || $id_member5 == '5c3bf7a26814db37f6b41ff2' || $id_member6 == '5c3bf7a26814db37f6b41ff2' || $id_member7 == '5c3bf7a26814db37f6b41ff2' || $id_member8 == '5c3bf7a26814db37f6b41ff2'
                        || $id_member9 == '5c3bf7a26814db37f6b41ff2' || $id_member10 == '5c3bf7a26814db37f6b41ff2'){
                            $forRex_mn    = 1;
                        }
                        //lee ying
                        if($id_member1 == '590c33ec597ce7d26734f355' || $id_member2 == '590c33ec597ce7d26734f355' || $id_member3 == '590c33ec597ce7d26734f355' || $id_member4 == '590c33ec597ce7d26734f355'
                        || $id_member5 == '590c33ec597ce7d26734f355' || $id_member6 == '590c33ec597ce7d26734f355' || $id_member7 == '590c33ec597ce7d26734f355' || $id_member8 == '590c33ec597ce7d26734f355'
                        || $id_member9 == '590c33ec597ce7d26734f355' || $id_member10 == '590c33ec597ce7d26734f355'){
                            $forLeeYing_mn    = 1;
                        }
                        //shah
                        if($id_member1 == '58acfc97f4b21ebff852f886' || $id_member2 == '58acfc97f4b21ebff852f886' || $id_member3 == '58acfc97f4b21ebff852f886' || $id_member4 == '58acfc97f4b21ebff852f886'
                        || $id_member5 == '58acfc97f4b21ebff852f886' || $id_member6 == '58acfc97f4b21ebff852f886' || $id_member7 == '58acfc97f4b21ebff852f886' || $id_member8 == '58acfc97f4b21ebff852f886'
                        || $id_member9 == '58acfc97f4b21ebff852f886' || $id_member10 == '58acfc97f4b21ebff852f886'){
                            $forShah_mn    = 1;
                        }
                        //mrs. suhana
                        if($id_member1 == '58abdb1a88c1ba56168f5d9e' || $id_member2 == '58abdb1a88c1ba56168f5d9e' || $id_member3 == '58abdb1a88c1ba56168f5d9e' || $id_member4 == '58abdb1a88c1ba56168f5d9e'
                        || $id_member5 == '58abdb1a88c1ba56168f5d9e' || $id_member6 == '58abdb1a88c1ba56168f5d9e' || $id_member7 == '58abdb1a88c1ba56168f5d9e' || $id_member8 == '58abdb1a88c1ba56168f5d9e'
                        || $id_member9 == '58abdb1a88c1ba56168f5d9e' || $id_member10 == '58abdb1a88c1ba56168f5d9e'){
                            $forMrsSuhana_mn    = 1;
                        }                        

                        //send notification
                        if($forArief_mn == 1){
                            SendNotification_Arief($cardName, $cardURL);
                        }

                        if($forAzmi_mn == 1){
                            SendNotification_Azmi($cardName, $cardURL);
                        }

                        if($forAlex_mn == 1){
                            SendNotification_Alex($cardName, $cardURL);
                        }

                        if($forLeeJenLing_mn == 1){
                            SendNotification_LeeJenLing($cardName, $cardURL);
                        }

                        if($forPriya_mn == 1){
                            SendNotification_Priya($cardName, $cardURL);
                        }

                        if($forJeremy_mn == 1){
                            SendNotification_Jeremy($cardName, $cardURL);
                        }

                        if($forRex_mn == 1){
                            SendNotification_Rex($cardName, $cardURL);
                        }

                        if($forLeeYing_mn == 1){
                            SendNotification_LeeYing($cardName, $cardURL);
                        }

                        if($forShah_mn == 1){
                            SendNotification_Shah($cardName, $cardURL);
                        }

                        if($forMrsSuhana_mn == 1){
                            SendNotification_Suhana($cardName, $cardURL);
                        }
                        
                    } else {
                        $id_member    = $dataMMB[0];

                        //check which memberID
                        if($id_member == '5959c36a55c57339a774ff8c'){
                            $forArief   = 1;
                        } 
                        if($id_member == '58b66821144d4b68a5ab1c84'){
                            $forAzmi    = 1;
                        } 
                        if($id_member == '5b70ef61add7a23d75e11a44'){
                            $forAlex    = 1;
                        } 
                        if($id_member == '5c3bf62e0581ed89ee7b6580'){
                            $forLeeJenLing    = 1;
                        } 
                        if($id_member == '5b70f173191434079ebf6420'){
                            $forPriya   = 1;
                        } 
                        if($id_member == '559cce487e609257b9b668e8'){
                            $forJeremy  = 1;
                        } 
                        if($id_member == '5c3bf7a26814db37f6b41ff2'){
                            $forRex     = 1;
                        } 
                        if($id_member == '590c33ec597ce7d26734f355'){
                            $forLeeYing       = 1;
                        } 
                        if($id_member == '58acfc97f4b21ebff852f886'){
                            $forShah    = 1;
                        } 
                        if($id_member == '58abdb1a88c1ba56168f5d9e'){
                            $forMrsSuhana     = 1;
                        } 

                        //send notification
                        if($forArief == 1){
                            SendNotification_Arief($cardName, $cardURL);
                        }

                        if($forAzmi == 1){
                            SendNotification_Azmi($cardName, $cardURL);
                        }

                        if($forAlex == 1){
                            SendNotification_Alex($cardName, $cardURL);
                        }

                        if($forLeeJenLing == 1){
                            SendNotification_LeeJenLing($cardName, $cardURL);
                        }

                        if($forPriya == 1){
                            SendNotification_Priya($cardName, $cardURL);
                        }

                        if($forJeremy == 1){
                            SendNotification_Jeremy($cardName, $cardURL);
                        }

                        if($forRex == 1){
                            SendNotification_Rex($cardName, $cardURL);
                        }

                        if($forLeeYing == 1){
                            SendNotification_LeeYing($cardName, $cardURL);
                        }

                        if($forShah == 1){
                            SendNotification_Shah($cardName, $cardURL);
                        }

                        if($forMrsSuhana == 1){
                            SendNotification_Suhana($cardName, $cardURL);
                        }
                    }
                }
            }
            $i++;
        }
    }

    //get data from sound list && fix
    function getDataFromSoundList(){
        
        $APIURI     = 'https://api.trello.com/1/lists/5c33089944403668623df872/cards?cards=visible&card_member_fields=all&key=cee15f8c5b0d25a32783f5139b64edb4&token=0a9207e773917d4738910c973994011eea5bc955f93e59cdff9d1b7f1ab8776d';

        $init       = curl_init();
        curl_setopt($init, CURLOPT_URL, $APIURI); //Url together with parameters
        curl_setopt($init, CURLOPT_RETURNTRANSFER, 1); //Return data instead printing directly in Browser
        curl_setopt($init, CURLOPT_CONNECTTIMEOUT , 7); //Timeout after 7 seconds
        curl_setopt($init, CURLOPT_USERAGENT , "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
        curl_setopt($init, CURLOPT_HEADER, 0);

        $exec       = curl_exec($init);
        $close      = curl_close($init);

        $decodeJSON = json_decode($exec, true);
        $countJsonArray  = count($decodeJSON);
        // echo $countJsonArray;
        $i = 0;
        while($i < $countJsonArray){
            //parsing "labels" array
            foreach($decodeJSON[$i]['labels'] as $labelsData){
                $dataLabel      = $labelsData['name'];
                //conditional to filter "new" label
                if($dataLabel == 'NEW'){
                    $dataMMB    = $decodeJSON[$i]['idMembers'];
                    //this data for card name and card url
                    $cardName   = $decodeJSON[$i]['name'];
                    $cardURL    = $decodeJSON[$i]['url'];
                    
                    //if found the data member > 1
                    if(count($dataMMB) > 1){                            
                        $id_member1 = $dataMMB[0];
                        $id_member2 = $dataMMB[1];
                        $id_member3 = $dataMMB[2];
                        $id_member4 = $dataMMB[3];
                        $id_member5 = $dataMMB[4];
                        $id_member6 = $dataMMB[5];
                        $id_member7 = $dataMMB[6];
                        $id_member8 = $dataMMB[7];
                        $id_member9 = $dataMMB[8];
                        $id_member10 = $dataMMB[9];
                        
                        //arief
                        if($id_member1 == '5959c36a55c57339a774ff8c' || $id_member2 == '5959c36a55c57339a774ff8c' || $id_member3 == '5959c36a55c57339a774ff8c' || $id_member4 == '5959c36a55c57339a774ff8c'
                        || $id_member5 == '5959c36a55c57339a774ff8c' || $id_member6 == '5959c36a55c57339a774ff8c' || $id_member7 == '5959c36a55c57339a774ff8c' || $id_member8 == '5959c36a55c57339a774ff8c'
                        || $id_member9 == '5959c36a55c57339a774ff8c' || $id_member10 == '5959c36a55c57339a774ff8c'){
                            $forArief_mn   = 1;
                        } 
                        //azmi
                        if($id_member1 == '58b66821144d4b68a5ab1c84' || $id_member2 == '58b66821144d4b68a5ab1c84' || $id_member3 == '58b66821144d4b68a5ab1c84' || $id_member4 == '58b66821144d4b68a5ab1c84'
                        || $id_member5 == '58b66821144d4b68a5ab1c84' || $id_member6 == '58b66821144d4b68a5ab1c84' || $id_member7 == '58b66821144d4b68a5ab1c84' || $id_member8 == '58b66821144d4b68a5ab1c84'
                        || $id_member9 == '58b66821144d4b68a5ab1c84' || $id_member10 == '58b66821144d4b68a5ab1c84'){
                            $forAzmi_mn    = 1;
                        }
                        //alex
                        if($id_member1 == '5b70ef61add7a23d75e11a44' || $id_member2 == '5b70ef61add7a23d75e11a44' || $id_member3 == '5b70ef61add7a23d75e11a44' || $id_member4 == '5b70ef61add7a23d75e11a44'
                        || $id_member5 == '5b70ef61add7a23d75e11a44' || $id_member6 == '5b70ef61add7a23d75e11a44' || $id_member7 == '5b70ef61add7a23d75e11a44' || $id_member8 == '5b70ef61add7a23d75e11a44'
                        || $id_member9 == '5b70ef61add7a23d75e11a44' || $id_member10 == '5b70ef61add7a23d75e11a44'){
                            $forAlex_mn    = 1;
                        }
                        //lee jen ling
                        if($id_member1 == '5c3bf62e0581ed89ee7b6580' || $id_member2 == '5c3bf62e0581ed89ee7b6580' || $id_member3 == '5c3bf62e0581ed89ee7b6580' || $id_member4 == '5c3bf62e0581ed89ee7b6580'
                        || $id_member5 == '5c3bf62e0581ed89ee7b6580' || $id_member6 == '5c3bf62e0581ed89ee7b6580' || $id_member7 == '5c3bf62e0581ed89ee7b6580' || $id_member8 == '5c3bf62e0581ed89ee7b6580'
                        || $id_member9 == '5c3bf62e0581ed89ee7b6580' || $id_member10 == '5c3bf62e0581ed89ee7b6580'){
                            $forLeeJenLing_mn    = 1;
                        }
                        //priya
                        if($id_member1 == '5b70f173191434079ebf6420' || $id_member2 == '5b70f173191434079ebf6420' || $id_member3 == '5b70f173191434079ebf6420' || $id_member4 == '5b70f173191434079ebf6420'
                        || $id_member5 == '5b70f173191434079ebf6420' || $id_member6 == '5b70f173191434079ebf6420' || $id_member7 == '5b70f173191434079ebf6420' || $id_member8 == '5b70f173191434079ebf6420'
                        || $id_member9 == '5b70f173191434079ebf6420' || $id_member10 == '5b70f173191434079ebf6420'){
                            $forPriya_mn    = 1;
                        }
                        //jeremy
                        if($id_member1 == '559cce487e609257b9b668e8' || $id_member2 == '559cce487e609257b9b668e8' || $id_member3 == '559cce487e609257b9b668e8' || $id_member4 == '559cce487e609257b9b668e8'
                        || $id_member5 == '559cce487e609257b9b668e8' || $id_member6 == '559cce487e609257b9b668e8' || $id_member7 == '559cce487e609257b9b668e8' || $id_member8 == '559cce487e609257b9b668e8'
                        || $id_member9 == '559cce487e609257b9b668e8' || $id_member10 == '559cce487e609257b9b668e8'){
                            $forJeremy_mn    = 1;
                        }
                        //rex
                        if($id_member1 == '5c3bf7a26814db37f6b41ff2' || $id_member2 == '5c3bf7a26814db37f6b41ff2' || $id_member3 == '5c3bf7a26814db37f6b41ff2' || $id_member4 == '5c3bf7a26814db37f6b41ff2'
                        || $id_member5 == '5c3bf7a26814db37f6b41ff2' || $id_member6 == '5c3bf7a26814db37f6b41ff2' || $id_member7 == '5c3bf7a26814db37f6b41ff2' || $id_member8 == '5c3bf7a26814db37f6b41ff2'
                        || $id_member9 == '5c3bf7a26814db37f6b41ff2' || $id_member10 == '5c3bf7a26814db37f6b41ff2'){
                            $forRex_mn    = 1;
                        }
                        //lee ying
                        if($id_member1 == '590c33ec597ce7d26734f355' || $id_member2 == '590c33ec597ce7d26734f355' || $id_member3 == '590c33ec597ce7d26734f355' || $id_member4 == '590c33ec597ce7d26734f355'
                        || $id_member5 == '590c33ec597ce7d26734f355' || $id_member6 == '590c33ec597ce7d26734f355' || $id_member7 == '590c33ec597ce7d26734f355' || $id_member8 == '590c33ec597ce7d26734f355'
                        || $id_member9 == '590c33ec597ce7d26734f355' || $id_member10 == '590c33ec597ce7d26734f355'){
                            $forLeeYing_mn    = 1;
                        }
                        //shah
                        if($id_member1 == '58acfc97f4b21ebff852f886' || $id_member2 == '58acfc97f4b21ebff852f886' || $id_member3 == '58acfc97f4b21ebff852f886' || $id_member4 == '58acfc97f4b21ebff852f886'
                        || $id_member5 == '58acfc97f4b21ebff852f886' || $id_member6 == '58acfc97f4b21ebff852f886' || $id_member7 == '58acfc97f4b21ebff852f886' || $id_member8 == '58acfc97f4b21ebff852f886'
                        || $id_member9 == '58acfc97f4b21ebff852f886' || $id_member10 == '58acfc97f4b21ebff852f886'){
                            $forShah_mn    = 1;
                        }
                        //mrs. suhana
                        if($id_member1 == '58abdb1a88c1ba56168f5d9e' || $id_member2 == '58abdb1a88c1ba56168f5d9e' || $id_member3 == '58abdb1a88c1ba56168f5d9e' || $id_member4 == '58abdb1a88c1ba56168f5d9e'
                        || $id_member5 == '58abdb1a88c1ba56168f5d9e' || $id_member6 == '58abdb1a88c1ba56168f5d9e' || $id_member7 == '58abdb1a88c1ba56168f5d9e' || $id_member8 == '58abdb1a88c1ba56168f5d9e'
                        || $id_member9 == '58abdb1a88c1ba56168f5d9e' || $id_member10 == '58abdb1a88c1ba56168f5d9e'){
                            $forMrsSuhana_mn    = 1;
                        }                        

                        //send notification
                        if($forArief_mn == 1){
                            SendNotification_Arief($cardName, $cardURL);
                        }

                        if($forAzmi_mn == 1){
                            SendNotification_Azmi($cardName, $cardURL);
                        }

                        if($forAlex_mn == 1){
                            SendNotification_Alex($cardName, $cardURL);
                        }

                        if($forLeeJenLing_mn == 1){
                            SendNotification_LeeJenLing($cardName, $cardURL);
                        }

                        if($forPriya_mn == 1){
                            SendNotification_Priya($cardName, $cardURL);
                        }

                        if($forJeremy_mn == 1){
                            SendNotification_Jeremy($cardName, $cardURL);
                        }

                        if($forRex_mn == 1){
                            SendNotification_Rex($cardName, $cardURL);
                        }

                        if($forLeeYing_mn == 1){
                            SendNotification_LeeYing($cardName, $cardURL);
                        }

                        if($forShah_mn == 1){
                            SendNotification_Shah($cardName, $cardURL);
                        }

                        if($forMrsSuhana_mn == 1){
                            SendNotification_Suhana($cardName, $cardURL);
                        }
                        
                    } else {
                        $id_member    = $dataMMB[0];

                        //check which memberID
                        if($id_member == '5959c36a55c57339a774ff8c'){
                            $forArief   = 1;
                        } 
                        if($id_member == '58b66821144d4b68a5ab1c84'){
                            $forAzmi    = 1;
                        } 
                        if($id_member == '5b70ef61add7a23d75e11a44'){
                            $forAlex    = 1;
                        } 
                        if($id_member == '5c3bf62e0581ed89ee7b6580'){
                            $forLeeJenLing    = 1;
                        } 
                        if($id_member == '5b70f173191434079ebf6420'){
                            $forPriya   = 1;
                        } 
                        if($id_member == '559cce487e609257b9b668e8'){
                            $forJeremy  = 1;
                        } 
                        if($id_member == '5c3bf7a26814db37f6b41ff2'){
                            $forRex     = 1;
                        } 
                        if($id_member == '590c33ec597ce7d26734f355'){
                            $forLeeYing       = 1;
                        } 
                        if($id_member == '58acfc97f4b21ebff852f886'){
                            $forShah    = 1;
                        } 
                        if($id_member == '58abdb1a88c1ba56168f5d9e'){
                            $forMrsSuhana     = 1;
                        } 

                        //send notification
                        if($forArief == 1){
                            SendNotification_Arief($cardName, $cardURL);
                        }

                        if($forAzmi == 1){
                            SendNotification_Azmi($cardName, $cardURL);
                        }

                        if($forAlex == 1){
                            SendNotification_Alex($cardName, $cardURL);
                        }

                        if($forLeeJenLing == 1){
                            SendNotification_LeeJenLing($cardName, $cardURL);
                        }

                        if($forPriya == 1){
                            SendNotification_Priya($cardName, $cardURL);
                        }

                        if($forJeremy == 1){
                            SendNotification_Jeremy($cardName, $cardURL);
                        }

                        if($forRex == 1){
                            SendNotification_Rex($cardName, $cardURL);
                        }

                        if($forLeeYing == 1){
                            SendNotification_LeeYing($cardName, $cardURL);
                        }

                        if($forShah == 1){
                            SendNotification_Shah($cardName, $cardURL);
                        }

                        if($forMrsSuhana == 1){
                            SendNotification_Suhana($cardName, $cardURL);
                        }
                    }
                }
            }
            $i++;
        }
    }

    //get data from operation list && fix
    function getDataFromOperationList(){
        
        $APIURI     = 'https://api.trello.com/1/lists/5c3308a05bf0e452fb5f1480/cards?cards=visible&card_member_fields=all&key=cee15f8c5b0d25a32783f5139b64edb4&token=0a9207e773917d4738910c973994011eea5bc955f93e59cdff9d1b7f1ab8776d';

        $init       = curl_init();
        curl_setopt($init, CURLOPT_URL, $APIURI); //Url together with parameters
        curl_setopt($init, CURLOPT_RETURNTRANSFER, 1); //Return data instead printing directly in Browser
        curl_setopt($init, CURLOPT_CONNECTTIMEOUT , 7); //Timeout after 7 seconds
        curl_setopt($init, CURLOPT_USERAGENT , "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
        curl_setopt($init, CURLOPT_HEADER, 0);

        $exec       = curl_exec($init);
        $close      = curl_close($init);

        $decodeJSON = json_decode($exec, true);
        $countJsonArray  = count($decodeJSON);
        // echo $countJsonArray;
        $i = 0;
        while($i < $countJsonArray){
            //parsing "labels" array
            foreach($decodeJSON[$i]['labels'] as $labelsData){
                $dataLabel      = $labelsData['name'];
                //conditional to filter "new" label
                if($dataLabel == 'NEW'){
                    $dataMMB    = $decodeJSON[$i]['idMembers'];
                    //this data for card name and card url
                    $cardName   = $decodeJSON[$i]['name'];
                    $cardURL    = $decodeJSON[$i]['url'];
                    
                    //if found the data member > 1
                    if(count($dataMMB) > 1){                            
                        $id_member1 = $dataMMB[0];
                        $id_member2 = $dataMMB[1];
                        $id_member3 = $dataMMB[2];
                        $id_member4 = $dataMMB[3];
                        $id_member5 = $dataMMB[4];
                        $id_member6 = $dataMMB[5];
                        $id_member7 = $dataMMB[6];
                        $id_member8 = $dataMMB[7];
                        $id_member9 = $dataMMB[8];
                        $id_member10 = $dataMMB[9];
                        
                        //arief
                        if($id_member1 == '5959c36a55c57339a774ff8c' || $id_member2 == '5959c36a55c57339a774ff8c' || $id_member3 == '5959c36a55c57339a774ff8c' || $id_member4 == '5959c36a55c57339a774ff8c'
                        || $id_member5 == '5959c36a55c57339a774ff8c' || $id_member6 == '5959c36a55c57339a774ff8c' || $id_member7 == '5959c36a55c57339a774ff8c' || $id_member8 == '5959c36a55c57339a774ff8c'
                        || $id_member9 == '5959c36a55c57339a774ff8c' || $id_member10 == '5959c36a55c57339a774ff8c'){
                            $forArief_mn   = 1;
                        } 
                        //azmi
                        if($id_member1 == '58b66821144d4b68a5ab1c84' || $id_member2 == '58b66821144d4b68a5ab1c84' || $id_member3 == '58b66821144d4b68a5ab1c84' || $id_member4 == '58b66821144d4b68a5ab1c84'
                        || $id_member5 == '58b66821144d4b68a5ab1c84' || $id_member6 == '58b66821144d4b68a5ab1c84' || $id_member7 == '58b66821144d4b68a5ab1c84' || $id_member8 == '58b66821144d4b68a5ab1c84'
                        || $id_member9 == '58b66821144d4b68a5ab1c84' || $id_member10 == '58b66821144d4b68a5ab1c84'){
                            $forAzmi_mn    = 1;
                        }
                        //alex
                        if($id_member1 == '5b70ef61add7a23d75e11a44' || $id_member2 == '5b70ef61add7a23d75e11a44' || $id_member3 == '5b70ef61add7a23d75e11a44' || $id_member4 == '5b70ef61add7a23d75e11a44'
                        || $id_member5 == '5b70ef61add7a23d75e11a44' || $id_member6 == '5b70ef61add7a23d75e11a44' || $id_member7 == '5b70ef61add7a23d75e11a44' || $id_member8 == '5b70ef61add7a23d75e11a44'
                        || $id_member9 == '5b70ef61add7a23d75e11a44' || $id_member10 == '5b70ef61add7a23d75e11a44'){
                            $forAlex_mn    = 1;
                        }
                        //lee jen ling
                        if($id_member1 == '5c3bf62e0581ed89ee7b6580' || $id_member2 == '5c3bf62e0581ed89ee7b6580' || $id_member3 == '5c3bf62e0581ed89ee7b6580' || $id_member4 == '5c3bf62e0581ed89ee7b6580'
                        || $id_member5 == '5c3bf62e0581ed89ee7b6580' || $id_member6 == '5c3bf62e0581ed89ee7b6580' || $id_member7 == '5c3bf62e0581ed89ee7b6580' || $id_member8 == '5c3bf62e0581ed89ee7b6580'
                        || $id_member9 == '5c3bf62e0581ed89ee7b6580' || $id_member10 == '5c3bf62e0581ed89ee7b6580'){
                            $forLeeJenLing_mn    = 1;
                        }
                        //priya
                        if($id_member1 == '5b70f173191434079ebf6420' || $id_member2 == '5b70f173191434079ebf6420' || $id_member3 == '5b70f173191434079ebf6420' || $id_member4 == '5b70f173191434079ebf6420'
                        || $id_member5 == '5b70f173191434079ebf6420' || $id_member6 == '5b70f173191434079ebf6420' || $id_member7 == '5b70f173191434079ebf6420' || $id_member8 == '5b70f173191434079ebf6420'
                        || $id_member9 == '5b70f173191434079ebf6420' || $id_member10 == '5b70f173191434079ebf6420'){
                            $forPriya_mn    = 1;
                        }
                        //jeremy
                        if($id_member1 == '559cce487e609257b9b668e8' || $id_member2 == '559cce487e609257b9b668e8' || $id_member3 == '559cce487e609257b9b668e8' || $id_member4 == '559cce487e609257b9b668e8'
                        || $id_member5 == '559cce487e609257b9b668e8' || $id_member6 == '559cce487e609257b9b668e8' || $id_member7 == '559cce487e609257b9b668e8' || $id_member8 == '559cce487e609257b9b668e8'
                        || $id_member9 == '559cce487e609257b9b668e8' || $id_member10 == '559cce487e609257b9b668e8'){
                            $forJeremy_mn    = 1;
                        }
                        //rex
                        if($id_member1 == '5c3bf7a26814db37f6b41ff2' || $id_member2 == '5c3bf7a26814db37f6b41ff2' || $id_member3 == '5c3bf7a26814db37f6b41ff2' || $id_member4 == '5c3bf7a26814db37f6b41ff2'
                        || $id_member5 == '5c3bf7a26814db37f6b41ff2' || $id_member6 == '5c3bf7a26814db37f6b41ff2' || $id_member7 == '5c3bf7a26814db37f6b41ff2' || $id_member8 == '5c3bf7a26814db37f6b41ff2'
                        || $id_member9 == '5c3bf7a26814db37f6b41ff2' || $id_member10 == '5c3bf7a26814db37f6b41ff2'){
                            $forRex_mn    = 1;
                        }
                        //lee ying
                        if($id_member1 == '590c33ec597ce7d26734f355' || $id_member2 == '590c33ec597ce7d26734f355' || $id_member3 == '590c33ec597ce7d26734f355' || $id_member4 == '590c33ec597ce7d26734f355'
                        || $id_member5 == '590c33ec597ce7d26734f355' || $id_member6 == '590c33ec597ce7d26734f355' || $id_member7 == '590c33ec597ce7d26734f355' || $id_member8 == '590c33ec597ce7d26734f355'
                        || $id_member9 == '590c33ec597ce7d26734f355' || $id_member10 == '590c33ec597ce7d26734f355'){
                            $forLeeYing_mn    = 1;
                        }
                        //shah
                        if($id_member1 == '58acfc97f4b21ebff852f886' || $id_member2 == '58acfc97f4b21ebff852f886' || $id_member3 == '58acfc97f4b21ebff852f886' || $id_member4 == '58acfc97f4b21ebff852f886'
                        || $id_member5 == '58acfc97f4b21ebff852f886' || $id_member6 == '58acfc97f4b21ebff852f886' || $id_member7 == '58acfc97f4b21ebff852f886' || $id_member8 == '58acfc97f4b21ebff852f886'
                        || $id_member9 == '58acfc97f4b21ebff852f886' || $id_member10 == '58acfc97f4b21ebff852f886'){
                            $forShah_mn    = 1;
                        }
                        //mrs. suhana
                        if($id_member1 == '58abdb1a88c1ba56168f5d9e' || $id_member2 == '58abdb1a88c1ba56168f5d9e' || $id_member3 == '58abdb1a88c1ba56168f5d9e' || $id_member4 == '58abdb1a88c1ba56168f5d9e'
                        || $id_member5 == '58abdb1a88c1ba56168f5d9e' || $id_member6 == '58abdb1a88c1ba56168f5d9e' || $id_member7 == '58abdb1a88c1ba56168f5d9e' || $id_member8 == '58abdb1a88c1ba56168f5d9e'
                        || $id_member9 == '58abdb1a88c1ba56168f5d9e' || $id_member10 == '58abdb1a88c1ba56168f5d9e'){
                            $forMrsSuhana_mn    = 1;
                        }                        

                        //send notification
                        if($forArief_mn == 1){
                            SendNotification_Arief($cardName, $cardURL);
                        }

                        if($forAzmi_mn == 1){
                            SendNotification_Azmi($cardName, $cardURL);
                        }

                        if($forAlex_mn == 1){
                            SendNotification_Alex($cardName, $cardURL);
                        }

                        if($forLeeJenLing_mn == 1){
                            SendNotification_LeeJenLing($cardName, $cardURL);
                        }

                        if($forPriya_mn == 1){
                            SendNotification_Priya($cardName, $cardURL);
                        }

                        if($forJeremy_mn == 1){
                            SendNotification_Jeremy($cardName, $cardURL);
                        }

                        if($forRex_mn == 1){
                            SendNotification_Rex($cardName, $cardURL);
                        }

                        if($forLeeYing_mn == 1){
                            SendNotification_LeeYing($cardName, $cardURL);
                        }

                        if($forShah_mn == 1){
                            SendNotification_Shah($cardName, $cardURL);
                        }

                        if($forMrsSuhana_mn == 1){
                            SendNotification_Suhana($cardName, $cardURL);
                        }
                        
                    } else {
                        $id_member    = $dataMMB[0];

                        //check which memberID
                        if($id_member == '5959c36a55c57339a774ff8c'){
                            $forArief   = 1;
                        } 
                        if($id_member == '58b66821144d4b68a5ab1c84'){
                            $forAzmi    = 1;
                        } 
                        if($id_member == '5b70ef61add7a23d75e11a44'){
                            $forAlex    = 1;
                        } 
                        if($id_member == '5c3bf62e0581ed89ee7b6580'){
                            $forLeeJenLing    = 1;
                        } 
                        if($id_member == '5b70f173191434079ebf6420'){
                            $forPriya   = 1;
                        } 
                        if($id_member == '559cce487e609257b9b668e8'){
                            $forJeremy  = 1;
                        } 
                        if($id_member == '5c3bf7a26814db37f6b41ff2'){
                            $forRex     = 1;
                        } 
                        if($id_member == '590c33ec597ce7d26734f355'){
                            $forLeeYing       = 1;
                        } 
                        if($id_member == '58acfc97f4b21ebff852f886'){
                            $forShah    = 1;
                        } 
                        if($id_member == '58abdb1a88c1ba56168f5d9e'){
                            $forMrsSuhana     = 1;
                        } 

                        //send notification
                        if($forArief == 1){
                            SendNotification_Arief($cardName, $cardURL);
                        }

                        if($forAzmi == 1){
                            SendNotification_Azmi($cardName, $cardURL);
                        }

                        if($forAlex == 1){
                            SendNotification_Alex($cardName, $cardURL);
                        }

                        if($forLeeJenLing == 1){
                            SendNotification_LeeJenLing($cardName, $cardURL);
                        }

                        if($forPriya == 1){
                            SendNotification_Priya($cardName, $cardURL);
                        }

                        if($forJeremy == 1){
                            SendNotification_Jeremy($cardName, $cardURL);
                        }

                        if($forRex == 1){
                            SendNotification_Rex($cardName, $cardURL);
                        }

                        if($forLeeYing == 1){
                            SendNotification_LeeYing($cardName, $cardURL);
                        }

                        if($forShah == 1){
                            SendNotification_Shah($cardName, $cardURL);
                        }

                        if($forMrsSuhana == 1){
                            SendNotification_Suhana($cardName, $cardURL);
                        }
                    }
                }
            }
            $i++;
        }
    }

    //get data from operation list && fix
    function getDataFromMarketingList(){
        
        $APIURI     = 'https://api.trello.com/1/lists/5c3308aa6fe4f68a28703bfa/cards?cards=visible&card_member_fields=all&key=cee15f8c5b0d25a32783f5139b64edb4&token=0a9207e773917d4738910c973994011eea5bc955f93e59cdff9d1b7f1ab8776d';

        $init       = curl_init();
        curl_setopt($init, CURLOPT_URL, $APIURI); //Url together with parameters
        curl_setopt($init, CURLOPT_RETURNTRANSFER, 1); //Return data instead printing directly in Browser
        curl_setopt($init, CURLOPT_CONNECTTIMEOUT , 7); //Timeout after 7 seconds
        curl_setopt($init, CURLOPT_USERAGENT , "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
        curl_setopt($init, CURLOPT_HEADER, 0);

        $exec       = curl_exec($init);
        $close      = curl_close($init);

        $decodeJSON = json_decode($exec, true);
        $countJsonArray  = count($decodeJSON);
        // echo $countJsonArray;
        $i = 0;
        while($i < $countJsonArray){
            //parsing "labels" array
            foreach($decodeJSON[$i]['labels'] as $labelsData){
                $dataLabel      = $labelsData['name'];
                //conditional to filter "new" label
                if($dataLabel == 'NEW'){
                    $dataMMB    = $decodeJSON[$i]['idMembers'];
                    //this data for card name and card url
                    $cardName   = $decodeJSON[$i]['name'];
                    $cardURL    = $decodeJSON[$i]['url'];
                    
                    //if found the data member > 1
                    if(count($dataMMB) > 1){                            
                        $id_member1 = $dataMMB[0];
                        $id_member2 = $dataMMB[1];
                        $id_member3 = $dataMMB[2];
                        $id_member4 = $dataMMB[3];
                        $id_member5 = $dataMMB[4];
                        $id_member6 = $dataMMB[5];
                        $id_member7 = $dataMMB[6];
                        $id_member8 = $dataMMB[7];
                        $id_member9 = $dataMMB[8];
                        $id_member10 = $dataMMB[9];
                        
                        //arief
                        if($id_member1 == '5959c36a55c57339a774ff8c' || $id_member2 == '5959c36a55c57339a774ff8c' || $id_member3 == '5959c36a55c57339a774ff8c' || $id_member4 == '5959c36a55c57339a774ff8c'
                        || $id_member5 == '5959c36a55c57339a774ff8c' || $id_member6 == '5959c36a55c57339a774ff8c' || $id_member7 == '5959c36a55c57339a774ff8c' || $id_member8 == '5959c36a55c57339a774ff8c'
                        || $id_member9 == '5959c36a55c57339a774ff8c' || $id_member10 == '5959c36a55c57339a774ff8c'){
                            $forArief_mn   = 1;
                        } 
                        //azmi
                        if($id_member1 == '58b66821144d4b68a5ab1c84' || $id_member2 == '58b66821144d4b68a5ab1c84' || $id_member3 == '58b66821144d4b68a5ab1c84' || $id_member4 == '58b66821144d4b68a5ab1c84'
                        || $id_member5 == '58b66821144d4b68a5ab1c84' || $id_member6 == '58b66821144d4b68a5ab1c84' || $id_member7 == '58b66821144d4b68a5ab1c84' || $id_member8 == '58b66821144d4b68a5ab1c84'
                        || $id_member9 == '58b66821144d4b68a5ab1c84' || $id_member10 == '58b66821144d4b68a5ab1c84'){
                            $forAzmi_mn    = 1;
                        }
                        //alex
                        if($id_member1 == '5b70ef61add7a23d75e11a44' || $id_member2 == '5b70ef61add7a23d75e11a44' || $id_member3 == '5b70ef61add7a23d75e11a44' || $id_member4 == '5b70ef61add7a23d75e11a44'
                        || $id_member5 == '5b70ef61add7a23d75e11a44' || $id_member6 == '5b70ef61add7a23d75e11a44' || $id_member7 == '5b70ef61add7a23d75e11a44' || $id_member8 == '5b70ef61add7a23d75e11a44'
                        || $id_member9 == '5b70ef61add7a23d75e11a44' || $id_member10 == '5b70ef61add7a23d75e11a44'){
                            $forAlex_mn    = 1;
                        }
                        //lee jen ling
                        if($id_member1 == '5c3bf62e0581ed89ee7b6580' || $id_member2 == '5c3bf62e0581ed89ee7b6580' || $id_member3 == '5c3bf62e0581ed89ee7b6580' || $id_member4 == '5c3bf62e0581ed89ee7b6580'
                        || $id_member5 == '5c3bf62e0581ed89ee7b6580' || $id_member6 == '5c3bf62e0581ed89ee7b6580' || $id_member7 == '5c3bf62e0581ed89ee7b6580' || $id_member8 == '5c3bf62e0581ed89ee7b6580'
                        || $id_member9 == '5c3bf62e0581ed89ee7b6580' || $id_member10 == '5c3bf62e0581ed89ee7b6580'){
                            $forLeeJenLing_mn    = 1;
                        }
                        //priya
                        if($id_member1 == '5b70f173191434079ebf6420' || $id_member2 == '5b70f173191434079ebf6420' || $id_member3 == '5b70f173191434079ebf6420' || $id_member4 == '5b70f173191434079ebf6420'
                        || $id_member5 == '5b70f173191434079ebf6420' || $id_member6 == '5b70f173191434079ebf6420' || $id_member7 == '5b70f173191434079ebf6420' || $id_member8 == '5b70f173191434079ebf6420'
                        || $id_member9 == '5b70f173191434079ebf6420' || $id_member10 == '5b70f173191434079ebf6420'){
                            $forPriya_mn    = 1;
                        }
                        //jeremy
                        if($id_member1 == '559cce487e609257b9b668e8' || $id_member2 == '559cce487e609257b9b668e8' || $id_member3 == '559cce487e609257b9b668e8' || $id_member4 == '559cce487e609257b9b668e8'
                        || $id_member5 == '559cce487e609257b9b668e8' || $id_member6 == '559cce487e609257b9b668e8' || $id_member7 == '559cce487e609257b9b668e8' || $id_member8 == '559cce487e609257b9b668e8'
                        || $id_member9 == '559cce487e609257b9b668e8' || $id_member10 == '559cce487e609257b9b668e8'){
                            $forJeremy_mn    = 1;
                        }
                        //rex
                        if($id_member1 == '5c3bf7a26814db37f6b41ff2' || $id_member2 == '5c3bf7a26814db37f6b41ff2' || $id_member3 == '5c3bf7a26814db37f6b41ff2' || $id_member4 == '5c3bf7a26814db37f6b41ff2'
                        || $id_member5 == '5c3bf7a26814db37f6b41ff2' || $id_member6 == '5c3bf7a26814db37f6b41ff2' || $id_member7 == '5c3bf7a26814db37f6b41ff2' || $id_member8 == '5c3bf7a26814db37f6b41ff2'
                        || $id_member9 == '5c3bf7a26814db37f6b41ff2' || $id_member10 == '5c3bf7a26814db37f6b41ff2'){
                            $forRex_mn    = 1;
                        }
                        //lee ying
                        if($id_member1 == '590c33ec597ce7d26734f355' || $id_member2 == '590c33ec597ce7d26734f355' || $id_member3 == '590c33ec597ce7d26734f355' || $id_member4 == '590c33ec597ce7d26734f355'
                        || $id_member5 == '590c33ec597ce7d26734f355' || $id_member6 == '590c33ec597ce7d26734f355' || $id_member7 == '590c33ec597ce7d26734f355' || $id_member8 == '590c33ec597ce7d26734f355'
                        || $id_member9 == '590c33ec597ce7d26734f355' || $id_member10 == '590c33ec597ce7d26734f355'){
                            $forLeeYing_mn    = 1;
                        }
                        //shah
                        if($id_member1 == '58acfc97f4b21ebff852f886' || $id_member2 == '58acfc97f4b21ebff852f886' || $id_member3 == '58acfc97f4b21ebff852f886' || $id_member4 == '58acfc97f4b21ebff852f886'
                        || $id_member5 == '58acfc97f4b21ebff852f886' || $id_member6 == '58acfc97f4b21ebff852f886' || $id_member7 == '58acfc97f4b21ebff852f886' || $id_member8 == '58acfc97f4b21ebff852f886'
                        || $id_member9 == '58acfc97f4b21ebff852f886' || $id_member10 == '58acfc97f4b21ebff852f886'){
                            $forShah_mn    = 1;
                        }
                        //mrs. suhana
                        if($id_member1 == '58abdb1a88c1ba56168f5d9e' || $id_member2 == '58abdb1a88c1ba56168f5d9e' || $id_member3 == '58abdb1a88c1ba56168f5d9e' || $id_member4 == '58abdb1a88c1ba56168f5d9e'
                        || $id_member5 == '58abdb1a88c1ba56168f5d9e' || $id_member6 == '58abdb1a88c1ba56168f5d9e' || $id_member7 == '58abdb1a88c1ba56168f5d9e' || $id_member8 == '58abdb1a88c1ba56168f5d9e'
                        || $id_member9 == '58abdb1a88c1ba56168f5d9e' || $id_member10 == '58abdb1a88c1ba56168f5d9e'){
                            $forMrsSuhana_mn    = 1;
                        }                        

                        //send notification
                        if($forArief_mn == 1){
                            SendNotification_Arief($cardName, $cardURL);
                        }

                        if($forAzmi_mn == 1){
                            SendNotification_Azmi($cardName, $cardURL);
                        }

                        if($forAlex_mn == 1){
                            SendNotification_Alex($cardName, $cardURL);
                        }

                        if($forLeeJenLing_mn == 1){
                            SendNotification_LeeJenLing($cardName, $cardURL);
                        }

                        if($forPriya_mn == 1){
                            SendNotification_Priya($cardName, $cardURL);
                        }

                        if($forJeremy_mn == 1){
                            SendNotification_Jeremy($cardName, $cardURL);
                        }

                        if($forRex_mn == 1){
                            SendNotification_Rex($cardName, $cardURL);
                        }

                        if($forLeeYing_mn == 1){
                            SendNotification_LeeYing($cardName, $cardURL);
                        }

                        if($forShah_mn == 1){
                            SendNotification_Shah($cardName, $cardURL);
                        }

                        if($forMrsSuhana_mn == 1){
                            SendNotification_Suhana($cardName, $cardURL);
                        }
                        
                    } else {
                        $id_member    = $dataMMB[0];

                        //check which memberID
                        if($id_member == '5959c36a55c57339a774ff8c'){
                            $forArief   = 1;
                        } 
                        if($id_member == '58b66821144d4b68a5ab1c84'){
                            $forAzmi    = 1;
                        } 
                        if($id_member == '5b70ef61add7a23d75e11a44'){
                            $forAlex    = 1;
                        } 
                        if($id_member == '5c3bf62e0581ed89ee7b6580'){
                            $forLeeJenLing    = 1;
                        } 
                        if($id_member == '5b70f173191434079ebf6420'){
                            $forPriya   = 1;
                        } 
                        if($id_member == '559cce487e609257b9b668e8'){
                            $forJeremy  = 1;
                        } 
                        if($id_member == '5c3bf7a26814db37f6b41ff2'){
                            $forRex     = 1;
                        } 
                        if($id_member == '590c33ec597ce7d26734f355'){
                            $forLeeYing       = 1;
                        } 
                        if($id_member == '58acfc97f4b21ebff852f886'){
                            $forShah    = 1;
                        } 
                        if($id_member == '58abdb1a88c1ba56168f5d9e'){
                            $forMrsSuhana     = 1;
                        } 

                        //send notification
                        if($forArief == 1){
                            SendNotification_Arief($cardName, $cardURL);
                        }

                        if($forAzmi == 1){
                            SendNotification_Azmi($cardName, $cardURL);
                        }

                        if($forAlex == 1){
                            SendNotification_Alex($cardName, $cardURL);
                        }

                        if($forLeeJenLing == 1){
                            SendNotification_LeeJenLing($cardName, $cardURL);
                        }

                        if($forPriya == 1){
                            SendNotification_Priya($cardName, $cardURL);
                        }

                        if($forJeremy == 1){
                            SendNotification_Jeremy($cardName, $cardURL);
                        }

                        if($forRex == 1){
                            SendNotification_Rex($cardName, $cardURL);
                        }

                        if($forLeeYing == 1){
                            SendNotification_LeeYing($cardName, $cardURL);
                        }

                        if($forShah == 1){
                            SendNotification_Shah($cardName, $cardURL);
                        }

                        if($forMrsSuhana == 1){
                            SendNotification_Suhana($cardName, $cardURL);
                        }
                    }
                }
            }
            $i++;
        }
    }
?>