<?php
error_reporting(0);
//get data from development list
function GetDataOperationList(){
    $devListID  = '5c3308a05bf0e452fb5f1480';
    $key        = 'cee15f8c5b0d25a32783f5139b64edb4';
    $token      = '0a9207e773917d4738910c973994011eea5bc955f93e59cdff9d1b7f1ab8776d';
    $APIURI     = 'https://api.trello.com/1/lists/'.$devListID.'/cards?cards=visible&limit=50&card_member_fields=all&key='.$key.'&token='.$token;

    $init       = curl_init();
    curl_setopt($init, CURLOPT_URL, $APIURI); //Url together with parameters
    curl_setopt($init, CURLOPT_RETURNTRANSFER, 1); //Return data instead printing directly in Browser
    curl_setopt($init, CURLOPT_CONNECTTIMEOUT , 7); //Timeout after 7 seconds
    curl_setopt($init, CURLOPT_USERAGENT , "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
    curl_setopt($init, CURLOPT_HEADER, 0);

    $exec       = curl_exec($init);
    $close      = curl_close($init);

    $decodeJSON = json_decode($exec, true);
    $countJsonArray  = count($decodeJSON);
    
    $i = 0;
    while($i < $countJsonArray){
        // echo "data - ".$i.'<br>';
        foreach($decodeJSON[$i]['labels'] as $labelsData){
            //data  from labels array
            $labelName      = $labelsData['name'];
            $labelID        = $labelsData['id'];
            $labelColor     = $labelsData['color'];
            // echo "label id: ".$labelID.' and label name: '.$labelName.'and label color: '.$labelColor.'<br>';
            
            //data from card json return
            $card_Name      = $decodeJSON[$i]['name'];
            $card_ID        = $decodeJSON[$i]['id'];
            $card_IdBoard   = $decodeJSON[$i]['idBoard'];           //get id board
            $card_IdList    = $decodeJSON[$i]['idList'];            //get id list
            // echo "card id: ".$card_ID.' and card name: '.$card_Name.'and carc ID_Board: '.$card_IdBoard.' and card ID_List: '.$card_IdList.'<br>';
            
            //conditional to filter "new" label
            if(strpos($labelName, 'WIP') !== false){
                //date setting
                date_default_timezone_set('Asia/Kuala_Lumpur');
                $createdAt      = date('Y-m-d H:i:s');
                $lastActivityRaw= date('Y-m-d');
                $date_now       = new DateTime();
                
                //connect to database
                global $mysqli;
                $mysqli = mysqli_connect('localhost', 'saraismi_arief', 'kaigan2019', 'saraismi_trelloSlackDayCounter');
                            
                //select data from db
                $sql_select             = "SELECT * FROM card_data WHERE id_card='$card_ID'";
                $query_select           = mysqli_query($mysqli, $sql_select);
                
                //if data found
                if(mysqli_num_rows($query_select) > 0){
                    // don't do anyting
                } else {
                    //insert data to db
                    $sql                = "INSERT INTO card_data (id_card, dateLastActivity, name, idBoard, idList, created_at, updated_at) 
                                            VALUES ('$card_ID', '$lastActivityRaw', '$card_Name', '$card_IdBoard', '$card_IdList', '$createdAt', '$createdAt')";
                    $query              = mysqli_query($mysqli, $sql);
                }

                //select dateLastActivity from db
                $getData            = "SELECT dateLastActivity FROM card_data WHERE id_card='$card_ID'";
                $query_exec         = mysqli_query($mysqli, $getData);
                
                foreach($query_exec as $hasil){
                    $dataResult     = $hasil['dateLastActivity'];
                    $dateResult     = new DateTime($dataResult);

                    //the result
                    $dayCounter     = $date_now->diff($dateResult)->format("%a");

                    if($dayCounter == 0){
                        //dont add any label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                    } else if($dayCounter == 1){
                        AddLabelOneDay($card_ID);
                        //delete other label
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                    } else if($dayCounter == 2){
                        AddLabelTwoDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 3){
                        AddLabelThreeDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 4){
                        AddLabelFourDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 5){
                        AddLabelFiveDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 6){
                        AddLabelSixDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 7){
                        AddLabelSevenDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    }else {
                        AddLabelOverSevenDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        
                    }
                }
            }
            
            if(strpos($labelName, 'Day 1') !== false){
                //date setting
                date_default_timezone_set('Asia/Kuala_Lumpur');
                $createdAt      = date('Y-m-d H:i:s');
                $lastActivityRaw= date('Y-m-d');
                $date_now       = new DateTime();

                //connect to database
                global $mysqli;
                $mysqli = mysqli_connect('localhost', 'saraismi_arief', 'kaigan2019', 'saraismi_trelloSlackDayCounter');
                
                //select dateLastActivity from db
                $getData            = "SELECT dateLastActivity FROM card_data WHERE id_card='$card_ID'";
                $query_exec         = mysqli_query($mysqli, $getData);
                
                foreach($query_exec as $hasil){
                    $dataResult     = $hasil['dateLastActivity'];
                    $dateResult     = new DateTime($dataResult);

                    //the result
                    $dayCounter     = $date_now->diff($dateResult)->format("%a");

                    if($dayCounter == 0){
                        //dont add any label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                    } else if($dayCounter == 1){
                        AddLabelOneDay($card_ID);
                        //delete other label
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                    } else if($dayCounter == 2){
                        AddLabelTwoDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 3){
                        AddLabelThreeDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 4){
                        AddLabelFourDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 5){
                        AddLabelFiveDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 6){
                        AddLabelSixDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 7){
                        AddLabelSevenDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    }else {
                        AddLabelOverSevenDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        
                    }
                }
            } 

            if(strpos($labelName, 'Day 2') !== false){
                //date setting
                date_default_timezone_set('Asia/Kuala_Lumpur');
                $createdAt      = date('Y-m-d H:i:s');
                $lastActivityRaw= date('Y-m-d');
                $date_now       = new DateTime();

                //connect to database
                global $mysqli;
                $mysqli = mysqli_connect('localhost', 'saraismi_arief', 'kaigan2019', 'saraismi_trelloSlackDayCounter');

                //select dateLastActivity from db
                $getData            = "SELECT dateLastActivity FROM card_data WHERE id_card='$card_ID'";
                $query_exec         = mysqli_query($mysqli, $getData);
                
                foreach($query_exec as $hasil){
                    $dataResult     = $hasil['dateLastActivity'];
                    $dateResult     = new DateTime($dataResult);

                    //the result
                    $dayCounter     = $date_now->diff($dateResult)->format("%a");

                    if($dayCounter == 0){
                        //dont add any label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                    } else if($dayCounter == 1){
                        AddLabelOneDay($card_ID);
                        //delete other label
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                    } else if($dayCounter == 2){
                        AddLabelTwoDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 3){
                        AddLabelThreeDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 4){
                        AddLabelFourDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 5){
                        AddLabelFiveDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 6){
                        AddLabelSixDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 7){
                        AddLabelSevenDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    }else {
                        AddLabelOverSevenDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        
                    }
                }
            } 

            if(strpos($labelName, 'Day 3') !== false){
                //date setting
                date_default_timezone_set('Asia/Kuala_Lumpur');
                $createdAt      = date('Y-m-d H:i:s');
                $lastActivityRaw= date('Y-m-d');
                $date_now       = new DateTime();
                
                //connect to database
                global $mysqli;
                $mysqli = mysqli_connect('localhost', 'saraismi_arief', 'kaigan2019', 'saraismi_trelloSlackDayCounter');

                //select dateLastActivity from db
                $getData            = "SELECT dateLastActivity FROM card_data WHERE id_card='$card_ID'";
                $query_exec         = mysqli_query($mysqli, $getData);
                
                foreach($query_exec as $hasil){
                    $dataResult     = $hasil['dateLastActivity'];
                    $dateResult     = new DateTime($dataResult);

                    //the result
                    $dayCounter     = $date_now->diff($dateResult)->format("%a");

                    if($dayCounter == 0){
                        //dont add any label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                    } else if($dayCounter == 1){
                        AddLabelOneDay($card_ID);
                        //delete other label
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                    } else if($dayCounter == 2){
                        AddLabelTwoDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 3){
                        AddLabelThreeDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 4){
                        AddLabelFourDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 5){
                        AddLabelFiveDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 6){
                        AddLabelSixDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 7){
                        AddLabelSevenDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    }else {
                        AddLabelOverSevenDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        
                    }
                }
            } 

            if(strpos($labelName, 'Day 4') !== false){
                //date setting
                date_default_timezone_set('Asia/Kuala_Lumpur');
                $createdAt      = date('Y-m-d H:i:s');
                $lastActivityRaw= date('Y-m-d');
                $date_now       = new DateTime();
                
                //connect to database
                global $mysqli;
                $mysqli = mysqli_connect('localhost', 'saraismi_arief', 'kaigan2019', 'saraismi_trelloSlackDayCounter');

                //select dateLastActivity from db
                $getData            = "SELECT dateLastActivity FROM card_data WHERE id_card='$card_ID'";
                $query_exec         = mysqli_query($mysqli, $getData);
                
                foreach($query_exec as $hasil){
                    $dataResult     = $hasil['dateLastActivity'];
                    $dateResult     = new DateTime($dataResult);

                    //the result
                    $dayCounter     = $date_now->diff($dateResult)->format("%a");

                    if($dayCounter == 0){
                        //dont add any label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                    } else if($dayCounter == 1){
                        AddLabelOneDay($card_ID);
                        //delete other label
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                    } else if($dayCounter == 2){
                        AddLabelTwoDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 3){
                        AddLabelThreeDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 4){
                        AddLabelFourDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 5){
                        AddLabelFiveDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 6){
                        AddLabelSixDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 7){
                        AddLabelSevenDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    }else {
                        AddLabelOverSevenDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        
                    }
                }
            } 

            if(strpos($labelName, 'Day 5') !== false){
                //date setting
                date_default_timezone_set('Asia/Kuala_Lumpur');
                $createdAt      = date('Y-m-d H:i:s');
                $lastActivityRaw= date('Y-m-d');
                $date_now       = new DateTime();
                
                //connect to database
                global $mysqli;
                $mysqli = mysqli_connect('localhost', 'saraismi_arief', 'kaigan2019', 'saraismi_trelloSlackDayCounter');

                //select dateLastActivity from db
                $getData            = "SELECT dateLastActivity FROM card_data WHERE id_card='$card_ID'";
                $query_exec         = mysqli_query($mysqli, $getData);
                
                foreach($query_exec as $hasil){
                    $dataResult     = $hasil['dateLastActivity'];
                    $dateResult     = new DateTime($dataResult);

                    //the result
                    $dayCounter     = $date_now->diff($dateResult)->format("%a");

                    if($dayCounter == 0){
                        //dont add any label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                    } else if($dayCounter == 1){
                        AddLabelOneDay($card_ID);
                        //delete other label
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                    } else if($dayCounter == 2){
                        AddLabelTwoDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 3){
                        AddLabelThreeDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 4){
                        AddLabelFourDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 5){
                        AddLabelFiveDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 6){
                        AddLabelSixDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 7){
                        AddLabelSevenDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    }else {
                        AddLabelOverSevenDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        
                    }
                }
            } 

            if(strpos($labelName, 'Day 6') !== false){
                //date setting
                date_default_timezone_set('Asia/Kuala_Lumpur');
                $createdAt      = date('Y-m-d H:i:s');
                $lastActivityRaw= date('Y-m-d');
                $date_now       = new DateTime();
                
                //connect to database
                global $mysqli;
                $mysqli = mysqli_connect('localhost', 'saraismi_arief', 'kaigan2019', 'saraismi_trelloSlackDayCounter');

                //select dateLastActivity from db
                $getData            = "SELECT dateLastActivity FROM card_data WHERE id_card='$card_ID'";
                $query_exec         = mysqli_query($mysqli, $getData);
                
                foreach($query_exec as $hasil){
                    $dataResult     = $hasil['dateLastActivity'];
                    $dateResult     = new DateTime($dataResult);

                    //the result
                    $dayCounter     = $date_now->diff($dateResult)->format("%a");

                    if($dayCounter == 0){
                        //dont add any label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                    } else if($dayCounter == 1){
                        AddLabelOneDay($card_ID);
                        //delete other label
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                    } else if($dayCounter == 2){
                        AddLabelTwoDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 3){
                        AddLabelThreeDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 4){
                        AddLabelFourDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 5){
                        AddLabelFiveDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 6){
                        AddLabelSixDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 7){
                        AddLabelSevenDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    }else {
                        AddLabelOverSevenDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        
                    }
                }
            } 

            if(strpos($labelName, 'Day 7') !== false){
                //date setting
                date_default_timezone_set('Asia/Kuala_Lumpur');
                $createdAt      = date('Y-m-d H:i:s');
                $lastActivityRaw= date('Y-m-d');
                $date_now       = new DateTime();
                
                //connect to database
                global $mysqli;
                $mysqli = mysqli_connect('localhost', 'saraismi_arief', 'kaigan2019', 'saraismi_trelloSlackDayCounter');

                //select dateLastActivity from db
                $getData            = "SELECT dateLastActivity FROM card_data WHERE id_card='$card_ID'";
                $query_exec         = mysqli_query($mysqli, $getData);
                
                foreach($query_exec as $hasil){
                    $dataResult     = $hasil['dateLastActivity'];
                    $dateResult     = new DateTime($dataResult);

                    //the result
                    $dayCounter     = $date_now->diff($dateResult)->format("%a");

                    if($dayCounter == 0){
                        //dont add any label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                    } else if($dayCounter == 1){
                        AddLabelOneDay($card_ID);
                        //delete other label
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                    } else if($dayCounter == 2){
                        AddLabelTwoDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 3){
                        AddLabelThreeDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 4){
                        AddLabelFourDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 5){
                        AddLabelFiveDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 6){
                        AddLabelSixDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 7){
                        AddLabelSevenDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    }else {
                        AddLabelOverSevenDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        
                    }
                }
            } 

            if(strpos($labelName, 'Day 7+') !== false){
                //date setting
                date_default_timezone_set('Asia/Kuala_Lumpur');
                $createdAt      = date('Y-m-d H:i:s');
                $lastActivityRaw= date('Y-m-d');
                $date_now       = new DateTime();
                
                //connect to database
                global $mysqli;
                $mysqli = mysqli_connect('localhost', 'saraismi_arief', 'kaigan2019', 'saraismi_trelloSlackDayCounter');

                //select dateLastActivity from db
                $getData            = "SELECT dateLastActivity FROM card_data WHERE id_card='$card_ID'";
                $query_exec         = mysqli_query($mysqli, $getData);
                
                foreach($query_exec as $hasil){
                    $dataResult     = $hasil['dateLastActivity'];
                    $dateResult     = new DateTime($dataResult);

                    //the result
                    $dayCounter     = $date_now->diff($dateResult)->format("%a");

                    if($dayCounter == 0){
                        //dont add any label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                    } else if($dayCounter == 1){
                        AddLabelOneDay($card_ID);
                        //delete other label
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                    } else if($dayCounter == 2){
                        AddLabelTwoDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 3){
                        AddLabelThreeDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 4){
                        AddLabelFourDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 5){
                        AddLabelFiveDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 6){
                        AddLabelSixDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 7){
                        AddLabelSevenDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else {
                        AddLabelOverSevenDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                    }
                }
            } 
        }
        $i++;
    }
}

//for add label to trello card
    function AddLabelOneDay($cardID){
        $endpointURL    = 'https://api.trello.com/1/cards/'.$cardID.'/labels';
        $parameters     = array(
                "color" => "black",
                "name"  => "Day 1",
                "key"   => "cee15f8c5b0d25a32783f5139b64edb4",
                "token" => "0a9207e773917d4738910c973994011eea5bc955f93e59cdff9d1b7f1ab8776d"
        );
        $payload        = json_encode($parameters);

        // Prepare new cURL resource
        $ch = curl_init($endpointURL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);

        // Set HTTP Header for POST request 
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($payload))
        );
        
        // Submit the POST request
        $result = curl_exec($ch);
        
        // Close cURL session handle
        curl_close($ch);
    }

    function AddLabelTwoDays($cardID){
        $endpointURL    = 'https://api.trello.com/1/cards/'.$cardID.'/labels';
        $parameters     = array(
                "color" => "black",
                "name"  => "Day 2",
                "key"   => "cee15f8c5b0d25a32783f5139b64edb4",
                "token" => "0a9207e773917d4738910c973994011eea5bc955f93e59cdff9d1b7f1ab8776d"
        );
        $payload        = json_encode($parameters);

        // Prepare new cURL resource
        $ch = curl_init($endpointURL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);

        // Set HTTP Header for POST request 
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($payload))
        );
        
        // Submit the POST request
        $result = curl_exec($ch);
        
        // Close cURL session handle
        curl_close($ch);
    }

    function AddLabelThreeDays($cardID){
        $endpointURL    = 'https://api.trello.com/1/cards/'.$cardID.'/labels';
        $parameters     = array(
                "color" => "black",
                "name"  => "Day 3",
                "key"   => "cee15f8c5b0d25a32783f5139b64edb4",
                "token" => "0a9207e773917d4738910c973994011eea5bc955f93e59cdff9d1b7f1ab8776d"
        );
        $payload        = json_encode($parameters);

        // Prepare new cURL resource
        $ch = curl_init($endpointURL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);

        // Set HTTP Header for POST request 
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($payload))
        );
        
        // Submit the POST request
        $result = curl_exec($ch);
        
        // Close cURL session handle
        curl_close($ch);
    }

    function AddLabelFourDays($cardID){
        $endpointURL    = 'https://api.trello.com/1/cards/'.$cardID.'/labels';
        $parameters     = array(
                "color" => "black",
                "name"  => "Day 4",
                "key"   => "cee15f8c5b0d25a32783f5139b64edb4",
                "token" => "0a9207e773917d4738910c973994011eea5bc955f93e59cdff9d1b7f1ab8776d"
        );
        $payload        = json_encode($parameters);

        // Prepare new cURL resource
        $ch = curl_init($endpointURL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);

        // Set HTTP Header for POST request 
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($payload))
        );
        
        // Submit the POST request
        $result = curl_exec($ch);
        
        // Close cURL session handle
        curl_close($ch);
    }

    function AddLabelFiveDays($cardID){
        $endpointURL    = 'https://api.trello.com/1/cards/'.$cardID.'/labels';
        $parameters     = array(
                "color" => "black",
                "name"  => "Day 5",
                "key"   => "cee15f8c5b0d25a32783f5139b64edb4",
                "token" => "0a9207e773917d4738910c973994011eea5bc955f93e59cdff9d1b7f1ab8776d"

        );
        $payload        = json_encode($parameters);

        // Prepare new cURL resource
        $ch = curl_init($endpointURL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);

        // Set HTTP Header for POST request 
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($payload))
        );
        
        // Submit the POST request
        $result = curl_exec($ch);
        
        // Close cURL session handle
        curl_close($ch);
    }

    function AddLabelSixDays($cardID){
        $endpointURL    = 'https://api.trello.com/1/cards/'.$cardID.'/labels';
        $parameters     = array(
                "color" => "black",
                "name"  => "Day 6",
                "key"   => "cee15f8c5b0d25a32783f5139b64edb4",
                "token" => "0a9207e773917d4738910c973994011eea5bc955f93e59cdff9d1b7f1ab8776d"

        );
        $payload        = json_encode($parameters);

        // Prepare new cURL resource
        $ch = curl_init($endpointURL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);

        // Set HTTP Header for POST request 
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($payload))
        );
        
        // Submit the POST request
        $result = curl_exec($ch);
        
        // Close cURL session handle
        curl_close($ch);
    }

    function AddLabelSevenDays($cardID){
        $endpointURL    = 'https://api.trello.com/1/cards/'.$cardID.'/labels';
        $parameters     = array(
                "color" => "black",
                "name"  => "Day 7",
                "key"   => "cee15f8c5b0d25a32783f5139b64edb4",
                "token" => "0a9207e773917d4738910c973994011eea5bc955f93e59cdff9d1b7f1ab8776d"
        );
        $payload        = json_encode($parameters);

        // Prepare new cURL resource
        $ch = curl_init($endpointURL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);

        // Set HTTP Header for POST request 
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($payload))
        );
        
        // Submit the POST request
        $result = curl_exec($ch);
        
        // Close cURL session handle
        curl_close($ch);
    }

    function AddLabelOverSevenDays($cardID){
        $endpointURL    = 'https://api.trello.com/1/cards/'.$cardID.'/labels';
        $parameters     = array(
                "color" => "red",
                "name"  => "Day 7+",
                "key"   => "cee15f8c5b0d25a32783f5139b64edb4",
                "token" => "0a9207e773917d4738910c973994011eea5bc955f93e59cdff9d1b7f1ab8776d"
        );
        $payload        = json_encode($parameters);

        // Prepare new cURL resource
        $ch = curl_init($endpointURL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);

        // Set HTTP Header for POST request 
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($payload))
        );
        
        // Submit the POST request
        $result = curl_exec($ch);
        
        // Close cURL session handle
        curl_close($ch);
    }

//for delete label
    /*
        ID Labels
        ---------
        Day 1   = 5c74c9019ba3c5188015ba78
        Day 2   = 5c7647ba7ec0210584912079
        Day 3   = 5c76471dccccde7db60eccf8
        Day 4   = 5c60ea917fca4c37d2727cb8
        Day 5   = 5c60ea959541aa5f075a006e
        Day 6   = 5c60ea9a85179403e254b1a7
        Day 7   = 5c6f94033a3b483d5a382d01
        Day 7+  = 5c6e084be1b4e188b05753c5
    */
    function DeleteLabelDayOne($cardID){
        $labelID= '5c74c9019ba3c5188015ba78';
        $URL    = 'https://api.trello.com/1/cards/'.$cardID.'/idLabels/'.$labelID.'?key=cee15f8c5b0d25a32783f5139b64edb4&token=0a9207e773917d4738910c973994011eea5bc955f93e59cdff9d1b7f1ab8776d';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$URL);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        // curl_setopt($ch, CURLOPT_POSTFIELDS, $arrays);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $result = json_decode($result);
        curl_close($ch);
    }

    function DeleteLabelDayTwo($cardID){
        $labelID= '5c7647ba7ec0210584912079';
        $URL    = 'https://api.trello.com/1/cards/'.$cardID.'/idLabels/'.$labelID.'?key=cee15f8c5b0d25a32783f5139b64edb4&token=0a9207e773917d4738910c973994011eea5bc955f93e59cdff9d1b7f1ab8776d';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$URL);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        // curl_setopt($ch, CURLOPT_POSTFIELDS, $arrays);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $result = json_decode($result);
        curl_close($ch);
    }

    function DeleteLabelDayThree($cardID){
        $labelID= '5c76471dccccde7db60eccf8';
        $URL    = 'https://api.trello.com/1/cards/'.$cardID.'/idLabels/'.$labelID.'?key=cee15f8c5b0d25a32783f5139b64edb4&token=0a9207e773917d4738910c973994011eea5bc955f93e59cdff9d1b7f1ab8776d';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$URL);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        // curl_setopt($ch, CURLOPT_POSTFIELDS, $arrays);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $result = json_decode($result);
        curl_close($ch);
    }

    function DeleteLabelDayFour($cardID){
        $labelID= '5c60ea917fca4c37d2727cb8';
        $URL    = 'https://api.trello.com/1/cards/'.$cardID.'/idLabels/'.$labelID.'?key=cee15f8c5b0d25a32783f5139b64edb4&token=0a9207e773917d4738910c973994011eea5bc955f93e59cdff9d1b7f1ab8776d';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$URL);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        // curl_setopt($ch, CURLOPT_POSTFIELDS, $arrays);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $result = json_decode($result);
        curl_close($ch);
    }

    function DeleteLabelDayFive($cardID){
        $labelID= '5c60ea959541aa5f075a006e';
        $URL    = 'https://api.trello.com/1/cards/'.$cardID.'/idLabels/'.$labelID.'?key=cee15f8c5b0d25a32783f5139b64edb4&token=0a9207e773917d4738910c973994011eea5bc955f93e59cdff9d1b7f1ab8776d';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$URL);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        // curl_setopt($ch, CURLOPT_POSTFIELDS, $arrays);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $result = json_decode($result);
        curl_close($ch);
    }

    function DeleteLabelDaySix($cardID){
        $labelID= '5c60ea9a85179403e254b1a7';
        $URL    = 'https://api.trello.com/1/cards/'.$cardID.'/idLabels/'.$labelID.'?key=cee15f8c5b0d25a32783f5139b64edb4&token=0a9207e773917d4738910c973994011eea5bc955f93e59cdff9d1b7f1ab8776d';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$URL);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        // curl_setopt($ch, CURLOPT_POSTFIELDS, $arrays);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $result = json_decode($result);
        curl_close($ch);
    }

    function DeleteLabelDaySeven($cardID){
        $labelID= '5c6f94033a3b483d5a382d01';
        $URL    = 'https://api.trello.com/1/cards/'.$cardID.'/idLabels/'.$labelID.'?key=cee15f8c5b0d25a32783f5139b64edb4&token=0a9207e773917d4738910c973994011eea5bc955f93e59cdff9d1b7f1ab8776d';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$URL);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        // curl_setopt($ch, CURLOPT_POSTFIELDS, $arrays);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $result = json_decode($result);
        curl_close($ch);
    }

    function DeleteLabelDaySevenPlus($cardID){
        $labelID= '5c6e084be1b4e188b05753c5';
        $URL    = 'https://api.trello.com/1/cards/'.$cardID.'/idLabels/'.$labelID.'?key=cee15f8c5b0d25a32783f5139b64edb4&token=0a9207e773917d4738910c973994011eea5bc955f93e59cdff9d1b7f1ab8776d';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$URL);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        // curl_setopt($ch, CURLOPT_POSTFIELDS, $arrays);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $result = json_decode($result);
        curl_close($ch);
    }

//call main function    
GetDataOperationList();       //1
?>