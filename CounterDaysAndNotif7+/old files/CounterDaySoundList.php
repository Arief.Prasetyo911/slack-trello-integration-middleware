<?php 
error_reporting(0);
//get data from sound list
function GetDataSoundList(){
    $devListID  = '5c33089944403668623df872';
    $key        = 'cee15f8c5b0d25a32783f5139b64edb4';
    $token      = '0a9207e773917d4738910c973994011eea5bc955f93e59cdff9d1b7f1ab8776d';
    $APIURI     = 'https://api.trello.com/1/lists/'.$devListID.'/cards?cards=visible&card_member_fields=all&key='.$key.'&token='.$token;

    $init       = curl_init();
    curl_setopt($init, CURLOPT_URL, $APIURI); //Url together with parameters
    curl_setopt($init, CURLOPT_RETURNTRANSFER, 1); //Return data instead printing directly in Browser
    curl_setopt($init, CURLOPT_CONNECTTIMEOUT , 7); //Timeout after 7 seconds
    curl_setopt($init, CURLOPT_USERAGENT , "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
    curl_setopt($init, CURLOPT_HEADER, 0);

    $exec       = curl_exec($init);
    $close      = curl_close($init);

    $decodeJSON = json_decode($exec, true);
    $countJsonArray  = count($decodeJSON);
    
    $i = 0;
    while($i < $countJsonArray){
        // echo "data - ".$i.'<br>';
        foreach($decodeJSON[$i]['labels'] as $labelsData){
            //data  from labels array
            $labelName      = $labelsData['name'];
            $labelID        = $labelsData['id'];
            $labelColor     = $labelsData['color'];
            // echo "label id: ".$labelID.' and label name: '.$labelName.'and label color: '.$labelColor.'<br>';
            
            //data from card json return
            $card_Name      = $decodeJSON[$i]['name'];
            $card_ID        = $decodeJSON[$i]['id'];
            $card_IdBoard   = $decodeJSON[$i]['idBoard'];           //get id board
            $card_IdList    = $decodeJSON[$i]['idList'];            //get id list
            // echo "card id: ".$card_ID.' and card name: '.$card_Name.'and carc ID_Board: '.$card_IdBoard.' and card ID_List: '.$card_IdList.'<br>';
            
            //conditional to filter "new" label
            if(strpos($labelName, 'WIP') !== false){
                //date setting
                date_default_timezone_set('Asia/Kuala_Lumpur');
                $createdAt      = date('Y-m-d H:i:s');
                $lastActivityRaw= date('Y-m-d');
                $date_now       = new DateTime();
                
                //connect to database
                global $mysqli;
                $mysqli = mysqli_connect('localhost', 'saraismi_arief', 'kaigan2019', 'saraismi_trelloSlackDayCounter');
                            
                //select data from db
                $sql_select             = "SELECT * FROM card_data WHERE id_card='$card_ID'";
                $query_select           = mysqli_query($mysqli, $sql_select);
                
                //if data found
                if(mysqli_num_rows($query_select) > 0){
                    // don't do anyting
                } else {
                    //insert data to db
                    $sql                = "INSERT INTO card_data (id_card, dateLastActivity, name, idBoard, idList, created_at, updated_at) 
                                            VALUES ('$card_ID', '$lastActivityRaw', '$card_Name', '$card_IdBoard', '$card_IdList', '$createdAt', '$createdAt')";
                    $query              = mysqli_query($mysqli, $sql);
                }

                //select dateLastActivity from db
                $getData            = "SELECT dateLastActivity FROM card_data WHERE id_card='$card_ID'";
                $query_exec         = mysqli_query($mysqli, $getData);
                
                foreach($query_exec as $hasil){
                    $dataResult     = $hasil['dateLastActivity'];
                    $dateResult     = new DateTime($dataResult);

                    //the result
                    $dayCounter     = $date_now->diff($dateResult)->format("%a");

                    if($dayCounter == 0){
                        //dont add any label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                    } else if($dayCounter == 1){
                        AddLabelOneDay($card_ID);
                        //delete other label
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                    } else if($dayCounter == 2){
                        AddLabelTwoDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 3){
                        AddLabelThreeDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 4){
                        AddLabelFourDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 5){
                        AddLabelFiveDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 6){
                        AddLabelSixDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 7){
                        AddLabelSevenDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    }else {
                        AddLabelOverSevenDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        
                    }
                }
            }
            
            if(strpos($labelName, 'Day 1') !== false){
                //date setting
                date_default_timezone_set('Asia/Kuala_Lumpur');
                $createdAt      = date('Y-m-d H:i:s');
                $lastActivityRaw= date('Y-m-d');
                $date_now       = new DateTime();

                //connect to database
                global $mysqli;
                $mysqli = mysqli_connect('localhost', 'saraismi_arief', 'kaigan2019', 'saraismi_trelloSlackDayCounter');
                
                //select dateLastActivity from db
                $getData            = "SELECT dateLastActivity FROM card_data WHERE id_card='$card_ID'";
                $query_exec         = mysqli_query($mysqli, $getData);
                
                foreach($query_exec as $hasil){
                    $dataResult     = $hasil['dateLastActivity'];
                    $dateResult     = new DateTime($dataResult);

                    //the result
                    $dayCounter     = $date_now->diff($dateResult)->format("%a");

                    if($dayCounter == 0){
                        //dont add any label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                    } else if($dayCounter == 1){
                        AddLabelOneDay($card_ID);
                        //delete other label
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                    } else if($dayCounter == 2){
                        AddLabelTwoDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 3){
                        AddLabelThreeDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 4){
                        AddLabelFourDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 5){
                        AddLabelFiveDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 6){
                        AddLabelSixDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 7){
                        AddLabelSevenDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    }else {
                        AddLabelOverSevenDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        
                    }
                }
            } 

            if(strpos($labelName, 'Day 2') !== false){
                //date setting
                date_default_timezone_set('Asia/Kuala_Lumpur');
                $createdAt      = date('Y-m-d H:i:s');
                $lastActivityRaw= date('Y-m-d');
                $date_now       = new DateTime();

                //connect to database
                global $mysqli;
                $mysqli = mysqli_connect('localhost', 'saraismi_arief', 'kaigan2019', 'saraismi_trelloSlackDayCounter');

                //select dateLastActivity from db
                $getData            = "SELECT dateLastActivity FROM card_data WHERE id_card='$card_ID'";
                $query_exec         = mysqli_query($mysqli, $getData);
                
                foreach($query_exec as $hasil){
                    $dataResult     = $hasil['dateLastActivity'];
                    $dateResult     = new DateTime($dataResult);

                    //the result
                    $dayCounter     = $date_now->diff($dateResult)->format("%a");

                    if($dayCounter == 0){
                        //dont add any label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                    } else if($dayCounter == 1){
                        AddLabelOneDay($card_ID);
                        //delete other label
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                    } else if($dayCounter == 2){
                        AddLabelTwoDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 3){
                        AddLabelThreeDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 4){
                        AddLabelFourDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 5){
                        AddLabelFiveDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 6){
                        AddLabelSixDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 7){
                        AddLabelSevenDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    }else {
                        AddLabelOverSevenDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        
                    }
                }
            } 

            if(strpos($labelName, 'Day 3') !== false){
                //date setting
                date_default_timezone_set('Asia/Kuala_Lumpur');
                $createdAt      = date('Y-m-d H:i:s');
                $lastActivityRaw= date('Y-m-d');
                $date_now       = new DateTime();
                
                //connect to database
                global $mysqli;
                $mysqli = mysqli_connect('localhost', 'saraismi_arief', 'kaigan2019', 'saraismi_trelloSlackDayCounter');

                //select dateLastActivity from db
                $getData            = "SELECT dateLastActivity FROM card_data WHERE id_card='$card_ID'";
                $query_exec         = mysqli_query($mysqli, $getData);
                
                foreach($query_exec as $hasil){
                    $dataResult     = $hasil['dateLastActivity'];
                    $dateResult     = new DateTime($dataResult);

                    //the result
                    $dayCounter     = $date_now->diff($dateResult)->format("%a");

                    if($dayCounter == 0){
                        //dont add any label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                    } else if($dayCounter == 1){
                        AddLabelOneDay($card_ID);
                        //delete other label
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                    } else if($dayCounter == 2){
                        AddLabelTwoDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 3){
                        AddLabelThreeDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 4){
                        AddLabelFourDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 5){
                        AddLabelFiveDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 6){
                        AddLabelSixDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 7){
                        AddLabelSevenDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    }else {
                        AddLabelOverSevenDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        
                    }
                }
            } 

            if(strpos($labelName, 'Day 4') !== false){
                //date setting
                date_default_timezone_set('Asia/Kuala_Lumpur');
                $createdAt      = date('Y-m-d H:i:s');
                $lastActivityRaw= date('Y-m-d');
                $date_now       = new DateTime();
                
                //connect to database
                global $mysqli;
                $mysqli = mysqli_connect('localhost', 'saraismi_arief', 'kaigan2019', 'saraismi_trelloSlackDayCounter');

                //select dateLastActivity from db
                $getData            = "SELECT dateLastActivity FROM card_data WHERE id_card='$card_ID'";
                $query_exec         = mysqli_query($mysqli, $getData);
                
                foreach($query_exec as $hasil){
                    $dataResult     = $hasil['dateLastActivity'];
                    $dateResult     = new DateTime($dataResult);

                    //the result
                    $dayCounter     = $date_now->diff($dateResult)->format("%a");

                    if($dayCounter == 0){
                        //dont add any label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                    } else if($dayCounter == 1){
                        AddLabelOneDay($card_ID);
                        //delete other label
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                    } else if($dayCounter == 2){
                        AddLabelTwoDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 3){
                        AddLabelThreeDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 4){
                        AddLabelFourDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 5){
                        AddLabelFiveDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 6){
                        AddLabelSixDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 7){
                        AddLabelSevenDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    }else {
                        AddLabelOverSevenDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        
                    }
                }
            } 

            if(strpos($labelName, 'Day 5') !== false){
                //date setting
                date_default_timezone_set('Asia/Kuala_Lumpur');
                $createdAt      = date('Y-m-d H:i:s');
                $lastActivityRaw= date('Y-m-d');
                $date_now       = new DateTime();
                
                //connect to database
                global $mysqli;
                $mysqli = mysqli_connect('localhost', 'saraismi_arief', 'kaigan2019', 'saraismi_trelloSlackDayCounter');

                //select dateLastActivity from db
                $getData            = "SELECT dateLastActivity FROM card_data WHERE id_card='$card_ID'";
                $query_exec         = mysqli_query($mysqli, $getData);
                
                foreach($query_exec as $hasil){
                    $dataResult     = $hasil['dateLastActivity'];
                    $dateResult     = new DateTime($dataResult);

                    //the result
                    $dayCounter     = $date_now->diff($dateResult)->format("%a");

                    if($dayCounter == 0){
                        //dont add any label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                    } else if($dayCounter == 1){
                        AddLabelOneDay($card_ID);
                        //delete other label
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                    } else if($dayCounter == 2){
                        AddLabelTwoDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 3){
                        AddLabelThreeDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 4){
                        AddLabelFourDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 5){
                        AddLabelFiveDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 6){
                        AddLabelSixDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 7){
                        AddLabelSevenDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    }else {
                        AddLabelOverSevenDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        
                    }
                }
            } 

            if(strpos($labelName, 'Day 6') !== false){
                //date setting
                date_default_timezone_set('Asia/Kuala_Lumpur');
                $createdAt      = date('Y-m-d H:i:s');
                $lastActivityRaw= date('Y-m-d');
                $date_now       = new DateTime();
                
                //connect to database
                global $mysqli;
                $mysqli = mysqli_connect('localhost', 'saraismi_arief', 'kaigan2019', 'saraismi_trelloSlackDayCounter');

                //select dateLastActivity from db
                $getData            = "SELECT dateLastActivity FROM card_data WHERE id_card='$card_ID'";
                $query_exec         = mysqli_query($mysqli, $getData);
                
                foreach($query_exec as $hasil){
                    $dataResult     = $hasil['dateLastActivity'];
                    $dateResult     = new DateTime($dataResult);

                    //the result
                    $dayCounter     = $date_now->diff($dateResult)->format("%a");

                    if($dayCounter == 0){
                        //dont add any label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                    } else if($dayCounter == 1){
                        AddLabelOneDay($card_ID);
                        //delete other label
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                    } else if($dayCounter == 2){
                        AddLabelTwoDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 3){
                        AddLabelThreeDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 4){
                        AddLabelFourDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 5){
                        AddLabelFiveDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 6){
                        AddLabelSixDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 7){
                        AddLabelSevenDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    }else {
                        AddLabelOverSevenDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        
                    }
                }
            } 

            if(strpos($labelName, 'Day 7') !== false){
                //date setting
                date_default_timezone_set('Asia/Kuala_Lumpur');
                $createdAt      = date('Y-m-d H:i:s');
                $lastActivityRaw= date('Y-m-d');
                $date_now       = new DateTime();
                
                //connect to database
                global $mysqli;
                $mysqli = mysqli_connect('localhost', 'saraismi_arief', 'kaigan2019', 'saraismi_trelloSlackDayCounter');

                //select dateLastActivity from db
                $getData            = "SELECT dateLastActivity FROM card_data WHERE id_card='$card_ID'";
                $query_exec         = mysqli_query($mysqli, $getData);
                
                foreach($query_exec as $hasil){
                    $dataResult     = $hasil['dateLastActivity'];
                    $dateResult     = new DateTime($dataResult);

                    //the result
                    $dayCounter     = $date_now->diff($dateResult)->format("%a");

                    if($dayCounter == 0){
                        //dont add any label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                    } else if($dayCounter == 1){
                        AddLabelOneDay($card_ID);
                        //delete other label
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                    } else if($dayCounter == 2){
                        AddLabelTwoDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 3){
                        AddLabelThreeDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 4){
                        AddLabelFourDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 5){
                        AddLabelFiveDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 6){
                        AddLabelSixDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 7){
                        AddLabelSevenDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    }else {
                        AddLabelOverSevenDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        
                    }
                }
            } 

            if(strpos($labelName, 'Day 7+') !== false){
                //date setting
                date_default_timezone_set('Asia/Kuala_Lumpur');
                $createdAt      = date('Y-m-d H:i:s');
                $lastActivityRaw= date('Y-m-d');
                $date_now       = new DateTime();
                
                //connect to database
                global $mysqli;
                $mysqli = mysqli_connect('localhost', 'saraismi_arief', 'kaigan2019', 'saraismi_trelloSlackDayCounter');

                //select dateLastActivity from db
                $getData            = "SELECT dateLastActivity FROM card_data WHERE id_card='$card_ID'";
                $query_exec         = mysqli_query($mysqli, $getData);
                
                foreach($query_exec as $hasil){
                    $dataResult     = $hasil['dateLastActivity'];
                    $dateResult     = new DateTime($dataResult);

                    //the result
                    $dayCounter     = $date_now->diff($dateResult)->format("%a");

                    if($dayCounter == 0){
                        //dont add any label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                    } else if($dayCounter == 1){
                        AddLabelOneDay($card_ID);
                        //delete other label
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                    } else if($dayCounter == 2){
                        AddLabelTwoDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 3){
                        AddLabelThreeDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 4){
                        AddLabelFourDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 5){
                        AddLabelFiveDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 6){
                        AddLabelSixDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySeven($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else if($dayCounter == 7){
                        AddLabelSevenDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySevenPlus($card_ID);
                        
                    } else {
                        AddLabelOverSevenDays($card_ID);
                        //delete other label
                        DeleteLabelDayOne($card_ID);
                        DeleteLabelDayTwo($card_ID);
                        DeleteLabelDayThree($card_ID);
                        DeleteLabelDayFour($card_ID);
                        DeleteLabelDayFive($card_ID);
                        DeleteLabelDaySix($card_ID);
                        DeleteLabelDaySeven($card_ID);

                        $dataMMB    = $decodeJSON[$i]['idMembers'];
                        //this data for card name and card url
                        $cardName   = $decodeJSON[$i]['name'];
                        $cardURL    = $decodeJSON[$i]['url'];
                        
                        if(count($dataMMB) > 1){
                            $id_member1 = $dataMMB[0];
                            $id_member2 = $dataMMB[1];
                            $id_member3 = $dataMMB[2];
                            $id_member4 = $dataMMB[3];
                            $id_member5 = $dataMMB[4];
                            $id_member6 = $dataMMB[5];
                            $id_member7 = $dataMMB[6];
                            $id_member8 = $dataMMB[7];
                            $id_member9 = $dataMMB[8];
                            $id_member10 = $dataMMB[9];

                            //arief
                            if($id_member1 == '5959c36a55c57339a774ff8c' || $id_member2 == '5959c36a55c57339a774ff8c' || $id_member3 == '5959c36a55c57339a774ff8c' || $id_member4 == '5959c36a55c57339a774ff8c'
                            || $id_member5 == '5959c36a55c57339a774ff8c' || $id_member6 == '5959c36a55c57339a774ff8c' || $id_member7 == '5959c36a55c57339a774ff8c' || $id_member8 == '5959c36a55c57339a774ff8c'
                            || $id_member9 == '5959c36a55c57339a774ff8c' || $id_member10 == '5959c36a55c57339a774ff8c'){
                                $sendTo = "Arief";
                                callNotification($sendTo, $cardName, $cardURL);
                            } 
                            //azmi
                            if($id_member1 == '58b66821144d4b68a5ab1c84' || $id_member2 == '58b66821144d4b68a5ab1c84' || $id_member3 == '58b66821144d4b68a5ab1c84' || $id_member4 == '58b66821144d4b68a5ab1c84'
                            || $id_member5 == '58b66821144d4b68a5ab1c84' || $id_member6 == '58b66821144d4b68a5ab1c84' || $id_member7 == '58b66821144d4b68a5ab1c84' || $id_member8 == '58b66821144d4b68a5ab1c84'
                            || $id_member9 == '58b66821144d4b68a5ab1c84' || $id_member10 == '58b66821144d4b68a5ab1c84'){
                                $sendTo = "Azmi";
                                callNotification($sendTo, $cardName, $cardURL);
                            }
                            //alex
                            if($id_member1 == '5b70ef61add7a23d75e11a44' || $id_member2 == '5b70ef61add7a23d75e11a44' || $id_member3 == '5b70ef61add7a23d75e11a44' || $id_member4 == '5b70ef61add7a23d75e11a44'
                            || $id_member5 == '5b70ef61add7a23d75e11a44' || $id_member6 == '5b70ef61add7a23d75e11a44' || $id_member7 == '5b70ef61add7a23d75e11a44' || $id_member8 == '5b70ef61add7a23d75e11a44'
                            || $id_member9 == '5b70ef61add7a23d75e11a44' || $id_member10 == '5b70ef61add7a23d75e11a44'){
                                $sendTo = "Alex";
                                callNotification($sendTo, $cardName, $cardURL);
                            }
                            //lee jen ling
                            if($id_member1 == '5c3bf62e0581ed89ee7b6580' || $id_member2 == '5c3bf62e0581ed89ee7b6580' || $id_member3 == '5c3bf62e0581ed89ee7b6580' || $id_member4 == '5c3bf62e0581ed89ee7b6580'
                            || $id_member5 == '5c3bf62e0581ed89ee7b6580' || $id_member6 == '5c3bf62e0581ed89ee7b6580' || $id_member7 == '5c3bf62e0581ed89ee7b6580' || $id_member8 == '5c3bf62e0581ed89ee7b6580'
                            || $id_member9 == '5c3bf62e0581ed89ee7b6580' || $id_member10 == '5c3bf62e0581ed89ee7b6580'){
                                $sendTo = "LeeJenLing";
                                callNotification($sendTo, $cardName, $cardURL);
                            }
                            //priya
                            if($id_member1 == '5b70f173191434079ebf6420' || $id_member2 == '5b70f173191434079ebf6420' || $id_member3 == '5b70f173191434079ebf6420' || $id_member4 == '5b70f173191434079ebf6420'
                            || $id_member5 == '5b70f173191434079ebf6420' || $id_member6 == '5b70f173191434079ebf6420' || $id_member7 == '5b70f173191434079ebf6420' || $id_member8 == '5b70f173191434079ebf6420'
                            || $id_member9 == '5b70f173191434079ebf6420' || $id_member10 == '5b70f173191434079ebf6420'){
                                $sendTo = "Priya";
                                callNotification($sendTo, $cardName, $cardURL);
                            }
                            //jeremy
                            if($id_member1 == '559cce487e609257b9b668e8' || $id_member2 == '559cce487e609257b9b668e8' || $id_member3 == '559cce487e609257b9b668e8' || $id_member4 == '559cce487e609257b9b668e8'
                            || $id_member5 == '559cce487e609257b9b668e8' || $id_member6 == '559cce487e609257b9b668e8' || $id_member7 == '559cce487e609257b9b668e8' || $id_member8 == '559cce487e609257b9b668e8'
                            || $id_member9 == '559cce487e609257b9b668e8' || $id_member10 == '559cce487e609257b9b668e8'){
                                $sendTo = "Jeremy";
                                callNotification($sendTo, $cardName, $cardURL);
                            }
                            //rex
                            if($id_member1 == '5c3bf7a26814db37f6b41ff2' || $id_member2 == '5c3bf7a26814db37f6b41ff2' || $id_member3 == '5c3bf7a26814db37f6b41ff2' || $id_member4 == '5c3bf7a26814db37f6b41ff2'
                            || $id_member5 == '5c3bf7a26814db37f6b41ff2' || $id_member6 == '5c3bf7a26814db37f6b41ff2' || $id_member7 == '5c3bf7a26814db37f6b41ff2' || $id_member8 == '5c3bf7a26814db37f6b41ff2'
                            || $id_member9 == '5c3bf7a26814db37f6b41ff2' || $id_member10 == '5c3bf7a26814db37f6b41ff2'){
                                $sendTo = "Rex";
                                callNotification($sendTo, $cardName, $cardURL);
                            }
                            //lee ying
                            if($id_member1 == '590c33ec597ce7d26734f355' || $id_member2 == '590c33ec597ce7d26734f355' || $id_member3 == '590c33ec597ce7d26734f355' || $id_member4 == '590c33ec597ce7d26734f355'
                            || $id_member5 == '590c33ec597ce7d26734f355' || $id_member6 == '590c33ec597ce7d26734f355' || $id_member7 == '590c33ec597ce7d26734f355' || $id_member8 == '590c33ec597ce7d26734f355'
                            || $id_member9 == '590c33ec597ce7d26734f355' || $id_member10 == '590c33ec597ce7d26734f355'){
                                $sendTo = "LeeYing";
                                callNotification($sendTo, $cardName, $cardURL);
                            }
                            //shah
                            if($id_member1 == '58acfc97f4b21ebff852f886' || $id_member2 == '58acfc97f4b21ebff852f886' || $id_member3 == '58acfc97f4b21ebff852f886' || $id_member4 == '58acfc97f4b21ebff852f886'
                            || $id_member5 == '58acfc97f4b21ebff852f886' || $id_member6 == '58acfc97f4b21ebff852f886' || $id_member7 == '58acfc97f4b21ebff852f886' || $id_member8 == '58acfc97f4b21ebff852f886'
                            || $id_member9 == '58acfc97f4b21ebff852f886' || $id_member10 == '58acfc97f4b21ebff852f886'){
                                $sendTo = "Shah";
                                callNotification($sendTo, $cardName, $cardURL);
                            }
                            //mrs. suhana
                            if($id_member1 == '58abdb1a88c1ba56168f5d9e' || $id_member2 == '58abdb1a88c1ba56168f5d9e' || $id_member3 == '58abdb1a88c1ba56168f5d9e' || $id_member4 == '58abdb1a88c1ba56168f5d9e'
                            || $id_member5 == '58abdb1a88c1ba56168f5d9e' || $id_member6 == '58abdb1a88c1ba56168f5d9e' || $id_member7 == '58abdb1a88c1ba56168f5d9e' || $id_member8 == '58abdb1a88c1ba56168f5d9e'
                            || $id_member9 == '58abdb1a88c1ba56168f5d9e' || $id_member10 == '58abdb1a88c1ba56168f5d9e'){
                                $sendTo = "Suhana";
                                callNotification($sendTo, $cardName, $cardURL);
                            }
                        } else {
                            $id_member    = $dataMMB[0];

                            //check which memberID
                            //arief
                            if($id_member == '5959c36a55c57339a774ff8c'){
                                $sendTo = "Arief";
                                callNotification($sendTo, $cardName, $cardURL);
                            } 
                            //azmi
                            if($id_member == '58b66821144d4b68a5ab1c84'){
                                $sendTo = "Azmi";
                                callNotification($sendTo, $cardName, $cardURL);
                            } 
                            //alex
                            if($id_member == '5b70ef61add7a23d75e11a44'){
                                $sendTo = "Alex";
                                callNotification($sendTo, $cardName, $cardURL);
                            } 
                            //LeeJenLing
                            if($id_member == '5c3bf62e0581ed89ee7b6580'){
                                $sendTo = "LeeJenLing";
                                callNotification($sendTo, $cardName, $cardURL);
                            } 
                            //priya
                            if($id_member == '5b70f173191434079ebf6420'){
                                $sendTo = "Priya";
                                callNotification($sendTo, $cardName, $cardURL);
                            } 
                            //jeremy
                            if($id_member == '559cce487e609257b9b668e8'){
                                $sendTo = "Jeremy";
                                callNotification($sendTo, $cardName, $cardURL);
                            } 
                            //rex
                            if($id_member == '5c3bf7a26814db37f6b41ff2'){
                                $sendTo = "Rex";
                                callNotification($sendTo, $cardName, $cardURL);
                            } 
                            //LeeYingFoo
                            if($id_member == '590c33ec597ce7d26734f355'){
                                $sendTo = "LeeYing";
                                callNotification($sendTo, $cardName, $cardURL);
                            } 
                            //shah
                            if($id_member == '58acfc97f4b21ebff852f886'){
                                $sendTo = "Shah";
                                callNotification($sendTo, $cardName, $cardURL);
                            } 
                            //suhana
                            if($id_member == '58abdb1a88c1ba56168f5d9e'){
                                $sendTo = "Suhana";
                                callNotification($sendTo, $cardName, $cardURL);
                            } 
                        }
                    }
                }
            } 
        }
        $i++;
    }
}

//for add label to trello card
    function AddLabelOneDay($cardID){
        $endpointURL    = 'https://api.trello.com/1/cards/'.$cardID.'/labels';
        $parameters     = array(
                "color" => "black",
                "name"  => "Day 1",
                "key"   => "cee15f8c5b0d25a32783f5139b64edb4",
                "token" => "0a9207e773917d4738910c973994011eea5bc955f93e59cdff9d1b7f1ab8776d"
        );
        $payload        = json_encode($parameters);

        // Prepare new cURL resource
        $ch = curl_init($endpointURL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);

        // Set HTTP Header for POST request 
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($payload))
        );
        
        // Submit the POST request
        $result = curl_exec($ch);
        
        // Close cURL session handle
        curl_close($ch);
    }

    function AddLabelTwoDays($cardID){
        $endpointURL    = 'https://api.trello.com/1/cards/'.$cardID.'/labels';
        $parameters     = array(
                "color" => "black",
                "name"  => "Day 2",
                "key"   => "cee15f8c5b0d25a32783f5139b64edb4",
                "token" => "0a9207e773917d4738910c973994011eea5bc955f93e59cdff9d1b7f1ab8776d"
        );
        $payload        = json_encode($parameters);

        // Prepare new cURL resource
        $ch = curl_init($endpointURL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);

        // Set HTTP Header for POST request 
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($payload))
        );
        
        // Submit the POST request
        $result = curl_exec($ch);
        
        // Close cURL session handle
        curl_close($ch);
    }

    function AddLabelThreeDays($cardID){
        $endpointURL    = 'https://api.trello.com/1/cards/'.$cardID.'/labels';
        $parameters     = array(
                "color" => "black",
                "name"  => "Day 3",
                "key"   => "cee15f8c5b0d25a32783f5139b64edb4",
                "token" => "0a9207e773917d4738910c973994011eea5bc955f93e59cdff9d1b7f1ab8776d"
        );
        $payload        = json_encode($parameters);

        // Prepare new cURL resource
        $ch = curl_init($endpointURL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);

        // Set HTTP Header for POST request 
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($payload))
        );
        
        // Submit the POST request
        $result = curl_exec($ch);
        
        // Close cURL session handle
        curl_close($ch);
    }

    function AddLabelFourDays($cardID){
        $endpointURL    = 'https://api.trello.com/1/cards/'.$cardID.'/labels';
        $parameters     = array(
                "color" => "black",
                "name"  => "Day 4",
                "key"   => "cee15f8c5b0d25a32783f5139b64edb4",
                "token" => "0a9207e773917d4738910c973994011eea5bc955f93e59cdff9d1b7f1ab8776d"
        );
        $payload        = json_encode($parameters);

        // Prepare new cURL resource
        $ch = curl_init($endpointURL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);

        // Set HTTP Header for POST request 
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($payload))
        );
        
        // Submit the POST request
        $result = curl_exec($ch);
        
        // Close cURL session handle
        curl_close($ch);
    }

    function AddLabelFiveDays($cardID){
        $endpointURL    = 'https://api.trello.com/1/cards/'.$cardID.'/labels';
        $parameters     = array(
                "color" => "black",
                "name"  => "Day 5",
                "key"   => "cee15f8c5b0d25a32783f5139b64edb4",
                "token" => "0a9207e773917d4738910c973994011eea5bc955f93e59cdff9d1b7f1ab8776d"

        );
        $payload        = json_encode($parameters);

        // Prepare new cURL resource
        $ch = curl_init($endpointURL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);

        // Set HTTP Header for POST request 
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($payload))
        );
        
        // Submit the POST request
        $result = curl_exec($ch);
        
        // Close cURL session handle
        curl_close($ch);
    }

    function AddLabelSixDays($cardID){
        $endpointURL    = 'https://api.trello.com/1/cards/'.$cardID.'/labels';
        $parameters     = array(
                "color" => "black",
                "name"  => "Day 6",
                "key"   => "cee15f8c5b0d25a32783f5139b64edb4",
                "token" => "0a9207e773917d4738910c973994011eea5bc955f93e59cdff9d1b7f1ab8776d"

        );
        $payload        = json_encode($parameters);

        // Prepare new cURL resource
        $ch = curl_init($endpointURL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);

        // Set HTTP Header for POST request 
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($payload))
        );
        
        // Submit the POST request
        $result = curl_exec($ch);
        
        // Close cURL session handle
        curl_close($ch);
    }

    function AddLabelSevenDays($cardID){
        $endpointURL    = 'https://api.trello.com/1/cards/'.$cardID.'/labels';
        $parameters     = array(
                "color" => "black",
                "name"  => "Day 7",
                "key"   => "cee15f8c5b0d25a32783f5139b64edb4",
                "token" => "0a9207e773917d4738910c973994011eea5bc955f93e59cdff9d1b7f1ab8776d"
        );
        $payload        = json_encode($parameters);

        // Prepare new cURL resource
        $ch = curl_init($endpointURL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);

        // Set HTTP Header for POST request 
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($payload))
        );
        
        // Submit the POST request
        $result = curl_exec($ch);
        
        // Close cURL session handle
        curl_close($ch);
    }

    function AddLabelOverSevenDays($cardID){
        $endpointURL    = 'https://api.trello.com/1/cards/'.$cardID.'/labels';
        $parameters     = array(
                "color" => "black",
                "name"  => "Day 7+",
                "key"   => "cee15f8c5b0d25a32783f5139b64edb4",
                "token" => "0a9207e773917d4738910c973994011eea5bc955f93e59cdff9d1b7f1ab8776d"
        );
        $payload        = json_encode($parameters);

        // Prepare new cURL resource
        $ch = curl_init($endpointURL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);

        // Set HTTP Header for POST request 
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($payload))
        );
        
        // Submit the POST request
        $result = curl_exec($ch);
        
        // Close cURL session handle
        curl_close($ch);
    }

//for delete label
    /*
        ID Labels
        ---------
        Day 1   = 5c74c9019ba3c5188015ba78
        Day 2   = 5c7647ba7ec0210584912079
        Day 3   = 5c76471dccccde7db60eccf8
        Day 4   = 5c60ea917fca4c37d2727cb8
        Day 5   = 5c60ea959541aa5f075a006e
        Day 6   = 5c60ea9a85179403e254b1a7
        Day 7   = 5c6f94033a3b483d5a382d01
        Day 7+  = 5c6e084be1b4e188b05753c5
    */
    function DeleteLabelDayOne($cardID){
        $labelID= '5c74c9019ba3c5188015ba78';
        $URL    = 'https://api.trello.com/1/cards/'.$cardID.'/idLabels/'.$labelID.'?key=cee15f8c5b0d25a32783f5139b64edb4&token=0a9207e773917d4738910c973994011eea5bc955f93e59cdff9d1b7f1ab8776d';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$URL);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        // curl_setopt($ch, CURLOPT_POSTFIELDS, $arrays);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $result = json_decode($result);
        curl_close($ch);
    }

    function DeleteLabelDayTwo($cardID){
        $labelID= '5c7647ba7ec0210584912079';
        $URL    = 'https://api.trello.com/1/cards/'.$cardID.'/idLabels/'.$labelID.'?key=cee15f8c5b0d25a32783f5139b64edb4&token=0a9207e773917d4738910c973994011eea5bc955f93e59cdff9d1b7f1ab8776d';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$URL);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        // curl_setopt($ch, CURLOPT_POSTFIELDS, $arrays);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $result = json_decode($result);
        curl_close($ch);
    }

    function DeleteLabelDayThree($cardID){
        $labelID= '5c76471dccccde7db60eccf8';
        $URL    = 'https://api.trello.com/1/cards/'.$cardID.'/idLabels/'.$labelID.'?key=cee15f8c5b0d25a32783f5139b64edb4&token=0a9207e773917d4738910c973994011eea5bc955f93e59cdff9d1b7f1ab8776d';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$URL);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        // curl_setopt($ch, CURLOPT_POSTFIELDS, $arrays);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $result = json_decode($result);
        curl_close($ch);
    }

    function DeleteLabelDayFour($cardID){
        $labelID= '5c60ea917fca4c37d2727cb8';
        $URL    = 'https://api.trello.com/1/cards/'.$cardID.'/idLabels/'.$labelID.'?key=cee15f8c5b0d25a32783f5139b64edb4&token=0a9207e773917d4738910c973994011eea5bc955f93e59cdff9d1b7f1ab8776d';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$URL);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        // curl_setopt($ch, CURLOPT_POSTFIELDS, $arrays);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $result = json_decode($result);
        curl_close($ch);
    }

    function DeleteLabelDayFive($cardID){
        $labelID= '5c60ea959541aa5f075a006e';
        $URL    = 'https://api.trello.com/1/cards/'.$cardID.'/idLabels/'.$labelID.'?key=cee15f8c5b0d25a32783f5139b64edb4&token=0a9207e773917d4738910c973994011eea5bc955f93e59cdff9d1b7f1ab8776d';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$URL);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        // curl_setopt($ch, CURLOPT_POSTFIELDS, $arrays);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $result = json_decode($result);
        curl_close($ch);
    }

    function DeleteLabelDaySix($cardID){
        $labelID= '5c60ea9a85179403e254b1a7';
        $URL    = 'https://api.trello.com/1/cards/'.$cardID.'/idLabels/'.$labelID.'?key=cee15f8c5b0d25a32783f5139b64edb4&token=0a9207e773917d4738910c973994011eea5bc955f93e59cdff9d1b7f1ab8776d';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$URL);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        // curl_setopt($ch, CURLOPT_POSTFIELDS, $arrays);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $result = json_decode($result);
        curl_close($ch);
    }

    function DeleteLabelDaySeven($cardID){
        $labelID= '5c6f94033a3b483d5a382d01';
        $URL    = 'https://api.trello.com/1/cards/'.$cardID.'/idLabels/'.$labelID.'?key=cee15f8c5b0d25a32783f5139b64edb4&token=0a9207e773917d4738910c973994011eea5bc955f93e59cdff9d1b7f1ab8776d';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$URL);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        // curl_setopt($ch, CURLOPT_POSTFIELDS, $arrays);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $result = json_decode($result);
        curl_close($ch);
    }

    function DeleteLabelDaySevenPlus($cardID){
        $labelID= '5c6e084be1b4e188b05753c5';
        $URL    = 'https://api.trello.com/1/cards/'.$cardID.'/idLabels/'.$labelID.'?key=cee15f8c5b0d25a32783f5139b64edb4&token=0a9207e773917d4738910c973994011eea5bc955f93e59cdff9d1b7f1ab8776d';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$URL);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        // curl_setopt($ch, CURLOPT_POSTFIELDS, $arrays);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $result = json_decode($result);
        curl_close($ch);
    }

//call function for notification
function callNotification($sendTo, $cardName, $cardURL){
    if($sendTo == "Arief"){
        $webhookURL_Arief   = 'https://hooks.slack.com/services/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';
        
        //data for notification
        $channel            = '#automation';
        $bot_name           = 'Kaigan Taskmaster';
        $icon               = ':alien:';
        $attachments        = array([
            'color'     => '#36a64f',        
            'text'      => "*<@U62TFQL1Y>, you're spending too much time for the task:*\n*[$cardName]*\n_Complete it now or split it into multiple smaller tasks._\n$cardURL",
            'mrkdwn'    =>  true
        ]);

        $data = array(
            'channel'     => $channel,
            'username'    => $bot_name,
            'icon_emoji'  => $icon,
            'attachments' => $attachments
        );

        $data_string = json_encode($data);

        $ch = curl_init($webhookURL_Arief);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string))
        );
        //Execute CURL
        $result = curl_exec($ch);
        curl_close($ch);
    } 
    
    else if($sendTo == "Azmi"){
        $webhookURL_Azmi    = 'https://hooks.slack.com/services/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';
        //data for notification
        $channel            = '#automation';
        $bot_name           = 'Kaigan Taskmaster';
        $icon               = ':alien:';
        $attachments        = array([
            'color'    => '#36a64f',
            'text'     => "*<@U5A2WEHEH>, you're spending too much time for the task:*\n*[$cardName]*\n_Complete it now or split it into multiple smaller tasks._\n$cardURL",
        ]);

        $data = array(
            'channel'     => $channel,
            'username'    => $bot_name,
            'icon_emoji'  => $icon,
            'attachments' => $attachments
        );

        $data_string = json_encode($data);
        $ch = curl_init($webhookURL_Azmi);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
            );
        //Execute CURL
        $result = curl_exec($ch);
        curl_close($ch);
    } 
    
    else if($sendTo == "Alex"){
        $webhookURL_Alex    = 'https://hooks.slack.com/services/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';
        //data for notification
        $channel            = '#automation';
        $bot_name           = 'Kaigan Taskmaster';
        $icon               = ':alien:';
        $attachments        = array([
            'color'    => '#36a64f',
            'text'     => "*<@U9DV9JUET>, you're spending too much time for the task:*\n*[$cardName]*\n_Complete it now or split it into multiple smaller tasks._\n$cardURL",
        ]);

        $data = array(
            'channel'     => $channel,
            'username'    => $bot_name,
            'icon_emoji'  => $icon,
            'attachments' => $attachments
        );

        $data_string = json_encode($data);
        $ch = curl_init($webhookURL_Alex);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
            );
        //Execute CURL
        $result = curl_exec($ch);
        curl_close($ch);
    } 
    
    else if($sendTo == "LeeJenLing"){
        $webhookURL_Alex    = 'https://hooks.slack.com/services/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';
        //data for notification
        $channel            = '#automation';
        $bot_name           = 'Kaigan Taskmaster';
        $icon               = ':alien:';
        $attachments        = array([
            'color'    => '#36a64f',
            'text'     => "*<@UFCJUGKDH>, you're spending too much time for the task:*\n*[$cardName]*\n_Complete it now or split it into multiple smaller tasks._\n$cardURL",
        ]);

        $data = array(
            'channel'     => $channel,
            'username'    => $bot_name,
            'icon_emoji'  => $icon,
            'attachments' => $attachments
        );

        $data_string = json_encode($data);
        $ch = curl_init($webhookURL_Alex);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
            );
        //Execute CURL
        $result = curl_exec($ch);
        curl_close($ch);
    } 
    
    else if($sendTo == "Priya"){
        $webhookURL_Priya   = 'https://hooks.slack.com/services/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';
        //data for notification
        $channel            = '#automation';
        $bot_name           = 'Kaigan Taskmaster';
        $icon               = ':alien:';
        $attachments        = array([
            'color'    => '#36a64f',
            'text'     => "*<@UCGKY4P2T>, you're spending too much time for the task:*\n*[$cardName]*\n_Complete it now or split it into multiple smaller tasks._\n$cardURL",
        ]);

        $data = array(
            'channel'     => $channel,
            'username'    => $bot_name,
            'icon_emoji'  => $icon,
            'attachments' => $attachments
        );

        $data_string = json_encode($data);
        $ch = curl_init($webhookURL_Priya);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
            );
        //Execute CURL
        $result = curl_exec($ch);
        curl_close($ch);
    } 
    
    else if($sendTo == "Jeremy"){
        $webhookURL_Jeremy  = 'https://hooks.slack.com/services/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';
        //data for notification
        $channel            = '#automation';
        $bot_name           = 'Kaigan Taskmaster';
        $icon               = ':alien:';
        $attachments        = array([
            'color'    => '#36a64f',
            'text'     => "*<@U59LLD0JE>, you're spending too much time for the task:*\n*[$cardName]*\n_Complete it now or split it into multiple smaller tasks._\n$cardURL",
        ]);

        $data = array(
            'channel'     => $channel,
            'username'    => $bot_name,
            'icon_emoji'  => $icon,
            'attachments' => $attachments
        );

        $data_string = json_encode($data);
        $ch = curl_init($webhookURL_Jeremy);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
            );
        //Execute CURL
        $result = curl_exec($ch);
        curl_close($ch);
    } 
    
    else if($sendTo == "Rex"){
        $webhookURL_Jeremy  = 'https://hooks.slack.com/services/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';
        //data for notification
        $channel            = '#automation';
        $bot_name           = 'Kaigan Taskmaster';
        $icon               = ':alien:';
        $attachments        = array([
            'color'    => '#36a64f',
            'text'     => "*<@UFC03D588>, you're spending too much time for the task:*\n*[$cardName]*\n_Complete it now or split it into multiple smaller tasks._\n$cardURL",
        ]);

        $data = array(
            'channel'     => $channel,
            'username'    => $bot_name,
            'icon_emoji'  => $icon,
            'attachments' => $attachments
        );

        $data_string = json_encode($data);
        $ch = curl_init($webhookURL_Jeremy);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
            );
        //Execute CURL
        $result = curl_exec($ch);
        curl_close($ch);
    } 
    
    else if($sendTo == "LeeYing"){
        $webhookURL_LeeYing   = 'https://hooks.slack.com/services/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';
        //data for notification
        $channel            = '#automation';
        $bot_name           = 'Kaigan Taskmaster';
        $icon               = ':alien:';
        $attachments        = array([
            'color'    => '#36a64f',
            'text'     => "*<@U591PGRJ4>, you're spending too much time for the task:*\n*[$cardName]*\n_Complete it now or split it into multiple smaller tasks._\n$cardURL",
        ]);

        $data = array(
            'channel'     => $channel,
            'username'    => $bot_name,
            'icon_emoji'  => $icon,
            'attachments' => $attachments
        );

        $data_string = json_encode($data);
        $ch = curl_init($webhookURL_LeeYing);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
            );
        //Execute CURL
        $result = curl_exec($ch);
        curl_close($ch);
    } 
    
    else if($sendTo == "Shah"){
        $webhookURL_Shah    = 'https://hooks.slack.com/services/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';
        //data for notification
        $channel            = '#automation';
        $bot_name           = 'Kaigan Taskmaster';
        $icon               = ':alien:';
        $attachments        = array([
            'color'    => '#36a64f',
            'text'     => "*<@U59L90G66>, you're spending too much time for the task:*\n*[$cardName]*\n_Complete it now or split it into multiple smaller tasks._\n$cardURL",
        ]);

        $data = array(
            'channel'     => $channel,
            'username'    => $bot_name,
            'icon_emoji'  => $icon,
            'attachments' => $attachments
        );

        $data_string = json_encode($data);
        $ch = curl_init($webhookURL_Shah);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
            );
        //Execute CURL
        $result = curl_exec($ch);
        curl_close($ch);
    } 
    
    else if($sendTo == "Suhana"){
        $webhookURL_Suhana   = 'https://hooks.slack.com/services/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';
        //data for notification
        $channel            = '#automation';
        $bot_name           = 'Kaigan Taskmaster';
        $icon               = ':alien:';
        $attachments        = array([
            'color'    => '#36a64f',
            'text'     => "*<@U59MSGE12>, you're spending too much time for the task:*\n*[$cardName]*\n_Complete it now or split it into multiple smaller tasks._\n$cardURL",
        ]);

        $data = array(
            'channel'     => $channel,
            'username'    => $bot_name,
            'icon_emoji'  => $icon,
            'attachments' => $attachments
        );

        $data_string = json_encode($data);
        $ch = curl_init($webhookURL_Suhana);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
            );
        //Execute CURL
        $result = curl_exec($ch);
        curl_close($ch);
    } 
    
    else {
        echo 'Unknown Member';
    }
}
    
GetDataSoundList();       //1
?>