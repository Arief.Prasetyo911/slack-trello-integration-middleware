<?php
error_reporting(0);
//get data from Development list
function GetDataDevelopmentList(){
    $devListID  = '5c3308c14360d00d5e72d78d';
    $key        = 'cee15f8c5b0d25a32783f5139b64edb4';
    $token      = '0a9207e773917d4738910c973994011eea5bc955f93e59cdff9d1b7f1ab8776d';
    $APIURI     = 'https://api.trello.com/1/lists/'.$devListID.'/cards?cards=visible&card_member_fields=all&key='.$key.'&token='.$token;

    $init       = curl_init();
    curl_setopt($init, CURLOPT_URL, $APIURI); //Url together with parameters
    curl_setopt($init, CURLOPT_RETURNTRANSFER, 1); //Return data instead printing directly in Browser
    curl_setopt($init, CURLOPT_CONNECTTIMEOUT , 7); //Timeout after 7 seconds
    curl_setopt($init, CURLOPT_USERAGENT , "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
    curl_setopt($init, CURLOPT_HEADER, 0);

    $exec       = curl_exec($init);
    $close      = curl_close($init);

    $decodeJSON = json_decode($exec, true);
    $countJsonArray  = count($decodeJSON);
    
    $i = 0;
    while($i < $countJsonArray){
        // echo "data - ".$i.'<br>';
        foreach($decodeJSON[$i]['labels'] as $labelsData){
            //data  from labels array
            $labelName      = $labelsData['name'];
            $labelID        = $labelsData['id'];
            $labelColor     = $labelsData['color'];
            
            //data from card json return
            $cardName       = $decodeJSON[$i]['name'];
            $cardID         = $decodeJSON[$i]['id'];
            $cardURL        = $decodeJSON[$i]['url'];
            $memberID       = $decodeJSON[$i]['idMembers'];
            
            if((strpos($labelName, 'Day 7+') !== false)){
                //fetch memberID array
                if(count($memberID) == 1){
                    $memberIDs  = $memberID[0];

                    //arief
                    if(strpos($memberIDs, '5959c36a55c57339a774ff8c') !== false){
                        $dataMember = "<@U62TFQL1Y>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //azmi
                    if(strpos($memberIDs, '58b66821144d4b68a5ab1c84') !== false){
                        $dataMember = "<@U5A2WEHEH>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //alex
                    if(strpos($memberIDs, '5b70ef61add7a23d75e11a44') !== false){
                        $dataMember = "<@U9DV9JUET>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //leeJenLing
                    if(strpos($memberIDs, '5c3bf62e0581ed89ee7b6580') !== false){
                        $dataMember = "<@UFCJUGKDH>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //priya
                    if(strpos($memberIDs, '5b70f173191434079ebf6420') !== false){
                        $dataMember = "<@UCGKY4P2T>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //jeremy
                    if(strpos($memberIDs, '559cce487e609257b9b668e8') !== false){
                        $dataMember = "<@U59LLD0JE>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //rex
                    if(strpos($memberIDs, '5c3bf7a26814db37f6b41ff2') !== false){
                        $dataMember = "<@UFC03D588>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //leeYing
                    if(strpos($memberIDs, '590c33ec597ce7d26734f355') !== false){
                        $dataMember = "<@U591PGRJ4>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //shah
                    if(strpos($memberIDs, '58acfc97f4b21ebff852f886') !== false){
                        $dataMember = "<@U59L90G66>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //mrs. suhana
                    if(strpos($memberIDs, '58abdb1a88c1ba56168f5d9e') !== false){
                        $dataMember = "<@U59MSGE12>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    
                } else {
                    $id_member1 = $memberID[0];
                    $id_member2 = $memberID[1];
                    $id_member3 = $memberID[2];
                    $id_member4 = $memberID[3];
                    $id_member5 = $memberID[4];
                    $id_member6 = $memberID[5];
                    $id_member7 = $memberID[6];
                    $id_member8 = $memberID[7];
                    $id_member9 = $memberID[8];
                    $id_member10 = $memberID[9];

                    //arief
                    if($id_member1 == '5959c36a55c57339a774ff8c' || $id_member2 == '5959c36a55c57339a774ff8c' || $id_member3 == '5959c36a55c57339a774ff8c' || $id_member4 == '5959c36a55c57339a774ff8c'
                    || $id_member5 == '5959c36a55c57339a774ff8c' || $id_member6 == '5959c36a55c57339a774ff8c' || $id_member7 == '5959c36a55c57339a774ff8c' || $id_member8 == '5959c36a55c57339a774ff8c'
                    || $id_member9 == '5959c36a55c57339a774ff8c' || $id_member10 == '5959c36a55c57339a774ff8c'){
                        $dataMember = "<@U62TFQL1Y>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    } 
                    //azmi
                    if($id_member1 == '58b66821144d4b68a5ab1c84' || $id_member2 == '58b66821144d4b68a5ab1c84' || $id_member3 == '58b66821144d4b68a5ab1c84' || $id_member4 == '58b66821144d4b68a5ab1c84'
                    || $id_member5 == '58b66821144d4b68a5ab1c84' || $id_member6 == '58b66821144d4b68a5ab1c84' || $id_member7 == '58b66821144d4b68a5ab1c84' || $id_member8 == '58b66821144d4b68a5ab1c84'
                    || $id_member9 == '58b66821144d4b68a5ab1c84' || $id_member10 == '58b66821144d4b68a5ab1c84'){
                        $dataMember = "<@U5A2WEHEH>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //alex
                    if($id_member1 == '5b70ef61add7a23d75e11a44' || $id_member2 == '5b70ef61add7a23d75e11a44' || $id_member3 == '5b70ef61add7a23d75e11a44' || $id_member4 == '5b70ef61add7a23d75e11a44'
                    || $id_member5 == '5b70ef61add7a23d75e11a44' || $id_member6 == '5b70ef61add7a23d75e11a44' || $id_member7 == '5b70ef61add7a23d75e11a44' || $id_member8 == '5b70ef61add7a23d75e11a44'
                    || $id_member9 == '5b70ef61add7a23d75e11a44' || $id_member10 == '5b70ef61add7a23d75e11a44'){
                        $dataMember = "<@U9DV9JUET>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //lee jen ling
                    if($id_member1 == '5c3bf62e0581ed89ee7b6580' || $id_member2 == '5c3bf62e0581ed89ee7b6580' || $id_member3 == '5c3bf62e0581ed89ee7b6580' || $id_member4 == '5c3bf62e0581ed89ee7b6580'
                    || $id_member5 == '5c3bf62e0581ed89ee7b6580' || $id_member6 == '5c3bf62e0581ed89ee7b6580' || $id_member7 == '5c3bf62e0581ed89ee7b6580' || $id_member8 == '5c3bf62e0581ed89ee7b6580'
                    || $id_member9 == '5c3bf62e0581ed89ee7b6580' || $id_member10 == '5c3bf62e0581ed89ee7b6580'){
                        $dataMember = "<@UFCJUGKDH>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //priya
                    if($id_member1 == '5b70f173191434079ebf6420' || $id_member2 == '5b70f173191434079ebf6420' || $id_member3 == '5b70f173191434079ebf6420' || $id_member4 == '5b70f173191434079ebf6420'
                    || $id_member5 == '5b70f173191434079ebf6420' || $id_member6 == '5b70f173191434079ebf6420' || $id_member7 == '5b70f173191434079ebf6420' || $id_member8 == '5b70f173191434079ebf6420'
                    || $id_member9 == '5b70f173191434079ebf6420' || $id_member10 == '5b70f173191434079ebf6420'){
                        $dataMember = "<@UCGKY4P2T>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //jeremy
                    if($id_member1 == '559cce487e609257b9b668e8' || $id_member2 == '559cce487e609257b9b668e8' || $id_member3 == '559cce487e609257b9b668e8' || $id_member4 == '559cce487e609257b9b668e8'
                    || $id_member5 == '559cce487e609257b9b668e8' || $id_member6 == '559cce487e609257b9b668e8' || $id_member7 == '559cce487e609257b9b668e8' || $id_member8 == '559cce487e609257b9b668e8'
                    || $id_member9 == '559cce487e609257b9b668e8' || $id_member10 == '559cce487e609257b9b668e8'){
                        $dataMember = "<@U59LLD0JE>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //rex
                    if($id_member1 == '5c3bf7a26814db37f6b41ff2' || $id_member2 == '5c3bf7a26814db37f6b41ff2' || $id_member3 == '5c3bf7a26814db37f6b41ff2' || $id_member4 == '5c3bf7a26814db37f6b41ff2'
                    || $id_member5 == '5c3bf7a26814db37f6b41ff2' || $id_member6 == '5c3bf7a26814db37f6b41ff2' || $id_member7 == '5c3bf7a26814db37f6b41ff2' || $id_member8 == '5c3bf7a26814db37f6b41ff2'
                    || $id_member9 == '5c3bf7a26814db37f6b41ff2' || $id_member10 == '5c3bf7a26814db37f6b41ff2'){
                        $dataMember = "<@UFC03D588>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //lee ying
                    if($id_member1 == '590c33ec597ce7d26734f355' || $id_member2 == '590c33ec597ce7d26734f355' || $id_member3 == '590c33ec597ce7d26734f355' || $id_member4 == '590c33ec597ce7d26734f355'
                    || $id_member5 == '590c33ec597ce7d26734f355' || $id_member6 == '590c33ec597ce7d26734f355' || $id_member7 == '590c33ec597ce7d26734f355' || $id_member8 == '590c33ec597ce7d26734f355'
                    || $id_member9 == '590c33ec597ce7d26734f355' || $id_member10 == '590c33ec597ce7d26734f355'){
                        $dataMember = "<@U591PGRJ4>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //shah
                    if($id_member1 == '58acfc97f4b21ebff852f886' || $id_member2 == '58acfc97f4b21ebff852f886' || $id_member3 == '58acfc97f4b21ebff852f886' || $id_member4 == '58acfc97f4b21ebff852f886'
                    || $id_member5 == '58acfc97f4b21ebff852f886' || $id_member6 == '58acfc97f4b21ebff852f886' || $id_member7 == '58acfc97f4b21ebff852f886' || $id_member8 == '58acfc97f4b21ebff852f886'
                    || $id_member9 == '58acfc97f4b21ebff852f886' || $id_member10 == '58acfc97f4b21ebff852f886'){
                        $dataMember = "<@U59L90G66>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //mrs. suhana
                    if($id_member1 == '58abdb1a88c1ba56168f5d9e' || $id_member2 == '58abdb1a88c1ba56168f5d9e' || $id_member3 == '58abdb1a88c1ba56168f5d9e' || $id_member4 == '58abdb1a88c1ba56168f5d9e'
                    || $id_member5 == '58abdb1a88c1ba56168f5d9e' || $id_member6 == '58abdb1a88c1ba56168f5d9e' || $id_member7 == '58abdb1a88c1ba56168f5d9e' || $id_member8 == '58abdb1a88c1ba56168f5d9e'
                    || $id_member9 == '58abdb1a88c1ba56168f5d9e' || $id_member10 == '58abdb1a88c1ba56168f5d9e'){
                        $dataMember = "<@U59MSGE12>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }         
                }
            }
        }
        $i++;
    }
}

//get data from GameDesign list
function GetDataGameDesignList(){
    $devListID  = '5bceba7f62850a31fb5160dc';
    $key        = 'cee15f8c5b0d25a32783f5139b64edb4';
    $token      = '0a9207e773917d4738910c973994011eea5bc955f93e59cdff9d1b7f1ab8776d';
    $APIURI     = 'https://api.trello.com/1/lists/'.$devListID.'/cards?cards=visible&card_member_fields=all&key='.$key.'&token='.$token;

    $init       = curl_init();
    curl_setopt($init, CURLOPT_URL, $APIURI); //Url together with parameters
    curl_setopt($init, CURLOPT_RETURNTRANSFER, 1); //Return data instead printing directly in Browser
    curl_setopt($init, CURLOPT_CONNECTTIMEOUT , 7); //Timeout after 7 seconds
    curl_setopt($init, CURLOPT_USERAGENT , "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
    curl_setopt($init, CURLOPT_HEADER, 0);

    $exec       = curl_exec($init);
    $close      = curl_close($init);

    $decodeJSON = json_decode($exec, true);
    $countJsonArray  = count($decodeJSON);
    
    $i = 0;
    while($i < $countJsonArray){
        // echo "data - ".$i.'<br>';
        foreach($decodeJSON[$i]['labels'] as $labelsData){
            //data  from labels array
            $labelName      = $labelsData['name'];
            $labelID        = $labelsData['id'];
            $labelColor     = $labelsData['color'];
            
            //data from card json return
            $cardName       = $decodeJSON[$i]['name'];
            $cardID         = $decodeJSON[$i]['id'];
            $cardURL        = $decodeJSON[$i]['url'];
            $memberID       = $decodeJSON[$i]['idMembers'];
            
            if((strpos($labelName, 'Day 7+') !== false)){
                //fetch memberID array
                if(count($memberID) == 1){
                    $memberIDs  = $memberID[0];

                    //arief
                    if(strpos($memberIDs, '5959c36a55c57339a774ff8c') !== false){
                        $dataMember = "<@U62TFQL1Y>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //azmi
                    if(strpos($memberIDs, '58b66821144d4b68a5ab1c84') !== false){
                        $dataMember = "<@U5A2WEHEH>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //alex
                    if(strpos($memberIDs, '5b70ef61add7a23d75e11a44') !== false){
                        $dataMember = "<@U9DV9JUET>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //leeJenLing
                    if(strpos($memberIDs, '5c3bf62e0581ed89ee7b6580') !== false){
                        $dataMember = "<@UFCJUGKDH>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //priya
                    if(strpos($memberIDs, '5b70f173191434079ebf6420') !== false){
                        $dataMember = "<@UCGKY4P2T>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //jeremy
                    if(strpos($memberIDs, '559cce487e609257b9b668e8') !== false){
                        $dataMember = "<@U59LLD0JE>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //rex
                    if(strpos($memberIDs, '5c3bf7a26814db37f6b41ff2') !== false){
                        $dataMember = "<@UFC03D588>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //leeYing
                    if(strpos($memberIDs, '590c33ec597ce7d26734f355') !== false){
                        $dataMember = "<@U591PGRJ4>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //shah
                    if(strpos($memberIDs, '58acfc97f4b21ebff852f886') !== false){
                        $dataMember = "<@U59L90G66>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //mrs. suhana
                    if(strpos($memberIDs, '58abdb1a88c1ba56168f5d9e') !== false){
                        $dataMember = "<@U59MSGE12>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    
                } else {
                    $id_member1 = $memberID[0];
                    $id_member2 = $memberID[1];
                    $id_member3 = $memberID[2];
                    $id_member4 = $memberID[3];
                    $id_member5 = $memberID[4];
                    $id_member6 = $memberID[5];
                    $id_member7 = $memberID[6];
                    $id_member8 = $memberID[7];
                    $id_member9 = $memberID[8];
                    $id_member10 = $memberID[9];

                    //arief
                    if($id_member1 == '5959c36a55c57339a774ff8c' || $id_member2 == '5959c36a55c57339a774ff8c' || $id_member3 == '5959c36a55c57339a774ff8c' || $id_member4 == '5959c36a55c57339a774ff8c'
                    || $id_member5 == '5959c36a55c57339a774ff8c' || $id_member6 == '5959c36a55c57339a774ff8c' || $id_member7 == '5959c36a55c57339a774ff8c' || $id_member8 == '5959c36a55c57339a774ff8c'
                    || $id_member9 == '5959c36a55c57339a774ff8c' || $id_member10 == '5959c36a55c57339a774ff8c'){
                        $dataMember = "<@U62TFQL1Y>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    } 
                    //azmi
                    if($id_member1 == '58b66821144d4b68a5ab1c84' || $id_member2 == '58b66821144d4b68a5ab1c84' || $id_member3 == '58b66821144d4b68a5ab1c84' || $id_member4 == '58b66821144d4b68a5ab1c84'
                    || $id_member5 == '58b66821144d4b68a5ab1c84' || $id_member6 == '58b66821144d4b68a5ab1c84' || $id_member7 == '58b66821144d4b68a5ab1c84' || $id_member8 == '58b66821144d4b68a5ab1c84'
                    || $id_member9 == '58b66821144d4b68a5ab1c84' || $id_member10 == '58b66821144d4b68a5ab1c84'){
                        $dataMember = "<@U5A2WEHEH>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //alex
                    if($id_member1 == '5b70ef61add7a23d75e11a44' || $id_member2 == '5b70ef61add7a23d75e11a44' || $id_member3 == '5b70ef61add7a23d75e11a44' || $id_member4 == '5b70ef61add7a23d75e11a44'
                    || $id_member5 == '5b70ef61add7a23d75e11a44' || $id_member6 == '5b70ef61add7a23d75e11a44' || $id_member7 == '5b70ef61add7a23d75e11a44' || $id_member8 == '5b70ef61add7a23d75e11a44'
                    || $id_member9 == '5b70ef61add7a23d75e11a44' || $id_member10 == '5b70ef61add7a23d75e11a44'){
                        $dataMember = "<@U9DV9JUET>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //lee jen ling
                    if($id_member1 == '5c3bf62e0581ed89ee7b6580' || $id_member2 == '5c3bf62e0581ed89ee7b6580' || $id_member3 == '5c3bf62e0581ed89ee7b6580' || $id_member4 == '5c3bf62e0581ed89ee7b6580'
                    || $id_member5 == '5c3bf62e0581ed89ee7b6580' || $id_member6 == '5c3bf62e0581ed89ee7b6580' || $id_member7 == '5c3bf62e0581ed89ee7b6580' || $id_member8 == '5c3bf62e0581ed89ee7b6580'
                    || $id_member9 == '5c3bf62e0581ed89ee7b6580' || $id_member10 == '5c3bf62e0581ed89ee7b6580'){
                        $dataMember = "<@UFCJUGKDH>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //priya
                    if($id_member1 == '5b70f173191434079ebf6420' || $id_member2 == '5b70f173191434079ebf6420' || $id_member3 == '5b70f173191434079ebf6420' || $id_member4 == '5b70f173191434079ebf6420'
                    || $id_member5 == '5b70f173191434079ebf6420' || $id_member6 == '5b70f173191434079ebf6420' || $id_member7 == '5b70f173191434079ebf6420' || $id_member8 == '5b70f173191434079ebf6420'
                    || $id_member9 == '5b70f173191434079ebf6420' || $id_member10 == '5b70f173191434079ebf6420'){
                        $dataMember = "<@UCGKY4P2T>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //jeremy
                    if($id_member1 == '559cce487e609257b9b668e8' || $id_member2 == '559cce487e609257b9b668e8' || $id_member3 == '559cce487e609257b9b668e8' || $id_member4 == '559cce487e609257b9b668e8'
                    || $id_member5 == '559cce487e609257b9b668e8' || $id_member6 == '559cce487e609257b9b668e8' || $id_member7 == '559cce487e609257b9b668e8' || $id_member8 == '559cce487e609257b9b668e8'
                    || $id_member9 == '559cce487e609257b9b668e8' || $id_member10 == '559cce487e609257b9b668e8'){
                        $dataMember = "<@U59LLD0JE>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //rex
                    if($id_member1 == '5c3bf7a26814db37f6b41ff2' || $id_member2 == '5c3bf7a26814db37f6b41ff2' || $id_member3 == '5c3bf7a26814db37f6b41ff2' || $id_member4 == '5c3bf7a26814db37f6b41ff2'
                    || $id_member5 == '5c3bf7a26814db37f6b41ff2' || $id_member6 == '5c3bf7a26814db37f6b41ff2' || $id_member7 == '5c3bf7a26814db37f6b41ff2' || $id_member8 == '5c3bf7a26814db37f6b41ff2'
                    || $id_member9 == '5c3bf7a26814db37f6b41ff2' || $id_member10 == '5c3bf7a26814db37f6b41ff2'){
                        $dataMember = "<@UFC03D588>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //lee ying
                    if($id_member1 == '590c33ec597ce7d26734f355' || $id_member2 == '590c33ec597ce7d26734f355' || $id_member3 == '590c33ec597ce7d26734f355' || $id_member4 == '590c33ec597ce7d26734f355'
                    || $id_member5 == '590c33ec597ce7d26734f355' || $id_member6 == '590c33ec597ce7d26734f355' || $id_member7 == '590c33ec597ce7d26734f355' || $id_member8 == '590c33ec597ce7d26734f355'
                    || $id_member9 == '590c33ec597ce7d26734f355' || $id_member10 == '590c33ec597ce7d26734f355'){
                        $dataMember = "<@U591PGRJ4>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //shah
                    if($id_member1 == '58acfc97f4b21ebff852f886' || $id_member2 == '58acfc97f4b21ebff852f886' || $id_member3 == '58acfc97f4b21ebff852f886' || $id_member4 == '58acfc97f4b21ebff852f886'
                    || $id_member5 == '58acfc97f4b21ebff852f886' || $id_member6 == '58acfc97f4b21ebff852f886' || $id_member7 == '58acfc97f4b21ebff852f886' || $id_member8 == '58acfc97f4b21ebff852f886'
                    || $id_member9 == '58acfc97f4b21ebff852f886' || $id_member10 == '58acfc97f4b21ebff852f886'){
                        $dataMember = "<@U59L90G66>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //mrs. suhana
                    if($id_member1 == '58abdb1a88c1ba56168f5d9e' || $id_member2 == '58abdb1a88c1ba56168f5d9e' || $id_member3 == '58abdb1a88c1ba56168f5d9e' || $id_member4 == '58abdb1a88c1ba56168f5d9e'
                    || $id_member5 == '58abdb1a88c1ba56168f5d9e' || $id_member6 == '58abdb1a88c1ba56168f5d9e' || $id_member7 == '58abdb1a88c1ba56168f5d9e' || $id_member8 == '58abdb1a88c1ba56168f5d9e'
                    || $id_member9 == '58abdb1a88c1ba56168f5d9e' || $id_member10 == '58abdb1a88c1ba56168f5d9e'){
                        $dataMember = "<@U59MSGE12>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }         
                }
            }
        }
        $i++;
    }
}

//get data from Writing list
function GetDataWritingList(){
    $devListID  = '5bceba7f62850a31fb5160de';
    $key        = 'cee15f8c5b0d25a32783f5139b64edb4';
    $token      = '0a9207e773917d4738910c973994011eea5bc955f93e59cdff9d1b7f1ab8776d';
    $APIURI     = 'https://api.trello.com/1/lists/'.$devListID.'/cards?cards=visible&card_member_fields=all&key='.$key.'&token='.$token;

    $init       = curl_init();
    curl_setopt($init, CURLOPT_URL, $APIURI); //Url together with parameters
    curl_setopt($init, CURLOPT_RETURNTRANSFER, 1); //Return data instead printing directly in Browser
    curl_setopt($init, CURLOPT_CONNECTTIMEOUT , 7); //Timeout after 7 seconds
    curl_setopt($init, CURLOPT_USERAGENT , "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
    curl_setopt($init, CURLOPT_HEADER, 0);

    $exec       = curl_exec($init);
    $close      = curl_close($init);

    $decodeJSON = json_decode($exec, true);
    $countJsonArray  = count($decodeJSON);
    
    $i = 0;
    while($i < $countJsonArray){
        // echo "data - ".$i.'<br>';
        foreach($decodeJSON[$i]['labels'] as $labelsData){
            //data  from labels array
            $labelName      = $labelsData['name'];
            $labelID        = $labelsData['id'];
            $labelColor     = $labelsData['color'];
            
            //data from card json return
            $cardName       = $decodeJSON[$i]['name'];
            $cardID         = $decodeJSON[$i]['id'];
            $cardURL        = $decodeJSON[$i]['url'];
            $memberID       = $decodeJSON[$i]['idMembers'];
            
            if((strpos($labelName, 'Day 7+') !== false)){
                //fetch memberID array
                if(count($memberID) == 1){
                    $memberIDs  = $memberID[0];

                    //arief
                    if(strpos($memberIDs, '5959c36a55c57339a774ff8c') !== false){
                        $dataMember = "<@U62TFQL1Y>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //azmi
                    if(strpos($memberIDs, '58b66821144d4b68a5ab1c84') !== false){
                        $dataMember = "<@U5A2WEHEH>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //alex
                    if(strpos($memberIDs, '5b70ef61add7a23d75e11a44') !== false){
                        $dataMember = "<@U9DV9JUET>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //leeJenLing
                    if(strpos($memberIDs, '5c3bf62e0581ed89ee7b6580') !== false){
                        $dataMember = "<@UFCJUGKDH>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //priya
                    if(strpos($memberIDs, '5b70f173191434079ebf6420') !== false){
                        $dataMember = "<@UCGKY4P2T>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //jeremy
                    if(strpos($memberIDs, '559cce487e609257b9b668e8') !== false){
                        $dataMember = "<@U59LLD0JE>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //rex
                    if(strpos($memberIDs, '5c3bf7a26814db37f6b41ff2') !== false){
                        $dataMember = "<@UFC03D588>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //leeYing
                    if(strpos($memberIDs, '590c33ec597ce7d26734f355') !== false){
                        $dataMember = "<@U591PGRJ4>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //shah
                    if(strpos($memberIDs, '58acfc97f4b21ebff852f886') !== false){
                        $dataMember = "<@U59L90G66>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //mrs. suhana
                    if(strpos($memberIDs, '58abdb1a88c1ba56168f5d9e') !== false){
                        $dataMember = "<@U59MSGE12>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    
                } else {
                    $id_member1 = $memberID[0];
                    $id_member2 = $memberID[1];
                    $id_member3 = $memberID[2];
                    $id_member4 = $memberID[3];
                    $id_member5 = $memberID[4];
                    $id_member6 = $memberID[5];
                    $id_member7 = $memberID[6];
                    $id_member8 = $memberID[7];
                    $id_member9 = $memberID[8];
                    $id_member10 = $memberID[9];

                    //arief
                    if($id_member1 == '5959c36a55c57339a774ff8c' || $id_member2 == '5959c36a55c57339a774ff8c' || $id_member3 == '5959c36a55c57339a774ff8c' || $id_member4 == '5959c36a55c57339a774ff8c'
                    || $id_member5 == '5959c36a55c57339a774ff8c' || $id_member6 == '5959c36a55c57339a774ff8c' || $id_member7 == '5959c36a55c57339a774ff8c' || $id_member8 == '5959c36a55c57339a774ff8c'
                    || $id_member9 == '5959c36a55c57339a774ff8c' || $id_member10 == '5959c36a55c57339a774ff8c'){
                        $dataMember = "<@U62TFQL1Y>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    } 
                    //azmi
                    if($id_member1 == '58b66821144d4b68a5ab1c84' || $id_member2 == '58b66821144d4b68a5ab1c84' || $id_member3 == '58b66821144d4b68a5ab1c84' || $id_member4 == '58b66821144d4b68a5ab1c84'
                    || $id_member5 == '58b66821144d4b68a5ab1c84' || $id_member6 == '58b66821144d4b68a5ab1c84' || $id_member7 == '58b66821144d4b68a5ab1c84' || $id_member8 == '58b66821144d4b68a5ab1c84'
                    || $id_member9 == '58b66821144d4b68a5ab1c84' || $id_member10 == '58b66821144d4b68a5ab1c84'){
                        $dataMember = "<@U5A2WEHEH>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //alex
                    if($id_member1 == '5b70ef61add7a23d75e11a44' || $id_member2 == '5b70ef61add7a23d75e11a44' || $id_member3 == '5b70ef61add7a23d75e11a44' || $id_member4 == '5b70ef61add7a23d75e11a44'
                    || $id_member5 == '5b70ef61add7a23d75e11a44' || $id_member6 == '5b70ef61add7a23d75e11a44' || $id_member7 == '5b70ef61add7a23d75e11a44' || $id_member8 == '5b70ef61add7a23d75e11a44'
                    || $id_member9 == '5b70ef61add7a23d75e11a44' || $id_member10 == '5b70ef61add7a23d75e11a44'){
                        $dataMember = "<@U9DV9JUET>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //lee jen ling
                    if($id_member1 == '5c3bf62e0581ed89ee7b6580' || $id_member2 == '5c3bf62e0581ed89ee7b6580' || $id_member3 == '5c3bf62e0581ed89ee7b6580' || $id_member4 == '5c3bf62e0581ed89ee7b6580'
                    || $id_member5 == '5c3bf62e0581ed89ee7b6580' || $id_member6 == '5c3bf62e0581ed89ee7b6580' || $id_member7 == '5c3bf62e0581ed89ee7b6580' || $id_member8 == '5c3bf62e0581ed89ee7b6580'
                    || $id_member9 == '5c3bf62e0581ed89ee7b6580' || $id_member10 == '5c3bf62e0581ed89ee7b6580'){
                        $dataMember = "<@UFCJUGKDH>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //priya
                    if($id_member1 == '5b70f173191434079ebf6420' || $id_member2 == '5b70f173191434079ebf6420' || $id_member3 == '5b70f173191434079ebf6420' || $id_member4 == '5b70f173191434079ebf6420'
                    || $id_member5 == '5b70f173191434079ebf6420' || $id_member6 == '5b70f173191434079ebf6420' || $id_member7 == '5b70f173191434079ebf6420' || $id_member8 == '5b70f173191434079ebf6420'
                    || $id_member9 == '5b70f173191434079ebf6420' || $id_member10 == '5b70f173191434079ebf6420'){
                        $dataMember = "<@UCGKY4P2T>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //jeremy
                    if($id_member1 == '559cce487e609257b9b668e8' || $id_member2 == '559cce487e609257b9b668e8' || $id_member3 == '559cce487e609257b9b668e8' || $id_member4 == '559cce487e609257b9b668e8'
                    || $id_member5 == '559cce487e609257b9b668e8' || $id_member6 == '559cce487e609257b9b668e8' || $id_member7 == '559cce487e609257b9b668e8' || $id_member8 == '559cce487e609257b9b668e8'
                    || $id_member9 == '559cce487e609257b9b668e8' || $id_member10 == '559cce487e609257b9b668e8'){
                        $dataMember = "<@U59LLD0JE>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //rex
                    if($id_member1 == '5c3bf7a26814db37f6b41ff2' || $id_member2 == '5c3bf7a26814db37f6b41ff2' || $id_member3 == '5c3bf7a26814db37f6b41ff2' || $id_member4 == '5c3bf7a26814db37f6b41ff2'
                    || $id_member5 == '5c3bf7a26814db37f6b41ff2' || $id_member6 == '5c3bf7a26814db37f6b41ff2' || $id_member7 == '5c3bf7a26814db37f6b41ff2' || $id_member8 == '5c3bf7a26814db37f6b41ff2'
                    || $id_member9 == '5c3bf7a26814db37f6b41ff2' || $id_member10 == '5c3bf7a26814db37f6b41ff2'){
                        $dataMember = "<@UFC03D588>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //lee ying
                    if($id_member1 == '590c33ec597ce7d26734f355' || $id_member2 == '590c33ec597ce7d26734f355' || $id_member3 == '590c33ec597ce7d26734f355' || $id_member4 == '590c33ec597ce7d26734f355'
                    || $id_member5 == '590c33ec597ce7d26734f355' || $id_member6 == '590c33ec597ce7d26734f355' || $id_member7 == '590c33ec597ce7d26734f355' || $id_member8 == '590c33ec597ce7d26734f355'
                    || $id_member9 == '590c33ec597ce7d26734f355' || $id_member10 == '590c33ec597ce7d26734f355'){
                        $dataMember = "<@U591PGRJ4>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //shah
                    if($id_member1 == '58acfc97f4b21ebff852f886' || $id_member2 == '58acfc97f4b21ebff852f886' || $id_member3 == '58acfc97f4b21ebff852f886' || $id_member4 == '58acfc97f4b21ebff852f886'
                    || $id_member5 == '58acfc97f4b21ebff852f886' || $id_member6 == '58acfc97f4b21ebff852f886' || $id_member7 == '58acfc97f4b21ebff852f886' || $id_member8 == '58acfc97f4b21ebff852f886'
                    || $id_member9 == '58acfc97f4b21ebff852f886' || $id_member10 == '58acfc97f4b21ebff852f886'){
                        $dataMember = "<@U59L90G66>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //mrs. suhana
                    if($id_member1 == '58abdb1a88c1ba56168f5d9e' || $id_member2 == '58abdb1a88c1ba56168f5d9e' || $id_member3 == '58abdb1a88c1ba56168f5d9e' || $id_member4 == '58abdb1a88c1ba56168f5d9e'
                    || $id_member5 == '58abdb1a88c1ba56168f5d9e' || $id_member6 == '58abdb1a88c1ba56168f5d9e' || $id_member7 == '58abdb1a88c1ba56168f5d9e' || $id_member8 == '58abdb1a88c1ba56168f5d9e'
                    || $id_member9 == '58abdb1a88c1ba56168f5d9e' || $id_member10 == '58abdb1a88c1ba56168f5d9e'){
                        $dataMember = "<@U59MSGE12>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }         
                }
            }
        }
        $i++;
    }
}

//get data from Art list
function GetDataArtList(){
    $devListID  = '5bceba7f62850a31fb5160dd';
    $key        = 'cee15f8c5b0d25a32783f5139b64edb4';
    $token      = '0a9207e773917d4738910c973994011eea5bc955f93e59cdff9d1b7f1ab8776d';
    $APIURI     = 'https://api.trello.com/1/lists/'.$devListID.'/cards?cards=visible&card_member_fields=all&key='.$key.'&token='.$token;

    $init       = curl_init();
    curl_setopt($init, CURLOPT_URL, $APIURI); //Url together with parameters
    curl_setopt($init, CURLOPT_RETURNTRANSFER, 1); //Return data instead printing directly in Browser
    curl_setopt($init, CURLOPT_CONNECTTIMEOUT , 7); //Timeout after 7 seconds
    curl_setopt($init, CURLOPT_USERAGENT , "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
    curl_setopt($init, CURLOPT_HEADER, 0);

    $exec       = curl_exec($init);
    $close      = curl_close($init);

    $decodeJSON = json_decode($exec, true);
    $countJsonArray  = count($decodeJSON);
    
    $i = 0;
    while($i < $countJsonArray){
        // echo "data - ".$i.'<br>';
        foreach($decodeJSON[$i]['labels'] as $labelsData){
            //data  from labels array
            $labelName      = $labelsData['name'];
            $labelID        = $labelsData['id'];
            $labelColor     = $labelsData['color'];
            
            //data from card json return
            $cardName       = $decodeJSON[$i]['name'];
            $cardID         = $decodeJSON[$i]['id'];
            $cardURL        = $decodeJSON[$i]['url'];
            $memberID       = $decodeJSON[$i]['idMembers'];
            
            if((strpos($labelName, 'Day 7+') !== false)){
                //fetch memberID array
                if(count($memberID) == 1){
                    $memberIDs  = $memberID[0];

                    //arief
                    if(strpos($memberIDs, '5959c36a55c57339a774ff8c') !== false){
                        $dataMember = "<@U62TFQL1Y>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //azmi
                    if(strpos($memberIDs, '58b66821144d4b68a5ab1c84') !== false){
                        $dataMember = "<@U5A2WEHEH>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //alex
                    if(strpos($memberIDs, '5b70ef61add7a23d75e11a44') !== false){
                        $dataMember = "<@U9DV9JUET>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //leeJenLing
                    if(strpos($memberIDs, '5c3bf62e0581ed89ee7b6580') !== false){
                        $dataMember = "<@UFCJUGKDH>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //priya
                    if(strpos($memberIDs, '5b70f173191434079ebf6420') !== false){
                        $dataMember = "<@UCGKY4P2T>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //jeremy
                    if(strpos($memberIDs, '559cce487e609257b9b668e8') !== false){
                        $dataMember = "<@U59LLD0JE>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //rex
                    if(strpos($memberIDs, '5c3bf7a26814db37f6b41ff2') !== false){
                        $dataMember = "<@UFC03D588>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //leeYing
                    if(strpos($memberIDs, '590c33ec597ce7d26734f355') !== false){
                        $dataMember = "<@U591PGRJ4>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //shah
                    if(strpos($memberIDs, '58acfc97f4b21ebff852f886') !== false){
                        $dataMember = "<@U59L90G66>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //mrs. suhana
                    if(strpos($memberIDs, '58abdb1a88c1ba56168f5d9e') !== false){
                        $dataMember = "<@U59MSGE12>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    
                } else {
                    $id_member1 = $memberID[0];
                    $id_member2 = $memberID[1];
                    $id_member3 = $memberID[2];
                    $id_member4 = $memberID[3];
                    $id_member5 = $memberID[4];
                    $id_member6 = $memberID[5];
                    $id_member7 = $memberID[6];
                    $id_member8 = $memberID[7];
                    $id_member9 = $memberID[8];
                    $id_member10 = $memberID[9];

                    //arief
                    if($id_member1 == '5959c36a55c57339a774ff8c' || $id_member2 == '5959c36a55c57339a774ff8c' || $id_member3 == '5959c36a55c57339a774ff8c' || $id_member4 == '5959c36a55c57339a774ff8c'
                    || $id_member5 == '5959c36a55c57339a774ff8c' || $id_member6 == '5959c36a55c57339a774ff8c' || $id_member7 == '5959c36a55c57339a774ff8c' || $id_member8 == '5959c36a55c57339a774ff8c'
                    || $id_member9 == '5959c36a55c57339a774ff8c' || $id_member10 == '5959c36a55c57339a774ff8c'){
                        $dataMember = "<@U62TFQL1Y>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    } 
                    //azmi
                    if($id_member1 == '58b66821144d4b68a5ab1c84' || $id_member2 == '58b66821144d4b68a5ab1c84' || $id_member3 == '58b66821144d4b68a5ab1c84' || $id_member4 == '58b66821144d4b68a5ab1c84'
                    || $id_member5 == '58b66821144d4b68a5ab1c84' || $id_member6 == '58b66821144d4b68a5ab1c84' || $id_member7 == '58b66821144d4b68a5ab1c84' || $id_member8 == '58b66821144d4b68a5ab1c84'
                    || $id_member9 == '58b66821144d4b68a5ab1c84' || $id_member10 == '58b66821144d4b68a5ab1c84'){
                        $dataMember = "<@U5A2WEHEH>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //alex
                    if($id_member1 == '5b70ef61add7a23d75e11a44' || $id_member2 == '5b70ef61add7a23d75e11a44' || $id_member3 == '5b70ef61add7a23d75e11a44' || $id_member4 == '5b70ef61add7a23d75e11a44'
                    || $id_member5 == '5b70ef61add7a23d75e11a44' || $id_member6 == '5b70ef61add7a23d75e11a44' || $id_member7 == '5b70ef61add7a23d75e11a44' || $id_member8 == '5b70ef61add7a23d75e11a44'
                    || $id_member9 == '5b70ef61add7a23d75e11a44' || $id_member10 == '5b70ef61add7a23d75e11a44'){
                        $dataMember = "<@U9DV9JUET>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //lee jen ling
                    if($id_member1 == '5c3bf62e0581ed89ee7b6580' || $id_member2 == '5c3bf62e0581ed89ee7b6580' || $id_member3 == '5c3bf62e0581ed89ee7b6580' || $id_member4 == '5c3bf62e0581ed89ee7b6580'
                    || $id_member5 == '5c3bf62e0581ed89ee7b6580' || $id_member6 == '5c3bf62e0581ed89ee7b6580' || $id_member7 == '5c3bf62e0581ed89ee7b6580' || $id_member8 == '5c3bf62e0581ed89ee7b6580'
                    || $id_member9 == '5c3bf62e0581ed89ee7b6580' || $id_member10 == '5c3bf62e0581ed89ee7b6580'){
                        $dataMember = "<@UFCJUGKDH>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //priya
                    if($id_member1 == '5b70f173191434079ebf6420' || $id_member2 == '5b70f173191434079ebf6420' || $id_member3 == '5b70f173191434079ebf6420' || $id_member4 == '5b70f173191434079ebf6420'
                    || $id_member5 == '5b70f173191434079ebf6420' || $id_member6 == '5b70f173191434079ebf6420' || $id_member7 == '5b70f173191434079ebf6420' || $id_member8 == '5b70f173191434079ebf6420'
                    || $id_member9 == '5b70f173191434079ebf6420' || $id_member10 == '5b70f173191434079ebf6420'){
                        $dataMember = "<@UCGKY4P2T>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //jeremy
                    if($id_member1 == '559cce487e609257b9b668e8' || $id_member2 == '559cce487e609257b9b668e8' || $id_member3 == '559cce487e609257b9b668e8' || $id_member4 == '559cce487e609257b9b668e8'
                    || $id_member5 == '559cce487e609257b9b668e8' || $id_member6 == '559cce487e609257b9b668e8' || $id_member7 == '559cce487e609257b9b668e8' || $id_member8 == '559cce487e609257b9b668e8'
                    || $id_member9 == '559cce487e609257b9b668e8' || $id_member10 == '559cce487e609257b9b668e8'){
                        $dataMember = "<@U59LLD0JE>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //rex
                    if($id_member1 == '5c3bf7a26814db37f6b41ff2' || $id_member2 == '5c3bf7a26814db37f6b41ff2' || $id_member3 == '5c3bf7a26814db37f6b41ff2' || $id_member4 == '5c3bf7a26814db37f6b41ff2'
                    || $id_member5 == '5c3bf7a26814db37f6b41ff2' || $id_member6 == '5c3bf7a26814db37f6b41ff2' || $id_member7 == '5c3bf7a26814db37f6b41ff2' || $id_member8 == '5c3bf7a26814db37f6b41ff2'
                    || $id_member9 == '5c3bf7a26814db37f6b41ff2' || $id_member10 == '5c3bf7a26814db37f6b41ff2'){
                        $dataMember = "<@UFC03D588>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //lee ying
                    if($id_member1 == '590c33ec597ce7d26734f355' || $id_member2 == '590c33ec597ce7d26734f355' || $id_member3 == '590c33ec597ce7d26734f355' || $id_member4 == '590c33ec597ce7d26734f355'
                    || $id_member5 == '590c33ec597ce7d26734f355' || $id_member6 == '590c33ec597ce7d26734f355' || $id_member7 == '590c33ec597ce7d26734f355' || $id_member8 == '590c33ec597ce7d26734f355'
                    || $id_member9 == '590c33ec597ce7d26734f355' || $id_member10 == '590c33ec597ce7d26734f355'){
                        $dataMember = "<@U591PGRJ4>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //shah
                    if($id_member1 == '58acfc97f4b21ebff852f886' || $id_member2 == '58acfc97f4b21ebff852f886' || $id_member3 == '58acfc97f4b21ebff852f886' || $id_member4 == '58acfc97f4b21ebff852f886'
                    || $id_member5 == '58acfc97f4b21ebff852f886' || $id_member6 == '58acfc97f4b21ebff852f886' || $id_member7 == '58acfc97f4b21ebff852f886' || $id_member8 == '58acfc97f4b21ebff852f886'
                    || $id_member9 == '58acfc97f4b21ebff852f886' || $id_member10 == '58acfc97f4b21ebff852f886'){
                        $dataMember = "<@U59L90G66>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //mrs. suhana
                    if($id_member1 == '58abdb1a88c1ba56168f5d9e' || $id_member2 == '58abdb1a88c1ba56168f5d9e' || $id_member3 == '58abdb1a88c1ba56168f5d9e' || $id_member4 == '58abdb1a88c1ba56168f5d9e'
                    || $id_member5 == '58abdb1a88c1ba56168f5d9e' || $id_member6 == '58abdb1a88c1ba56168f5d9e' || $id_member7 == '58abdb1a88c1ba56168f5d9e' || $id_member8 == '58abdb1a88c1ba56168f5d9e'
                    || $id_member9 == '58abdb1a88c1ba56168f5d9e' || $id_member10 == '58abdb1a88c1ba56168f5d9e'){
                        $dataMember = "<@U59MSGE12>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }         
                }
            }
        }
        $i++;
    }
}

//get data from Sound list
function GetDataSoundList(){
    $devListID  = '5c33089944403668623df872';
    $key        = 'cee15f8c5b0d25a32783f5139b64edb4';
    $token      = '0a9207e773917d4738910c973994011eea5bc955f93e59cdff9d1b7f1ab8776d';
    $APIURI     = 'https://api.trello.com/1/lists/'.$devListID.'/cards?cards=visible&card_member_fields=all&key='.$key.'&token='.$token;

    $init       = curl_init();
    curl_setopt($init, CURLOPT_URL, $APIURI); //Url together with parameters
    curl_setopt($init, CURLOPT_RETURNTRANSFER, 1); //Return data instead printing directly in Browser
    curl_setopt($init, CURLOPT_CONNECTTIMEOUT , 7); //Timeout after 7 seconds
    curl_setopt($init, CURLOPT_USERAGENT , "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
    curl_setopt($init, CURLOPT_HEADER, 0);

    $exec       = curl_exec($init);
    $close      = curl_close($init);

    $decodeJSON = json_decode($exec, true);
    $countJsonArray  = count($decodeJSON);
    
    $i = 0;
    while($i < $countJsonArray){
        // echo "data - ".$i.'<br>';
        foreach($decodeJSON[$i]['labels'] as $labelsData){
            //data  from labels array
            $labelName      = $labelsData['name'];
            $labelID        = $labelsData['id'];
            $labelColor     = $labelsData['color'];
            
            //data from card json return
            $cardName       = $decodeJSON[$i]['name'];
            $cardID         = $decodeJSON[$i]['id'];
            $cardURL        = $decodeJSON[$i]['url'];
            $memberID       = $decodeJSON[$i]['idMembers'];
            
            if((strpos($labelName, 'Day 7+') !== false)){
                //fetch memberID array
                if(count($memberID) == 1){
                    $memberIDs  = $memberID[0];

                    //arief
                    if(strpos($memberIDs, '5959c36a55c57339a774ff8c') !== false){
                        $dataMember = "<@U62TFQL1Y>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //azmi
                    if(strpos($memberIDs, '58b66821144d4b68a5ab1c84') !== false){
                        $dataMember = "<@U5A2WEHEH>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //alex
                    if(strpos($memberIDs, '5b70ef61add7a23d75e11a44') !== false){
                        $dataMember = "<@U9DV9JUET>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //leeJenLing
                    if(strpos($memberIDs, '5c3bf62e0581ed89ee7b6580') !== false){
                        $dataMember = "<@UFCJUGKDH>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //priya
                    if(strpos($memberIDs, '5b70f173191434079ebf6420') !== false){
                        $dataMember = "<@UCGKY4P2T>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //jeremy
                    if(strpos($memberIDs, '559cce487e609257b9b668e8') !== false){
                        $dataMember = "<@U59LLD0JE>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //rex
                    if(strpos($memberIDs, '5c3bf7a26814db37f6b41ff2') !== false){
                        $dataMember = "<@UFC03D588>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //leeYing
                    if(strpos($memberIDs, '590c33ec597ce7d26734f355') !== false){
                        $dataMember = "<@U591PGRJ4>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //shah
                    if(strpos($memberIDs, '58acfc97f4b21ebff852f886') !== false){
                        $dataMember = "<@U59L90G66>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //mrs. suhana
                    if(strpos($memberIDs, '58abdb1a88c1ba56168f5d9e') !== false){
                        $dataMember = "<@U59MSGE12>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    
                } else {
                    $id_member1 = $memberID[0];
                    $id_member2 = $memberID[1];
                    $id_member3 = $memberID[2];
                    $id_member4 = $memberID[3];
                    $id_member5 = $memberID[4];
                    $id_member6 = $memberID[5];
                    $id_member7 = $memberID[6];
                    $id_member8 = $memberID[7];
                    $id_member9 = $memberID[8];
                    $id_member10 = $memberID[9];

                    //arief
                    if($id_member1 == '5959c36a55c57339a774ff8c' || $id_member2 == '5959c36a55c57339a774ff8c' || $id_member3 == '5959c36a55c57339a774ff8c' || $id_member4 == '5959c36a55c57339a774ff8c'
                    || $id_member5 == '5959c36a55c57339a774ff8c' || $id_member6 == '5959c36a55c57339a774ff8c' || $id_member7 == '5959c36a55c57339a774ff8c' || $id_member8 == '5959c36a55c57339a774ff8c'
                    || $id_member9 == '5959c36a55c57339a774ff8c' || $id_member10 == '5959c36a55c57339a774ff8c'){
                        $dataMember = "<@U62TFQL1Y>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    } 
                    //azmi
                    if($id_member1 == '58b66821144d4b68a5ab1c84' || $id_member2 == '58b66821144d4b68a5ab1c84' || $id_member3 == '58b66821144d4b68a5ab1c84' || $id_member4 == '58b66821144d4b68a5ab1c84'
                    || $id_member5 == '58b66821144d4b68a5ab1c84' || $id_member6 == '58b66821144d4b68a5ab1c84' || $id_member7 == '58b66821144d4b68a5ab1c84' || $id_member8 == '58b66821144d4b68a5ab1c84'
                    || $id_member9 == '58b66821144d4b68a5ab1c84' || $id_member10 == '58b66821144d4b68a5ab1c84'){
                        $dataMember = "<@U5A2WEHEH>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //alex
                    if($id_member1 == '5b70ef61add7a23d75e11a44' || $id_member2 == '5b70ef61add7a23d75e11a44' || $id_member3 == '5b70ef61add7a23d75e11a44' || $id_member4 == '5b70ef61add7a23d75e11a44'
                    || $id_member5 == '5b70ef61add7a23d75e11a44' || $id_member6 == '5b70ef61add7a23d75e11a44' || $id_member7 == '5b70ef61add7a23d75e11a44' || $id_member8 == '5b70ef61add7a23d75e11a44'
                    || $id_member9 == '5b70ef61add7a23d75e11a44' || $id_member10 == '5b70ef61add7a23d75e11a44'){
                        $dataMember = "<@U9DV9JUET>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //lee jen ling
                    if($id_member1 == '5c3bf62e0581ed89ee7b6580' || $id_member2 == '5c3bf62e0581ed89ee7b6580' || $id_member3 == '5c3bf62e0581ed89ee7b6580' || $id_member4 == '5c3bf62e0581ed89ee7b6580'
                    || $id_member5 == '5c3bf62e0581ed89ee7b6580' || $id_member6 == '5c3bf62e0581ed89ee7b6580' || $id_member7 == '5c3bf62e0581ed89ee7b6580' || $id_member8 == '5c3bf62e0581ed89ee7b6580'
                    || $id_member9 == '5c3bf62e0581ed89ee7b6580' || $id_member10 == '5c3bf62e0581ed89ee7b6580'){
                        $dataMember = "<@UFCJUGKDH>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //priya
                    if($id_member1 == '5b70f173191434079ebf6420' || $id_member2 == '5b70f173191434079ebf6420' || $id_member3 == '5b70f173191434079ebf6420' || $id_member4 == '5b70f173191434079ebf6420'
                    || $id_member5 == '5b70f173191434079ebf6420' || $id_member6 == '5b70f173191434079ebf6420' || $id_member7 == '5b70f173191434079ebf6420' || $id_member8 == '5b70f173191434079ebf6420'
                    || $id_member9 == '5b70f173191434079ebf6420' || $id_member10 == '5b70f173191434079ebf6420'){
                        $dataMember = "<@UCGKY4P2T>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //jeremy
                    if($id_member1 == '559cce487e609257b9b668e8' || $id_member2 == '559cce487e609257b9b668e8' || $id_member3 == '559cce487e609257b9b668e8' || $id_member4 == '559cce487e609257b9b668e8'
                    || $id_member5 == '559cce487e609257b9b668e8' || $id_member6 == '559cce487e609257b9b668e8' || $id_member7 == '559cce487e609257b9b668e8' || $id_member8 == '559cce487e609257b9b668e8'
                    || $id_member9 == '559cce487e609257b9b668e8' || $id_member10 == '559cce487e609257b9b668e8'){
                        $dataMember = "<@U59LLD0JE>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //rex
                    if($id_member1 == '5c3bf7a26814db37f6b41ff2' || $id_member2 == '5c3bf7a26814db37f6b41ff2' || $id_member3 == '5c3bf7a26814db37f6b41ff2' || $id_member4 == '5c3bf7a26814db37f6b41ff2'
                    || $id_member5 == '5c3bf7a26814db37f6b41ff2' || $id_member6 == '5c3bf7a26814db37f6b41ff2' || $id_member7 == '5c3bf7a26814db37f6b41ff2' || $id_member8 == '5c3bf7a26814db37f6b41ff2'
                    || $id_member9 == '5c3bf7a26814db37f6b41ff2' || $id_member10 == '5c3bf7a26814db37f6b41ff2'){
                        $dataMember = "<@UFC03D588>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //lee ying
                    if($id_member1 == '590c33ec597ce7d26734f355' || $id_member2 == '590c33ec597ce7d26734f355' || $id_member3 == '590c33ec597ce7d26734f355' || $id_member4 == '590c33ec597ce7d26734f355'
                    || $id_member5 == '590c33ec597ce7d26734f355' || $id_member6 == '590c33ec597ce7d26734f355' || $id_member7 == '590c33ec597ce7d26734f355' || $id_member8 == '590c33ec597ce7d26734f355'
                    || $id_member9 == '590c33ec597ce7d26734f355' || $id_member10 == '590c33ec597ce7d26734f355'){
                        $dataMember = "<@U591PGRJ4>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //shah
                    if($id_member1 == '58acfc97f4b21ebff852f886' || $id_member2 == '58acfc97f4b21ebff852f886' || $id_member3 == '58acfc97f4b21ebff852f886' || $id_member4 == '58acfc97f4b21ebff852f886'
                    || $id_member5 == '58acfc97f4b21ebff852f886' || $id_member6 == '58acfc97f4b21ebff852f886' || $id_member7 == '58acfc97f4b21ebff852f886' || $id_member8 == '58acfc97f4b21ebff852f886'
                    || $id_member9 == '58acfc97f4b21ebff852f886' || $id_member10 == '58acfc97f4b21ebff852f886'){
                        $dataMember = "<@U59L90G66>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //mrs. suhana
                    if($id_member1 == '58abdb1a88c1ba56168f5d9e' || $id_member2 == '58abdb1a88c1ba56168f5d9e' || $id_member3 == '58abdb1a88c1ba56168f5d9e' || $id_member4 == '58abdb1a88c1ba56168f5d9e'
                    || $id_member5 == '58abdb1a88c1ba56168f5d9e' || $id_member6 == '58abdb1a88c1ba56168f5d9e' || $id_member7 == '58abdb1a88c1ba56168f5d9e' || $id_member8 == '58abdb1a88c1ba56168f5d9e'
                    || $id_member9 == '58abdb1a88c1ba56168f5d9e' || $id_member10 == '58abdb1a88c1ba56168f5d9e'){
                        $dataMember = "<@U59MSGE12>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }         
                }
            }
        }
        $i++;
    }
}

//get data from Operation list
function GetDataOperationList(){
    $devListID  = '5c3308a05bf0e452fb5f1480';
    $key        = 'cee15f8c5b0d25a32783f5139b64edb4';
    $token      = '0a9207e773917d4738910c973994011eea5bc955f93e59cdff9d1b7f1ab8776d';
    $APIURI     = 'https://api.trello.com/1/lists/'.$devListID.'/cards?cards=visible&card_member_fields=all&key='.$key.'&token='.$token;

    $init       = curl_init();
    curl_setopt($init, CURLOPT_URL, $APIURI); //Url together with parameters
    curl_setopt($init, CURLOPT_RETURNTRANSFER, 1); //Return data instead printing directly in Browser
    curl_setopt($init, CURLOPT_CONNECTTIMEOUT , 7); //Timeout after 7 seconds
    curl_setopt($init, CURLOPT_USERAGENT , "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
    curl_setopt($init, CURLOPT_HEADER, 0);

    $exec       = curl_exec($init);
    $close      = curl_close($init);

    $decodeJSON = json_decode($exec, true);
    $countJsonArray  = count($decodeJSON);
    
    $i = 0;
    while($i < $countJsonArray){
        // echo "data - ".$i.'<br>';
        foreach($decodeJSON[$i]['labels'] as $labelsData){
            //data  from labels array
            $labelName      = $labelsData['name'];
            $labelID        = $labelsData['id'];
            $labelColor     = $labelsData['color'];
            
            //data from card json return
            $cardName       = $decodeJSON[$i]['name'];
            $cardID         = $decodeJSON[$i]['id'];
            $cardURL        = $decodeJSON[$i]['url'];
            $memberID       = $decodeJSON[$i]['idMembers'];
            
            if((strpos($labelName, 'Day 7+') !== false)){
                //fetch memberID array
                if(count($memberID) == 1){
                    $memberIDs  = $memberID[0];

                    //arief
                    if(strpos($memberIDs, '5959c36a55c57339a774ff8c') !== false){
                        $dataMember = "<@U62TFQL1Y>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //azmi
                    if(strpos($memberIDs, '58b66821144d4b68a5ab1c84') !== false){
                        $dataMember = "<@U5A2WEHEH>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //alex
                    if(strpos($memberIDs, '5b70ef61add7a23d75e11a44') !== false){
                        $dataMember = "<@U9DV9JUET>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //leeJenLing
                    if(strpos($memberIDs, '5c3bf62e0581ed89ee7b6580') !== false){
                        $dataMember = "<@UFCJUGKDH>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //priya
                    if(strpos($memberIDs, '5b70f173191434079ebf6420') !== false){
                        $dataMember = "<@UCGKY4P2T>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //jeremy
                    if(strpos($memberIDs, '559cce487e609257b9b668e8') !== false){
                        $dataMember = "<@U59LLD0JE>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //rex
                    if(strpos($memberIDs, '5c3bf7a26814db37f6b41ff2') !== false){
                        $dataMember = "<@UFC03D588>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //leeYing
                    if(strpos($memberIDs, '590c33ec597ce7d26734f355') !== false){
                        $dataMember = "<@U591PGRJ4>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //shah
                    if(strpos($memberIDs, '58acfc97f4b21ebff852f886') !== false){
                        $dataMember = "<@U59L90G66>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //mrs. suhana
                    if(strpos($memberIDs, '58abdb1a88c1ba56168f5d9e') !== false){
                        $dataMember = "<@U59MSGE12>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    
                } else {
                    $id_member1 = $memberID[0];
                    $id_member2 = $memberID[1];
                    $id_member3 = $memberID[2];
                    $id_member4 = $memberID[3];
                    $id_member5 = $memberID[4];
                    $id_member6 = $memberID[5];
                    $id_member7 = $memberID[6];
                    $id_member8 = $memberID[7];
                    $id_member9 = $memberID[8];
                    $id_member10 = $memberID[9];

                    //arief
                    if($id_member1 == '5959c36a55c57339a774ff8c' || $id_member2 == '5959c36a55c57339a774ff8c' || $id_member3 == '5959c36a55c57339a774ff8c' || $id_member4 == '5959c36a55c57339a774ff8c'
                    || $id_member5 == '5959c36a55c57339a774ff8c' || $id_member6 == '5959c36a55c57339a774ff8c' || $id_member7 == '5959c36a55c57339a774ff8c' || $id_member8 == '5959c36a55c57339a774ff8c'
                    || $id_member9 == '5959c36a55c57339a774ff8c' || $id_member10 == '5959c36a55c57339a774ff8c'){
                        $dataMember = "<@U62TFQL1Y>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    } 
                    //azmi
                    if($id_member1 == '58b66821144d4b68a5ab1c84' || $id_member2 == '58b66821144d4b68a5ab1c84' || $id_member3 == '58b66821144d4b68a5ab1c84' || $id_member4 == '58b66821144d4b68a5ab1c84'
                    || $id_member5 == '58b66821144d4b68a5ab1c84' || $id_member6 == '58b66821144d4b68a5ab1c84' || $id_member7 == '58b66821144d4b68a5ab1c84' || $id_member8 == '58b66821144d4b68a5ab1c84'
                    || $id_member9 == '58b66821144d4b68a5ab1c84' || $id_member10 == '58b66821144d4b68a5ab1c84'){
                        $dataMember = "<@U5A2WEHEH>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //alex
                    if($id_member1 == '5b70ef61add7a23d75e11a44' || $id_member2 == '5b70ef61add7a23d75e11a44' || $id_member3 == '5b70ef61add7a23d75e11a44' || $id_member4 == '5b70ef61add7a23d75e11a44'
                    || $id_member5 == '5b70ef61add7a23d75e11a44' || $id_member6 == '5b70ef61add7a23d75e11a44' || $id_member7 == '5b70ef61add7a23d75e11a44' || $id_member8 == '5b70ef61add7a23d75e11a44'
                    || $id_member9 == '5b70ef61add7a23d75e11a44' || $id_member10 == '5b70ef61add7a23d75e11a44'){
                        $dataMember = "<@U9DV9JUET>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //lee jen ling
                    if($id_member1 == '5c3bf62e0581ed89ee7b6580' || $id_member2 == '5c3bf62e0581ed89ee7b6580' || $id_member3 == '5c3bf62e0581ed89ee7b6580' || $id_member4 == '5c3bf62e0581ed89ee7b6580'
                    || $id_member5 == '5c3bf62e0581ed89ee7b6580' || $id_member6 == '5c3bf62e0581ed89ee7b6580' || $id_member7 == '5c3bf62e0581ed89ee7b6580' || $id_member8 == '5c3bf62e0581ed89ee7b6580'
                    || $id_member9 == '5c3bf62e0581ed89ee7b6580' || $id_member10 == '5c3bf62e0581ed89ee7b6580'){
                        $dataMember = "<@UFCJUGKDH>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //priya
                    if($id_member1 == '5b70f173191434079ebf6420' || $id_member2 == '5b70f173191434079ebf6420' || $id_member3 == '5b70f173191434079ebf6420' || $id_member4 == '5b70f173191434079ebf6420'
                    || $id_member5 == '5b70f173191434079ebf6420' || $id_member6 == '5b70f173191434079ebf6420' || $id_member7 == '5b70f173191434079ebf6420' || $id_member8 == '5b70f173191434079ebf6420'
                    || $id_member9 == '5b70f173191434079ebf6420' || $id_member10 == '5b70f173191434079ebf6420'){
                        $dataMember = "<@UCGKY4P2T>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //jeremy
                    if($id_member1 == '559cce487e609257b9b668e8' || $id_member2 == '559cce487e609257b9b668e8' || $id_member3 == '559cce487e609257b9b668e8' || $id_member4 == '559cce487e609257b9b668e8'
                    || $id_member5 == '559cce487e609257b9b668e8' || $id_member6 == '559cce487e609257b9b668e8' || $id_member7 == '559cce487e609257b9b668e8' || $id_member8 == '559cce487e609257b9b668e8'
                    || $id_member9 == '559cce487e609257b9b668e8' || $id_member10 == '559cce487e609257b9b668e8'){
                        $dataMember = "<@U59LLD0JE>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //rex
                    if($id_member1 == '5c3bf7a26814db37f6b41ff2' || $id_member2 == '5c3bf7a26814db37f6b41ff2' || $id_member3 == '5c3bf7a26814db37f6b41ff2' || $id_member4 == '5c3bf7a26814db37f6b41ff2'
                    || $id_member5 == '5c3bf7a26814db37f6b41ff2' || $id_member6 == '5c3bf7a26814db37f6b41ff2' || $id_member7 == '5c3bf7a26814db37f6b41ff2' || $id_member8 == '5c3bf7a26814db37f6b41ff2'
                    || $id_member9 == '5c3bf7a26814db37f6b41ff2' || $id_member10 == '5c3bf7a26814db37f6b41ff2'){
                        $dataMember = "<@UFC03D588>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //lee ying
                    if($id_member1 == '590c33ec597ce7d26734f355' || $id_member2 == '590c33ec597ce7d26734f355' || $id_member3 == '590c33ec597ce7d26734f355' || $id_member4 == '590c33ec597ce7d26734f355'
                    || $id_member5 == '590c33ec597ce7d26734f355' || $id_member6 == '590c33ec597ce7d26734f355' || $id_member7 == '590c33ec597ce7d26734f355' || $id_member8 == '590c33ec597ce7d26734f355'
                    || $id_member9 == '590c33ec597ce7d26734f355' || $id_member10 == '590c33ec597ce7d26734f355'){
                        $dataMember = "<@U591PGRJ4>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //shah
                    if($id_member1 == '58acfc97f4b21ebff852f886' || $id_member2 == '58acfc97f4b21ebff852f886' || $id_member3 == '58acfc97f4b21ebff852f886' || $id_member4 == '58acfc97f4b21ebff852f886'
                    || $id_member5 == '58acfc97f4b21ebff852f886' || $id_member6 == '58acfc97f4b21ebff852f886' || $id_member7 == '58acfc97f4b21ebff852f886' || $id_member8 == '58acfc97f4b21ebff852f886'
                    || $id_member9 == '58acfc97f4b21ebff852f886' || $id_member10 == '58acfc97f4b21ebff852f886'){
                        $dataMember = "<@U59L90G66>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //mrs. suhana
                    if($id_member1 == '58abdb1a88c1ba56168f5d9e' || $id_member2 == '58abdb1a88c1ba56168f5d9e' || $id_member3 == '58abdb1a88c1ba56168f5d9e' || $id_member4 == '58abdb1a88c1ba56168f5d9e'
                    || $id_member5 == '58abdb1a88c1ba56168f5d9e' || $id_member6 == '58abdb1a88c1ba56168f5d9e' || $id_member7 == '58abdb1a88c1ba56168f5d9e' || $id_member8 == '58abdb1a88c1ba56168f5d9e'
                    || $id_member9 == '58abdb1a88c1ba56168f5d9e' || $id_member10 == '58abdb1a88c1ba56168f5d9e'){
                        $dataMember = "<@U59MSGE12>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }         
                }
            }
        }
        $i++;
    }
}

//get data from Marketing list
function GetDataMarketingList(){
    $devListID  = '5c3308aa6fe4f68a28703bfa';
    $key        = 'cee15f8c5b0d25a32783f5139b64edb4';
    $token      = '0a9207e773917d4738910c973994011eea5bc955f93e59cdff9d1b7f1ab8776d';
    $APIURI     = 'https://api.trello.com/1/lists/'.$devListID.'/cards?cards=visible&card_member_fields=all&key='.$key.'&token='.$token;

    $init       = curl_init();
    curl_setopt($init, CURLOPT_URL, $APIURI); //Url together with parameters
    curl_setopt($init, CURLOPT_RETURNTRANSFER, 1); //Return data instead printing directly in Browser
    curl_setopt($init, CURLOPT_CONNECTTIMEOUT , 7); //Timeout after 7 seconds
    curl_setopt($init, CURLOPT_USERAGENT , "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
    curl_setopt($init, CURLOPT_HEADER, 0);

    $exec       = curl_exec($init);
    $close      = curl_close($init);

    $decodeJSON = json_decode($exec, true);
    $countJsonArray  = count($decodeJSON);
    
    $i = 0;
    while($i < $countJsonArray){
        // echo "data - ".$i.'<br>';
        foreach($decodeJSON[$i]['labels'] as $labelsData){
            //data  from labels array
            $labelName      = $labelsData['name'];
            $labelID        = $labelsData['id'];
            $labelColor     = $labelsData['color'];
            
            //data from card json return
            $cardName       = $decodeJSON[$i]['name'];
            $cardID         = $decodeJSON[$i]['id'];
            $cardURL        = $decodeJSON[$i]['url'];
            $memberID       = $decodeJSON[$i]['idMembers'];
            
            if((strpos($labelName, 'Day 7+') !== false)){
                //fetch memberID array
                if(count($memberID) == 1){
                    $memberIDs  = $memberID[0];

                    //arief
                    if(strpos($memberIDs, '5959c36a55c57339a774ff8c') !== false){
                        $dataMember = "<@U62TFQL1Y>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //azmi
                    if(strpos($memberIDs, '58b66821144d4b68a5ab1c84') !== false){
                        $dataMember = "<@U5A2WEHEH>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //alex
                    if(strpos($memberIDs, '5b70ef61add7a23d75e11a44') !== false){
                        $dataMember = "<@U9DV9JUET>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //leeJenLing
                    if(strpos($memberIDs, '5c3bf62e0581ed89ee7b6580') !== false){
                        $dataMember = "<@UFCJUGKDH>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //priya
                    if(strpos($memberIDs, '5b70f173191434079ebf6420') !== false){
                        $dataMember = "<@UCGKY4P2T>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //jeremy
                    if(strpos($memberIDs, '559cce487e609257b9b668e8') !== false){
                        $dataMember = "<@U59LLD0JE>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //rex
                    if(strpos($memberIDs, '5c3bf7a26814db37f6b41ff2') !== false){
                        $dataMember = "<@UFC03D588>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //leeYing
                    if(strpos($memberIDs, '590c33ec597ce7d26734f355') !== false){
                        $dataMember = "<@U591PGRJ4>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //shah
                    if(strpos($memberIDs, '58acfc97f4b21ebff852f886') !== false){
                        $dataMember = "<@U59L90G66>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //mrs. suhana
                    if(strpos($memberIDs, '58abdb1a88c1ba56168f5d9e') !== false){
                        $dataMember = "<@U59MSGE12>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    
                } else {
                    $id_member1 = $memberID[0];
                    $id_member2 = $memberID[1];
                    $id_member3 = $memberID[2];
                    $id_member4 = $memberID[3];
                    $id_member5 = $memberID[4];
                    $id_member6 = $memberID[5];
                    $id_member7 = $memberID[6];
                    $id_member8 = $memberID[7];
                    $id_member9 = $memberID[8];
                    $id_member10 = $memberID[9];

                    //arief
                    if($id_member1 == '5959c36a55c57339a774ff8c' || $id_member2 == '5959c36a55c57339a774ff8c' || $id_member3 == '5959c36a55c57339a774ff8c' || $id_member4 == '5959c36a55c57339a774ff8c'
                    || $id_member5 == '5959c36a55c57339a774ff8c' || $id_member6 == '5959c36a55c57339a774ff8c' || $id_member7 == '5959c36a55c57339a774ff8c' || $id_member8 == '5959c36a55c57339a774ff8c'
                    || $id_member9 == '5959c36a55c57339a774ff8c' || $id_member10 == '5959c36a55c57339a774ff8c'){
                        $dataMember = "<@U62TFQL1Y>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    } 
                    //azmi
                    if($id_member1 == '58b66821144d4b68a5ab1c84' || $id_member2 == '58b66821144d4b68a5ab1c84' || $id_member3 == '58b66821144d4b68a5ab1c84' || $id_member4 == '58b66821144d4b68a5ab1c84'
                    || $id_member5 == '58b66821144d4b68a5ab1c84' || $id_member6 == '58b66821144d4b68a5ab1c84' || $id_member7 == '58b66821144d4b68a5ab1c84' || $id_member8 == '58b66821144d4b68a5ab1c84'
                    || $id_member9 == '58b66821144d4b68a5ab1c84' || $id_member10 == '58b66821144d4b68a5ab1c84'){
                        $dataMember = "<@U5A2WEHEH>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //alex
                    if($id_member1 == '5b70ef61add7a23d75e11a44' || $id_member2 == '5b70ef61add7a23d75e11a44' || $id_member3 == '5b70ef61add7a23d75e11a44' || $id_member4 == '5b70ef61add7a23d75e11a44'
                    || $id_member5 == '5b70ef61add7a23d75e11a44' || $id_member6 == '5b70ef61add7a23d75e11a44' || $id_member7 == '5b70ef61add7a23d75e11a44' || $id_member8 == '5b70ef61add7a23d75e11a44'
                    || $id_member9 == '5b70ef61add7a23d75e11a44' || $id_member10 == '5b70ef61add7a23d75e11a44'){
                        $dataMember = "<@U9DV9JUET>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //lee jen ling
                    if($id_member1 == '5c3bf62e0581ed89ee7b6580' || $id_member2 == '5c3bf62e0581ed89ee7b6580' || $id_member3 == '5c3bf62e0581ed89ee7b6580' || $id_member4 == '5c3bf62e0581ed89ee7b6580'
                    || $id_member5 == '5c3bf62e0581ed89ee7b6580' || $id_member6 == '5c3bf62e0581ed89ee7b6580' || $id_member7 == '5c3bf62e0581ed89ee7b6580' || $id_member8 == '5c3bf62e0581ed89ee7b6580'
                    || $id_member9 == '5c3bf62e0581ed89ee7b6580' || $id_member10 == '5c3bf62e0581ed89ee7b6580'){
                        $dataMember = "<@UFCJUGKDH>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //priya
                    if($id_member1 == '5b70f173191434079ebf6420' || $id_member2 == '5b70f173191434079ebf6420' || $id_member3 == '5b70f173191434079ebf6420' || $id_member4 == '5b70f173191434079ebf6420'
                    || $id_member5 == '5b70f173191434079ebf6420' || $id_member6 == '5b70f173191434079ebf6420' || $id_member7 == '5b70f173191434079ebf6420' || $id_member8 == '5b70f173191434079ebf6420'
                    || $id_member9 == '5b70f173191434079ebf6420' || $id_member10 == '5b70f173191434079ebf6420'){
                        $dataMember = "<@UCGKY4P2T>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //jeremy
                    if($id_member1 == '559cce487e609257b9b668e8' || $id_member2 == '559cce487e609257b9b668e8' || $id_member3 == '559cce487e609257b9b668e8' || $id_member4 == '559cce487e609257b9b668e8'
                    || $id_member5 == '559cce487e609257b9b668e8' || $id_member6 == '559cce487e609257b9b668e8' || $id_member7 == '559cce487e609257b9b668e8' || $id_member8 == '559cce487e609257b9b668e8'
                    || $id_member9 == '559cce487e609257b9b668e8' || $id_member10 == '559cce487e609257b9b668e8'){
                        $dataMember = "<@U59LLD0JE>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //rex
                    if($id_member1 == '5c3bf7a26814db37f6b41ff2' || $id_member2 == '5c3bf7a26814db37f6b41ff2' || $id_member3 == '5c3bf7a26814db37f6b41ff2' || $id_member4 == '5c3bf7a26814db37f6b41ff2'
                    || $id_member5 == '5c3bf7a26814db37f6b41ff2' || $id_member6 == '5c3bf7a26814db37f6b41ff2' || $id_member7 == '5c3bf7a26814db37f6b41ff2' || $id_member8 == '5c3bf7a26814db37f6b41ff2'
                    || $id_member9 == '5c3bf7a26814db37f6b41ff2' || $id_member10 == '5c3bf7a26814db37f6b41ff2'){
                        $dataMember = "<@UFC03D588>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //lee ying
                    if($id_member1 == '590c33ec597ce7d26734f355' || $id_member2 == '590c33ec597ce7d26734f355' || $id_member3 == '590c33ec597ce7d26734f355' || $id_member4 == '590c33ec597ce7d26734f355'
                    || $id_member5 == '590c33ec597ce7d26734f355' || $id_member6 == '590c33ec597ce7d26734f355' || $id_member7 == '590c33ec597ce7d26734f355' || $id_member8 == '590c33ec597ce7d26734f355'
                    || $id_member9 == '590c33ec597ce7d26734f355' || $id_member10 == '590c33ec597ce7d26734f355'){
                        $dataMember = "<@U591PGRJ4>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //shah
                    if($id_member1 == '58acfc97f4b21ebff852f886' || $id_member2 == '58acfc97f4b21ebff852f886' || $id_member3 == '58acfc97f4b21ebff852f886' || $id_member4 == '58acfc97f4b21ebff852f886'
                    || $id_member5 == '58acfc97f4b21ebff852f886' || $id_member6 == '58acfc97f4b21ebff852f886' || $id_member7 == '58acfc97f4b21ebff852f886' || $id_member8 == '58acfc97f4b21ebff852f886'
                    || $id_member9 == '58acfc97f4b21ebff852f886' || $id_member10 == '58acfc97f4b21ebff852f886'){
                        $dataMember = "<@U59L90G66>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }
                    //mrs. suhana
                    if($id_member1 == '58abdb1a88c1ba56168f5d9e' || $id_member2 == '58abdb1a88c1ba56168f5d9e' || $id_member3 == '58abdb1a88c1ba56168f5d9e' || $id_member4 == '58abdb1a88c1ba56168f5d9e'
                    || $id_member5 == '58abdb1a88c1ba56168f5d9e' || $id_member6 == '58abdb1a88c1ba56168f5d9e' || $id_member7 == '58abdb1a88c1ba56168f5d9e' || $id_member8 == '58abdb1a88c1ba56168f5d9e'
                    || $id_member9 == '58abdb1a88c1ba56168f5d9e' || $id_member10 == '58abdb1a88c1ba56168f5d9e'){
                        $dataMember = "<@U59MSGE12>";
                        sendNotification($dataMember, $cardName, $cardURL);
                    }         
                }
            }
        }
        $i++;
    }
}

function sendNotification($dataMember, $cardName, $cardURL){
    //arief
    if($dataMember == "<@U62TFQL1Y>"){
        // echo "send to arief, card name is: ".$cardName.' and card url is: '.$cardURL.'<br>';
        $webhookURL_Arief   = 'https://hooks.slack.com/services/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';
        //data for notification
        $channel            = '@arief prasetyo';
        $bot_name           = 'Kaigan Taskmaster';
        $icon               = ':alien:';
        $attachments        = array([
            'color'     => '#36a64f',        
            'text'      => "*<@U62TFQL1Y>,you're spending too much time for the task:*\n*'$cardName'*\n_Complete it now or split it into multiple smaller tasks._\n$cardURL",
            'mrkdwn'    =>  true
        ]);

        $data = array(
            'channel'     => $channel,
            'username'    => $bot_name,
            'icon_emoji'  => $icon,
            'attachments' => $attachments
        );

        $data_string = json_encode($data);

        $ch = curl_init($webhookURL_Arief);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string))
        );
        //Execute CURL
        $result = curl_exec($ch);
        curl_close($ch);
    } 

    //azmi
    else if($dataMember == "<@U5A2WEHEH>"){
        // echo "send to azmi, card name is: ".$cardName.' and card url is: '.$cardURL.'<br>';
        $webhookURL_Azmi    = "https://hooks.slack.com/services/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";
        //data for notification
        $channel            = '@imzahahs';
        $bot_name           = 'Kaigan Taskmaster';
        $icon               = ':alien:';
        $attachments        = array([
            'color'     => '#36a64f',        
            'text'      => "*<@U5A2WEHEH>,you're spending too much time for the task:*\n*'$cardName'*\n_Complete it now or split it into multiple smaller tasks._\n$cardURL",
            'mrkdwn'    =>  true
        ]);

        $data = array(
            'channel'     => $channel,
            'username'    => $bot_name,
            'icon_emoji'  => $icon,
            'attachments' => $attachments
        );

        $data_string = json_encode($data);

        $ch = curl_init($webhookURL_Azmi);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string))
        );
        //Execute CURL
        $result = curl_exec($ch);
        curl_close($ch);
    }

    //alex
    else if($dataMember == "<@U9DV9JUET>"){
        // echo "send to alex, card name is: ".$cardName.' and card url is: '.$cardURL.'<br>';
        $webhookURL_Alex    = "https://hooks.slack.com/services/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";
        //data for notification
        $channel            = '@Alex Lee';
        $bot_name           = 'Kaigan Taskmaster';
        $icon               = ':alien:';
        $attachments        = array([
            'color'     => '#36a64f',        
            'text'      => "*<@U9DV9JUET>,you're spending too much time for the task:*\n*'$cardName'*\n_Complete it now or split it into multiple smaller tasks._\n$cardURL",
            'mrkdwn'    =>  true
        ]);

        $data = array(
            'channel'     => $channel,
            'username'    => $bot_name,
            'icon_emoji'  => $icon,
            'attachments' => $attachments
        );

        $data_string = json_encode($data);

        $ch = curl_init($webhookURL_Alex);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string))
        );
        //Execute CURL
        $result = curl_exec($ch);
        curl_close($ch);
    }

    //leejenling
    else if($dataMember == "<@UFCJUGKDH>"){
        // echo "send to lee jen ling, card name is: ".$cardName.' and card url is: '.$cardURL.'<br>';
        $webhookURL_LeeJL   = "https://hooks.slack.com/services/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";
        //data for notification
        $channel            = '@Lee Jen Ling';
        $bot_name           = 'Kaigan Taskmaster';
        $icon               = ':alien:';
        $attachments        = array([
            'color'     => '#36a64f',        
            'text'      => "*<@UFCJUGKDH>,you're spending too much time for the task:*\n*'$cardName'*\n_Complete it now or split it into multiple smaller tasks._\n$cardURL",
            'mrkdwn'    =>  true
        ]);

        $data = array(
            'channel'     => $channel,
            'username'    => $bot_name,
            'icon_emoji'  => $icon,
            'attachments' => $attachments
        );

        $data_string = json_encode($data);

        $ch = curl_init($webhookURL_LeeJL);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string))
        );
        //Execute CURL
        $result = curl_exec($ch);
        curl_close($ch);
    }

    //priya
    else if($dataMember == "<@UCGKY4P2T>"){
        // echo "send to priya, card name is: ".$cardName.' and card url is: '.$cardURL.'<br>';
        $webhookURL_Priya   = "https://hooks.slack.com/services/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";
        //data for notification
        $channel            = '@priya k';
        $bot_name           = 'Kaigan Taskmaster';
        $icon               = ':alien:';
        $attachments        = array([
            'color'     => '#36a64f',        
            'text'      => "*<@UCGKY4P2T>,you're spending too much time for the task:*\n*'$cardName'*\n_Complete it now or split it into multiple smaller tasks._\n$cardURL",
            'mrkdwn'    =>  true
        ]);

        $data = array(
            'channel'     => $channel,
            'username'    => $bot_name,
            'icon_emoji'  => $icon,
            'attachments' => $attachments
        );

        $data_string = json_encode($data);

        $ch = curl_init($webhookURL_Priya);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string))
        );
        //Execute CURL
        $result = curl_exec($ch);
        curl_close($ch);
    }

    //jeremy
    else if($dataMember == "<@U59LLD0JE>"){
        // echo "send to jeremy, card name is: ".$cardName.' and card url is: '.$cardURL.'<br>';
        $webhookURL_Jeremy   = "https://hooks.slack.com/services/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";
        //data for notification
        $channel            = '@jeremy';
        $bot_name           = 'Kaigan Taskmaster';
        $icon               = ':alien:';
        $attachments        = array([
            'color'     => '#36a64f',        
            'text'      => "*<@U59LLD0JE>,you're spending too much time for the task:*\n*'$cardName'*\n_Complete it now or split it into multiple smaller tasks._\n$cardURL",
            'mrkdwn'    =>  true
        ]);

        $data = array(
            'channel'     => $channel,
            'username'    => $bot_name,
            'icon_emoji'  => $icon,
            'attachments' => $attachments
        );

        $data_string = json_encode($data);

        $ch = curl_init($webhookURL_Jeremy);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string))
        );
        //Execute CURL
        $result = curl_exec($ch);
        curl_close($ch);
    }

    //rex
    else if($dataMember == "<@UFC03D588>"){
        // echo "send to rex, card name is: ".$cardName.' and card url is: '.$cardURL.'<br>';
        $webhookURL_Rex     = "https://hooks.slack.com/services/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";
        //data for notification
        $channel            = '@Rex Naythian';
        $bot_name           = 'Kaigan Taskmaster';
        $icon               = ':alien:';
        $attachments        = array([
            'color'     => '#36a64f',        
            'text'      => "*<@UFC03D588>,you're spending too much time for the task:*\n*'$cardName'*\n_Complete it now or split it into multiple smaller tasks._\n$cardURL",
            'mrkdwn'    =>  true
        ]);

        $data = array(
            'channel'     => $channel,
            'username'    => $bot_name,
            'icon_emoji'  => $icon,
            'attachments' => $attachments
        );

        $data_string = json_encode($data);

        $ch = curl_init($webhookURL_Rex);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string))
        );
        //Execute CURL
        $result = curl_exec($ch);
        curl_close($ch);
    }

    //leeYing
    else if($dataMember == "<@U591PGRJ4>"){
        // echo "send to lee ying, card name is: ".$cardName.' and card url is: '.$cardURL.'<br>';
        $webhookURL_LeeYing   = "https://hooks.slack.com/services/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";
        //data for notification
        $channel            = '@leeying';
        $bot_name           = 'Kaigan Taskmaster';
        $icon               = ':alien:';
        $attachments        = array([
            'color'     => '#36a64f',        
            'text'      => "*<@U591PGRJ4>,you're spending too much time for the task:*\n*'$cardName'*\n_Complete it now or split it into multiple smaller tasks._\n$cardURL",
            'mrkdwn'    =>  true
        ]);

        $data = array(
            'channel'     => $channel,
            'username'    => $bot_name,
            'icon_emoji'  => $icon,
            'attachments' => $attachments
        );

        $data_string = json_encode($data);

        $ch = curl_init($webhookURL_LeeYing);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string))
        );
        //Execute CURL
        $result = curl_exec($ch);
        curl_close($ch);
    }

    //shah
    else if($dataMember == "<@U59L90G66>"){
        // echo "send to shah, card name is: ".$cardName.' and card url is: '.$cardURL.'<br>';
        $webhookURL_Shah    = "https://hooks.slack.com/services/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";
        //data for notification
        $channel            = '@shah';
        $bot_name           = 'Kaigan Taskmaster';
        $icon               = ':alien:';
        $attachments        = array([
            'color'     => '#36a64f',        
            'text'      => "*<@U59L90G66>,you're spending too much time for the task:*\n*'$cardName'*\n_Complete it now or split it into multiple smaller tasks._\n$cardURL",
            'mrkdwn'    =>  true
        ]);

        $data = array(
            'channel'     => $channel,
            'username'    => $bot_name,
            'icon_emoji'  => $icon,
            'attachments' => $attachments
        );

        $data_string = json_encode($data);

        $ch = curl_init($webhookURL_Shah);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string))
        );
        //Execute CURL
        $result = curl_exec($ch);
        curl_close($ch);
    }

    //mrs. Suhana
    else if($dataMember == "<@U59MSGE12>"){
        // echo "send to mrs. suhana, card name is: ".$cardName.' and card url is: '.$cardURL.'<br>';
        $webhookURL_Suhana   = "https://hooks.slack.com/services/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";
        //data for notification
        $channel            = '@suhana';
        $bot_name           = 'Kaigan Taskmaster';
        $icon               = ':alien:';
        $attachments        = array([
            'color'     => '#36a64f',        
            'text'      => "*<@U59MSGE12>,you're spending too much time for the task:*\n*'$cardName'*\n_Complete it now or split it into multiple smaller tasks._\n$cardURL",
            'mrkdwn'    =>  true
        ]);

        $data = array(
            'channel'     => $channel,
            'username'    => $bot_name,
            'icon_emoji'  => $icon,
            'attachments' => $attachments
        );

        $data_string = json_encode($data);

        $ch = curl_init($webhookURL_Suhana);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string))
        );
        //Execute CURL
        $result = curl_exec($ch);
        curl_close($ch);
    }
}

//main function
function callTheFunctionNow(){
    GetDataDevelopmentList();
    GetDataGameDesignList();
    GetDataWritingList();
    GetDataArtList();
    GetDataSoundList();
    GetDataOperationList();
    GetDataMarketingList();
}

//call main function
callTheFunctionNow();
?>