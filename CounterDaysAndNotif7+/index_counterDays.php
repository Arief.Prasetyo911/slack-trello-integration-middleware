<?php
error_reporting(0);
//get data from Development list
function GetDataDevelopmentList(){
    $devListID  = '5c3308c14360d00d5e72d78d';
    $key        = 'cee15f8c5b0d25a32783f5139b64edb4';
    $token      = '0a9207e773917d4738910c973994011eea5bc955f93e59cdff9d1b7f1ab8776d';
    $APIURI     = 'https://api.trello.com/1/lists/'.$devListID.'/cards?cards=visible&card_member_fields=all&key='.$key.'&token='.$token;

    $init       = curl_init();
    curl_setopt($init, CURLOPT_URL, $APIURI); //Url together with parameters
    curl_setopt($init, CURLOPT_RETURNTRANSFER, 1); //Return data instead printing directly in Browser
    curl_setopt($init, CURLOPT_CONNECTTIMEOUT , 7); //Timeout after 7 seconds
    curl_setopt($init, CURLOPT_USERAGENT , "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
    curl_setopt($init, CURLOPT_HEADER, 0);

    $exec       = curl_exec($init);
    $close      = curl_close($init);

    $decodeJSON = json_decode($exec, true);
    $countJsonArray  = count($decodeJSON);
    
    $i = 0;
    while($i < $countJsonArray){
        // echo "data - ".$i.'<br>';
        foreach($decodeJSON[$i]['labels'] as $labelsData){
            //data  from labels array
            $labelName      = $labelsData['name'];
            $labelID        = $labelsData['id'];
            $labelColor     = $labelsData['color'];
            
            //data from card json return
            $cardName       = $decodeJSON[$i]['name'];
            $cardID         = $decodeJSON[$i]['id'];
            $cardURL        = $decodeJSON[$i]['url'];
            $memberID       = $decodeJSON[$i]['idMembers'];

            //fetch memberID array
            if(count($memberID) == 1){
                $memberIDs  = $memberID[0];
                
            } else {
                $memberIDs1 = $memberID[0];
                $memberIDs2 = $memberID[1];
                $memberIDs3 = $memberID[2];
                $memberIDs4 = $memberID[3];
                $memberIDs5 = $memberID[4];
                $memberIDs6 = $memberID[5];
                $memberIDs7 = $memberID[6];
                $memberIDs8 = $memberID[7];
                $memberIDs9 = $memberID[8];
                $memberIDs10 = $memberID[9];

                $temp       = [$memberIDs1, $memberIDs2, $memberIDs3, $memberIDs4, $memberIDs5, $memberIDs6, $memberIDs7, $memberIDs8, $memberIDs9, $memberIDs10];
                $memberIDs  = implode(', ', $temp);
            }
            
            if(strpos($labelName, 'Day 7+') !== false){
                //date setting
                date_default_timezone_set('Asia/Kuala_Lumpur');
                $createdAt      = date('Y-m-d H:i:s');
                $lastActivityRaw= date('Y-m-d');
                $date_now       = new DateTime();
                
                //connect to database
                global $mysqli;
                $mysqli = mysqli_connect('localhost', 'saraismi_arief', 'kaigan2019', 'saraismi_trelloSlackDayCounter');
                            
                //select data from db
                $sql_select             = "SELECT * FROM notification_7plus WHERE id_card='$cardID'";
                $query_select           = mysqli_query($mysqli, $sql_select);
                //if data found
                if(mysqli_num_rows($query_select) > 0){
                    $sqlUpdate          = "UPDATE notification_7plus SET id_member='$memberIDs', card_name='$cardName', card_URL='$cardURL', created_at='$createdAt' WHERE id_card='$cardID'";
                    $query              = mysqli_query($mysqli, $sqlUpdate);
                } else {
                    $sql                = "INSERT INTO notification_7plus (id_card, id_member, card_name, card_URL, created_at) 
                                            VALUES ('$cardID', '$memberIDs', '$cardName', '$cardURL', '$createdAt')";
                    $query              = mysqli_query($mysqli, $sql);
                }

                //select created_at from db
                $getData            = "SELECT * FROM notification_7plus WHERE id_card='$cardID'";
                $query_exec         = mysqli_query($mysqli, $getData);
                //if data found
                if(mysqli_num_rows($query_exec) > 0){
                    foreach($query_exec as $results){
                        $manyMemberID   = $results['id_member'];

                        //check per IDs
                        //arief
                        if(strpos($manyMemberID, '5959c36a55c57339a774ff8c') !== false){
                            $dataMember = "<@U62TFQL1Y>";
                            temporaryData($dataMember, $cardName);
                        }
                        //azmi
                        if(strpos($manyMemberID, '58b66821144d4b68a5ab1c84') !== false){
                            $dataMember = "<@U5A2WEHEH>";
                            temporaryData($dataMember, $cardName);
                        }
                        //alex
                        if(strpos($manyMemberID, '5b70ef61add7a23d75e11a44') !== false){
                            $dataMember = "<@U9DV9JUET>";
                            temporaryData($dataMember, $cardName);
                        }
                        //leeJenLing
                        if(strpos($manyMemberID, '5c3bf62e0581ed89ee7b6580') !== false){
                            $dataMember = "<@UFCJUGKDH>";
                            temporaryData($dataMember, $cardName);
                        }
                        //priya
                        if(strpos($manyMemberID, '5b70f173191434079ebf6420') !== false){
                            $dataMember = "<@UCGKY4P2T>";
                            temporaryData($dataMember, $cardName);
                        }
                        //jeremy
                        if(strpos($manyMemberID, '559cce487e609257b9b668e8') !== false){
                            $dataMember = "<@U59LLD0JE>";
                            temporaryData($dataMember, $cardName);
                        }
                        //rex
                        if(strpos($manyMemberID, '5c3bf7a26814db37f6b41ff2') !== false){
                            $dataMember = "<@UFC03D588>";
                            temporaryData($dataMember, $cardName);
                        }
                        //leeYing
                        if(strpos($manyMemberID, '590c33ec597ce7d26734f355') !== false){
                            $dataMember = "<@U591PGRJ4>";
                            temporaryData($dataMember, $cardName);
                        }
                        //shah
                        if(strpos($manyMemberID, '58acfc97f4b21ebff852f886') !== false){
                            $dataMember = "<@U59L90G66>";
                            temporaryData($dataMember, $cardName);
                        }
                        //mrs. suhana
                        if(strpos($manyMemberID, '58abdb1a88c1ba56168f5d9e') !== false){
                            $dataMember = "<@U59MSGE12>";
                            temporaryData($dataMember, $cardName);
                        }
                    }
                } else {
                    echo "data not found <br>";
                }
            }
        }
        $i++;
    }
}

//get data from GameDesign list
function GetDataGameDesignList(){
    $devListID  = '5bceba7f62850a31fb5160dc';
    $key        = 'cee15f8c5b0d25a32783f5139b64edb4';
    $token      = '0a9207e773917d4738910c973994011eea5bc955f93e59cdff9d1b7f1ab8776d';
    $APIURI     = 'https://api.trello.com/1/lists/'.$devListID.'/cards?cards=visible&card_member_fields=all&key='.$key.'&token='.$token;

    $init       = curl_init();
    curl_setopt($init, CURLOPT_URL, $APIURI); //Url together with parameters
    curl_setopt($init, CURLOPT_RETURNTRANSFER, 1); //Return data instead printing directly in Browser
    curl_setopt($init, CURLOPT_CONNECTTIMEOUT , 7); //Timeout after 7 seconds
    curl_setopt($init, CURLOPT_USERAGENT , "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
    curl_setopt($init, CURLOPT_HEADER, 0);

    $exec       = curl_exec($init);
    $close      = curl_close($init);

    $decodeJSON = json_decode($exec, true);
    $countJsonArray  = count($decodeJSON);
    
    $i = 0;
    while($i < $countJsonArray){
        // echo "data - ".$i.'<br>';
        foreach($decodeJSON[$i]['labels'] as $labelsData){
            //data  from labels array
            $labelName      = $labelsData['name'];
            $labelID        = $labelsData['id'];
            $labelColor     = $labelsData['color'];
            
            //data from card json return
            $cardName       = $decodeJSON[$i]['name'];
            $cardID         = $decodeJSON[$i]['id'];
            $cardURL        = $decodeJSON[$i]['url'];
            $memberID       = $decodeJSON[$i]['idMembers'];

            //fetch memberID array
            if(count($memberID) == 1){
                $memberIDs  = $memberID[0];
                
            } else {
                $memberIDs1 = $memberID[0];
                $memberIDs2 = $memberID[1];
                $memberIDs3 = $memberID[2];
                $memberIDs4 = $memberID[3];
                $memberIDs5 = $memberID[4];
                $memberIDs6 = $memberID[5];
                $memberIDs7 = $memberID[6];
                $memberIDs8 = $memberID[7];
                $memberIDs9 = $memberID[8];
                $memberIDs10 = $memberID[9];

                $temp       = [$memberIDs1, $memberIDs2, $memberIDs3, $memberIDs4, $memberIDs5, $memberIDs6, $memberIDs7, $memberIDs8, $memberIDs9, $memberIDs10];
                $memberIDs  = implode(', ', $temp);
            }
            
            if(strpos($labelName, 'Day 7+') !== false){
                //date setting
                date_default_timezone_set('Asia/Kuala_Lumpur');
                $createdAt      = date('Y-m-d H:i:s');
                $lastActivityRaw= date('Y-m-d');
                $date_now       = new DateTime();
                
                //connect to database
                global $mysqli;
                $mysqli = mysqli_connect('localhost', 'saraismi_arief', 'kaigan2019', 'saraismi_trelloSlackDayCounter');
                            
                //select data from db
                $sql_select             = "SELECT * FROM notification_7plus WHERE id_card='$cardID'";
                $query_select           = mysqli_query($mysqli, $sql_select);
                //if data found
                if(mysqli_num_rows($query_select) > 0){
                    $sqlUpdate          = "UPDATE notification_7plus SET id_member='$memberIDs', card_name='$cardName', card_URL='$cardURL', created_at='$createdAt' WHERE id_card='$cardID'";
                    $query              = mysqli_query($mysqli, $sqlUpdate);
                } else {
                    $sql                = "INSERT INTO notification_7plus (id_card, id_member, card_name, card_URL, created_at) 
                                            VALUES ('$cardID', '$memberIDs', '$cardName', '$cardURL', '$createdAt')";
                    $query              = mysqli_query($mysqli, $sql);
                }

                //select created_at from db
                $getData            = "SELECT * FROM notification_7plus WHERE id_card='$cardID'";
                $query_exec         = mysqli_query($mysqli, $getData);
                //if data found
                if(mysqli_num_rows($query_exec) > 0){
                    foreach($query_exec as $results){
                        $manyMemberID   = $results['id_member'];

                        //check per IDs
                        //arief
                        if(strpos($manyMemberID, '5959c36a55c57339a774ff8c') !== false){
                            $dataMember = "<@U62TFQL1Y>";
                            temporaryData($dataMember, $cardName);
                        }
                        //azmi
                        if(strpos($manyMemberID, '58b66821144d4b68a5ab1c84') !== false){
                            $dataMember = "<@U5A2WEHEH>";
                            temporaryData($dataMember, $cardName);
                        }
                        //alex
                        if(strpos($manyMemberID, '5b70ef61add7a23d75e11a44') !== false){
                            $dataMember = "<@U9DV9JUET>";
                            temporaryData($dataMember, $cardName);
                        }
                        //leeJenLing
                        if(strpos($manyMemberID, '5c3bf62e0581ed89ee7b6580') !== false){
                            $dataMember = "<@UFCJUGKDH>";
                            temporaryData($dataMember, $cardName);
                        }
                        //priya
                        if(strpos($manyMemberID, '5b70f173191434079ebf6420') !== false){
                            $dataMember = "<@UCGKY4P2T>";
                            temporaryData($dataMember, $cardName);
                        }
                        //jeremy
                        if(strpos($manyMemberID, '559cce487e609257b9b668e8') !== false){
                            $dataMember = "<@U59LLD0JE>";
                            temporaryData($dataMember, $cardName);
                        }
                        //rex
                        if(strpos($manyMemberID, '5c3bf7a26814db37f6b41ff2') !== false){
                            $dataMember = "<@UFC03D588>";
                            temporaryData($dataMember, $cardName);
                        }
                        //leeYing
                        if(strpos($manyMemberID, '590c33ec597ce7d26734f355') !== false){
                            $dataMember = "<@U591PGRJ4>";
                            temporaryData($dataMember, $cardName);
                        }
                        //shah
                        if(strpos($manyMemberID, '58acfc97f4b21ebff852f886') !== false){
                            $dataMember = "<@U59L90G66>";
                            temporaryData($dataMember, $cardName);
                        }
                        //mrs. suhana
                        if(strpos($manyMemberID, '58abdb1a88c1ba56168f5d9e') !== false){
                            $dataMember = "<@U59MSGE12>";
                            temporaryData($dataMember, $cardName);
                        }
                    }
                } else {
                    echo "data not found <br>";
                }
            }
        }
        $i++;
    }
}

//get data from Writing list
function GetDataWritingList(){
    $devListID  = '5bceba7f62850a31fb5160de';
    $key        = 'cee15f8c5b0d25a32783f5139b64edb4';
    $token      = '0a9207e773917d4738910c973994011eea5bc955f93e59cdff9d1b7f1ab8776d';
    $APIURI     = 'https://api.trello.com/1/lists/'.$devListID.'/cards?cards=visible&card_member_fields=all&key='.$key.'&token='.$token;

    $init       = curl_init();
    curl_setopt($init, CURLOPT_URL, $APIURI); //Url together with parameters
    curl_setopt($init, CURLOPT_RETURNTRANSFER, 1); //Return data instead printing directly in Browser
    curl_setopt($init, CURLOPT_CONNECTTIMEOUT , 7); //Timeout after 7 seconds
    curl_setopt($init, CURLOPT_USERAGENT , "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
    curl_setopt($init, CURLOPT_HEADER, 0);

    $exec       = curl_exec($init);
    $close      = curl_close($init);

    $decodeJSON = json_decode($exec, true);
    $countJsonArray  = count($decodeJSON);
    
    $i = 0;
    while($i < $countJsonArray){
        // echo "data - ".$i.'<br>';
        foreach($decodeJSON[$i]['labels'] as $labelsData){
            //data  from labels array
            $labelName      = $labelsData['name'];
            $labelID        = $labelsData['id'];
            $labelColor     = $labelsData['color'];
            
            //data from card json return
            $cardName       = $decodeJSON[$i]['name'];
            $cardID         = $decodeJSON[$i]['id'];
            $cardURL        = $decodeJSON[$i]['url'];
            $memberID       = $decodeJSON[$i]['idMembers'];

            //fetch memberID array
            if(count($memberID) == 1){
                $memberIDs  = $memberID[0];
                
            } else {
                $memberIDs1 = $memberID[0];
                $memberIDs2 = $memberID[1];
                $memberIDs3 = $memberID[2];
                $memberIDs4 = $memberID[3];
                $memberIDs5 = $memberID[4];
                $memberIDs6 = $memberID[5];
                $memberIDs7 = $memberID[6];
                $memberIDs8 = $memberID[7];
                $memberIDs9 = $memberID[8];
                $memberIDs10 = $memberID[9];

                $temp       = [$memberIDs1, $memberIDs2, $memberIDs3, $memberIDs4, $memberIDs5, $memberIDs6, $memberIDs7, $memberIDs8, $memberIDs9, $memberIDs10];
                $memberIDs  = implode(', ', $temp);
            }
            
            if(strpos($labelName, 'Day 7+') !== false){
                //date setting
                date_default_timezone_set('Asia/Kuala_Lumpur');
                $createdAt      = date('Y-m-d H:i:s');
                $lastActivityRaw= date('Y-m-d');
                $date_now       = new DateTime();
                
                //connect to database
                global $mysqli;
                $mysqli = mysqli_connect('localhost', 'saraismi_arief', 'kaigan2019', 'saraismi_trelloSlackDayCounter');
                            
                //select data from db
                $sql_select             = "SELECT * FROM notification_7plus WHERE id_card='$cardID'";
                $query_select           = mysqli_query($mysqli, $sql_select);
                //if data found
                if(mysqli_num_rows($query_select) > 0){
                    $sqlUpdate          = "UPDATE notification_7plus SET id_member='$memberIDs', card_name='$cardName', card_URL='$cardURL', created_at='$createdAt' WHERE id_card='$cardID'";
                    $query              = mysqli_query($mysqli, $sqlUpdate);
                } else {
                    $sql                = "INSERT INTO notification_7plus (id_card, id_member, card_name, card_URL, created_at) 
                                            VALUES ('$cardID', '$memberIDs', '$cardName', '$cardURL', '$createdAt')";
                    $query              = mysqli_query($mysqli, $sql);
                }

                //select created_at from db
                $getData            = "SELECT * FROM notification_7plus WHERE id_card='$cardID'";
                $query_exec         = mysqli_query($mysqli, $getData);
                //if data found
                if(mysqli_num_rows($query_exec) > 0){
                    foreach($query_exec as $results){
                        $manyMemberID   = $results['id_member'];

                        //check per IDs
                        //arief
                        if(strpos($manyMemberID, '5959c36a55c57339a774ff8c') !== false){
                            $dataMember = "<@U62TFQL1Y>";
                            temporaryData($dataMember, $cardName);
                        }
                        //azmi
                        if(strpos($manyMemberID, '58b66821144d4b68a5ab1c84') !== false){
                            $dataMember = "<@U5A2WEHEH>";
                            temporaryData($dataMember, $cardName);
                        }
                        //alex
                        if(strpos($manyMemberID, '5b70ef61add7a23d75e11a44') !== false){
                            $dataMember = "<@U9DV9JUET>";
                            temporaryData($dataMember, $cardName);
                        }
                        //leeJenLing
                        if(strpos($manyMemberID, '5c3bf62e0581ed89ee7b6580') !== false){
                            $dataMember = "<@UFCJUGKDH>";
                            temporaryData($dataMember, $cardName);
                        }
                        //priya
                        if(strpos($manyMemberID, '5b70f173191434079ebf6420') !== false){
                            $dataMember = "<@UCGKY4P2T>";
                            temporaryData($dataMember, $cardName);
                        }
                        //jeremy
                        if(strpos($manyMemberID, '559cce487e609257b9b668e8') !== false){
                            $dataMember = "<@U59LLD0JE>";
                            temporaryData($dataMember, $cardName);
                        }
                        //rex
                        if(strpos($manyMemberID, '5c3bf7a26814db37f6b41ff2') !== false){
                            $dataMember = "<@UFC03D588>";
                            temporaryData($dataMember, $cardName);
                        }
                        //leeYing
                        if(strpos($manyMemberID, '590c33ec597ce7d26734f355') !== false){
                            $dataMember = "<@U591PGRJ4>";
                            temporaryData($dataMember, $cardName);
                        }
                        //shah
                        if(strpos($manyMemberID, '58acfc97f4b21ebff852f886') !== false){
                            $dataMember = "<@U59L90G66>";
                            temporaryData($dataMember, $cardName);
                        }
                        //mrs. suhana
                        if(strpos($manyMemberID, '58abdb1a88c1ba56168f5d9e') !== false){
                            $dataMember = "<@U59MSGE12>";
                            temporaryData($dataMember, $cardName);
                        }
                    }
                } else {
                    echo "data not found <br>";
                }
            }
        }
        $i++;
    }
}

//get data from Art list
function GetDataArtList(){
    $devListID  = '5bceba7f62850a31fb5160dd';
    $key        = 'cee15f8c5b0d25a32783f5139b64edb4';
    $token      = '0a9207e773917d4738910c973994011eea5bc955f93e59cdff9d1b7f1ab8776d';
    $APIURI     = 'https://api.trello.com/1/lists/'.$devListID.'/cards?cards=visible&card_member_fields=all&key='.$key.'&token='.$token;

    $init       = curl_init();
    curl_setopt($init, CURLOPT_URL, $APIURI); //Url together with parameters
    curl_setopt($init, CURLOPT_RETURNTRANSFER, 1); //Return data instead printing directly in Browser
    curl_setopt($init, CURLOPT_CONNECTTIMEOUT , 7); //Timeout after 7 seconds
    curl_setopt($init, CURLOPT_USERAGENT , "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
    curl_setopt($init, CURLOPT_HEADER, 0);

    $exec       = curl_exec($init);
    $close      = curl_close($init);

    $decodeJSON = json_decode($exec, true);
    $countJsonArray  = count($decodeJSON);
    
    $i = 0;
    while($i < $countJsonArray){
        // echo "data - ".$i.'<br>';
        foreach($decodeJSON[$i]['labels'] as $labelsData){
            //data  from labels array
            $labelName      = $labelsData['name'];
            $labelID        = $labelsData['id'];
            $labelColor     = $labelsData['color'];
            
            //data from card json return
            $cardName       = $decodeJSON[$i]['name'];
            $cardID         = $decodeJSON[$i]['id'];
            $cardURL        = $decodeJSON[$i]['url'];
            $memberID       = $decodeJSON[$i]['idMembers'];

            //fetch memberID array
            if(count($memberID) == 1){
                $memberIDs  = $memberID[0];
                
            } else {
                $memberIDs1 = $memberID[0];
                $memberIDs2 = $memberID[1];
                $memberIDs3 = $memberID[2];
                $memberIDs4 = $memberID[3];
                $memberIDs5 = $memberID[4];
                $memberIDs6 = $memberID[5];
                $memberIDs7 = $memberID[6];
                $memberIDs8 = $memberID[7];
                $memberIDs9 = $memberID[8];
                $memberIDs10 = $memberID[9];

                $temp       = [$memberIDs1, $memberIDs2, $memberIDs3, $memberIDs4, $memberIDs5, $memberIDs6, $memberIDs7, $memberIDs8, $memberIDs9, $memberIDs10];
                $memberIDs  = implode(', ', $temp);
            }
            
            if(strpos($labelName, 'Day 7+') !== false){
                //date setting
                date_default_timezone_set('Asia/Kuala_Lumpur');
                $createdAt      = date('Y-m-d H:i:s');
                $lastActivityRaw= date('Y-m-d');
                $date_now       = new DateTime();
                
                //connect to database
                global $mysqli;
                $mysqli = mysqli_connect('localhost', 'saraismi_arief', 'kaigan2019', 'saraismi_trelloSlackDayCounter');
                            
                //select data from db
                $sql_select             = "SELECT * FROM notification_7plus WHERE id_card='$cardID'";
                $query_select           = mysqli_query($mysqli, $sql_select);
                //if data found
                if(mysqli_num_rows($query_select) > 0){
                    $sqlUpdate          = "UPDATE notification_7plus SET id_member='$memberIDs', card_name='$cardName', card_URL='$cardURL', created_at='$createdAt' WHERE id_card='$cardID'";
                    $query              = mysqli_query($mysqli, $sqlUpdate);
                } else {
                    $sql                = "INSERT INTO notification_7plus (id_card, id_member, card_name, card_URL, created_at) 
                                            VALUES ('$cardID', '$memberIDs', '$cardName', '$cardURL', '$createdAt')";
                    $query              = mysqli_query($mysqli, $sql);
                }

                //select created_at from db
                $getData            = "SELECT * FROM notification_7plus WHERE id_card='$cardID'";
                $query_exec         = mysqli_query($mysqli, $getData);
                //if data found
                if(mysqli_num_rows($query_exec) > 0){
                    foreach($query_exec as $results){
                        $manyMemberID   = $results['id_member'];

                        //check per IDs
                        //arief
                        if(strpos($manyMemberID, '5959c36a55c57339a774ff8c') !== false){
                            $dataMember = "<@U62TFQL1Y>";
                            temporaryData($dataMember, $cardName);
                        }
                        //azmi
                        if(strpos($manyMemberID, '58b66821144d4b68a5ab1c84') !== false){
                            $dataMember = "<@U5A2WEHEH>";
                            temporaryData($dataMember, $cardName);
                        }
                        //alex
                        if(strpos($manyMemberID, '5b70ef61add7a23d75e11a44') !== false){
                            $dataMember = "<@U9DV9JUET>";
                            temporaryData($dataMember, $cardName);
                        }
                        //leeJenLing
                        if(strpos($manyMemberID, '5c3bf62e0581ed89ee7b6580') !== false){
                            $dataMember = "<@UFCJUGKDH>";
                            temporaryData($dataMember, $cardName);
                        }
                        //priya
                        if(strpos($manyMemberID, '5b70f173191434079ebf6420') !== false){
                            $dataMember = "<@UCGKY4P2T>";
                            temporaryData($dataMember, $cardName);
                        }
                        //jeremy
                        if(strpos($manyMemberID, '559cce487e609257b9b668e8') !== false){
                            $dataMember = "<@U59LLD0JE>";
                            temporaryData($dataMember, $cardName);
                        }
                        //rex
                        if(strpos($manyMemberID, '5c3bf7a26814db37f6b41ff2') !== false){
                            $dataMember = "<@UFC03D588>";
                            temporaryData($dataMember, $cardName);
                        }
                        //leeYing
                        if(strpos($manyMemberID, '590c33ec597ce7d26734f355') !== false){
                            $dataMember = "<@U591PGRJ4>";
                            temporaryData($dataMember, $cardName);
                        }
                        //shah
                        if(strpos($manyMemberID, '58acfc97f4b21ebff852f886') !== false){
                            $dataMember = "<@U59L90G66>";
                            temporaryData($dataMember, $cardName);
                        }
                        //mrs. suhana
                        if(strpos($manyMemberID, '58abdb1a88c1ba56168f5d9e') !== false){
                            $dataMember = "<@U59MSGE12>";
                            temporaryData($dataMember, $cardName);
                        }
                    }
                } else {
                    echo "data not found <br>";
                }
            }
        }
        $i++;
    }
}

//get data from Sound list
function GetDataSoundList(){
    $devListID  = '5c33089944403668623df872';
    $key        = 'cee15f8c5b0d25a32783f5139b64edb4';
    $token      = '0a9207e773917d4738910c973994011eea5bc955f93e59cdff9d1b7f1ab8776d';
    $APIURI     = 'https://api.trello.com/1/lists/'.$devListID.'/cards?cards=visible&card_member_fields=all&key='.$key.'&token='.$token;

    $init       = curl_init();
    curl_setopt($init, CURLOPT_URL, $APIURI); //Url together with parameters
    curl_setopt($init, CURLOPT_RETURNTRANSFER, 1); //Return data instead printing directly in Browser
    curl_setopt($init, CURLOPT_CONNECTTIMEOUT , 7); //Timeout after 7 seconds
    curl_setopt($init, CURLOPT_USERAGENT , "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
    curl_setopt($init, CURLOPT_HEADER, 0);

    $exec       = curl_exec($init);
    $close      = curl_close($init);

    $decodeJSON = json_decode($exec, true);
    $countJsonArray  = count($decodeJSON);
    
    $i = 0;
    while($i < $countJsonArray){
        // echo "data - ".$i.'<br>';
        foreach($decodeJSON[$i]['labels'] as $labelsData){
            //data  from labels array
            $labelName      = $labelsData['name'];
            $labelID        = $labelsData['id'];
            $labelColor     = $labelsData['color'];
            
            //data from card json return
            $cardName       = $decodeJSON[$i]['name'];
            $cardID         = $decodeJSON[$i]['id'];
            $cardURL        = $decodeJSON[$i]['url'];
            $memberID       = $decodeJSON[$i]['idMembers'];

            //fetch memberID array
            if(count($memberID) == 1){
                $memberIDs  = $memberID[0];
                
            } else {
                $memberIDs1 = $memberID[0];
                $memberIDs2 = $memberID[1];
                $memberIDs3 = $memberID[2];
                $memberIDs4 = $memberID[3];
                $memberIDs5 = $memberID[4];
                $memberIDs6 = $memberID[5];
                $memberIDs7 = $memberID[6];
                $memberIDs8 = $memberID[7];
                $memberIDs9 = $memberID[8];
                $memberIDs10 = $memberID[9];

                $temp       = [$memberIDs1, $memberIDs2, $memberIDs3, $memberIDs4, $memberIDs5, $memberIDs6, $memberIDs7, $memberIDs8, $memberIDs9, $memberIDs10];
                $memberIDs  = implode(', ', $temp);
            }
            
            if(strpos($labelName, 'Day 7+') !== false){
                //date setting
                date_default_timezone_set('Asia/Kuala_Lumpur');
                $createdAt      = date('Y-m-d H:i:s');
                $lastActivityRaw= date('Y-m-d');
                $date_now       = new DateTime();
                
                //connect to database
                global $mysqli;
                $mysqli = mysqli_connect('localhost', 'saraismi_arief', 'kaigan2019', 'saraismi_trelloSlackDayCounter');
                            
                //select data from db
                $sql_select             = "SELECT * FROM notification_7plus WHERE id_card='$cardID'";
                $query_select           = mysqli_query($mysqli, $sql_select);
                //if data found
                if(mysqli_num_rows($query_select) > 0){
                    $sqlUpdate          = "UPDATE notification_7plus SET id_member='$memberIDs', card_name='$cardName', card_URL='$cardURL', created_at='$createdAt' WHERE id_card='$cardID'";
                    $query              = mysqli_query($mysqli, $sqlUpdate);
                } else {
                    $sql                = "INSERT INTO notification_7plus (id_card, id_member, card_name, card_URL, created_at) 
                                            VALUES ('$cardID', '$memberIDs', '$cardName', '$cardURL', '$createdAt')";
                    $query              = mysqli_query($mysqli, $sql);
                }

                //select created_at from db
                $getData            = "SELECT * FROM notification_7plus WHERE id_card='$cardID'";
                $query_exec         = mysqli_query($mysqli, $getData);
                //if data found
                if(mysqli_num_rows($query_exec) > 0){
                    foreach($query_exec as $results){
                        $manyMemberID   = $results['id_member'];

                        //check per IDs
                        //arief
                        if(strpos($manyMemberID, '5959c36a55c57339a774ff8c') !== false){
                            $dataMember = "<@U62TFQL1Y>";
                            temporaryData($dataMember, $cardName);
                        }
                        //azmi
                        if(strpos($manyMemberID, '58b66821144d4b68a5ab1c84') !== false){
                            $dataMember = "<@U5A2WEHEH>";
                            temporaryData($dataMember, $cardName);
                        }
                        //alex
                        if(strpos($manyMemberID, '5b70ef61add7a23d75e11a44') !== false){
                            $dataMember = "<@U9DV9JUET>";
                            temporaryData($dataMember, $cardName);
                        }
                        //leeJenLing
                        if(strpos($manyMemberID, '5c3bf62e0581ed89ee7b6580') !== false){
                            $dataMember = "<@UFCJUGKDH>";
                            temporaryData($dataMember, $cardName);
                        }
                        //priya
                        if(strpos($manyMemberID, '5b70f173191434079ebf6420') !== false){
                            $dataMember = "<@UCGKY4P2T>";
                            temporaryData($dataMember, $cardName);
                        }
                        //jeremy
                        if(strpos($manyMemberID, '559cce487e609257b9b668e8') !== false){
                            $dataMember = "<@U59LLD0JE>";
                            temporaryData($dataMember, $cardName);
                        }
                        //rex
                        if(strpos($manyMemberID, '5c3bf7a26814db37f6b41ff2') !== false){
                            $dataMember = "<@UFC03D588>";
                            temporaryData($dataMember, $cardName);
                        }
                        //leeYing
                        if(strpos($manyMemberID, '590c33ec597ce7d26734f355') !== false){
                            $dataMember = "<@U591PGRJ4>";
                            temporaryData($dataMember, $cardName);
                        }
                        //shah
                        if(strpos($manyMemberID, '58acfc97f4b21ebff852f886') !== false){
                            $dataMember = "<@U59L90G66>";
                            temporaryData($dataMember, $cardName);
                        }
                        //mrs. suhana
                        if(strpos($manyMemberID, '58abdb1a88c1ba56168f5d9e') !== false){
                            $dataMember = "<@U59MSGE12>";
                            temporaryData($dataMember, $cardName);
                        }
                    }
                } else {
                    echo "data not found <br>";
                }
            }
        }
        $i++;
    }
}

//get data from Operation list
function GetDataOperationList(){
    $devListID  = '5c3308a05bf0e452fb5f1480';
    $key        = 'cee15f8c5b0d25a32783f5139b64edb4';
    $token      = '0a9207e773917d4738910c973994011eea5bc955f93e59cdff9d1b7f1ab8776d';
    $APIURI     = 'https://api.trello.com/1/lists/'.$devListID.'/cards?cards=visible&card_member_fields=all&key='.$key.'&token='.$token;

    $init       = curl_init();
    curl_setopt($init, CURLOPT_URL, $APIURI); //Url together with parameters
    curl_setopt($init, CURLOPT_RETURNTRANSFER, 1); //Return data instead printing directly in Browser
    curl_setopt($init, CURLOPT_CONNECTTIMEOUT , 7); //Timeout after 7 seconds
    curl_setopt($init, CURLOPT_USERAGENT , "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
    curl_setopt($init, CURLOPT_HEADER, 0);

    $exec       = curl_exec($init);
    $close      = curl_close($init);

    $decodeJSON = json_decode($exec, true);
    $countJsonArray  = count($decodeJSON);
    
    $i = 0;
    while($i < $countJsonArray){
        // echo "data - ".$i.'<br>';
        foreach($decodeJSON[$i]['labels'] as $labelsData){
            //data  from labels array
            $labelName      = $labelsData['name'];
            $labelID        = $labelsData['id'];
            $labelColor     = $labelsData['color'];
            
            //data from card json return
            $cardName       = $decodeJSON[$i]['name'];
            $cardID         = $decodeJSON[$i]['id'];
            $cardURL        = $decodeJSON[$i]['url'];
            $memberID       = $decodeJSON[$i]['idMembers'];

            //fetch memberID array
            if(count($memberID) == 1){
                $memberIDs  = $memberID[0];
                
            } else {
                $memberIDs1 = $memberID[0];
                $memberIDs2 = $memberID[1];
                $memberIDs3 = $memberID[2];
                $memberIDs4 = $memberID[3];
                $memberIDs5 = $memberID[4];
                $memberIDs6 = $memberID[5];
                $memberIDs7 = $memberID[6];
                $memberIDs8 = $memberID[7];
                $memberIDs9 = $memberID[8];
                $memberIDs10 = $memberID[9];

                $temp       = [$memberIDs1, $memberIDs2, $memberIDs3, $memberIDs4, $memberIDs5, $memberIDs6, $memberIDs7, $memberIDs8, $memberIDs9, $memberIDs10];
                $memberIDs  = implode(', ', $temp);
            }
            
            if(strpos($labelName, 'Day 7+') !== false){
                //date setting
                date_default_timezone_set('Asia/Kuala_Lumpur');
                $createdAt      = date('Y-m-d H:i:s');
                $lastActivityRaw= date('Y-m-d');
                $date_now       = new DateTime();
                
                //connect to database
                global $mysqli;
                $mysqli = mysqli_connect('localhost', 'saraismi_arief', 'kaigan2019', 'saraismi_trelloSlackDayCounter');
                            
                //select data from db
                $sql_select             = "SELECT * FROM notification_7plus WHERE id_card='$cardID'";
                $query_select           = mysqli_query($mysqli, $sql_select);
                //if data found
                if(mysqli_num_rows($query_select) > 0){
                    $sqlUpdate          = "UPDATE notification_7plus SET id_member='$memberIDs', card_name='$cardName', card_URL='$cardURL', created_at='$createdAt' WHERE id_card='$cardID'";
                    $query              = mysqli_query($mysqli, $sqlUpdate);
                } else {
                    $sql                = "INSERT INTO notification_7plus (id_card, id_member, card_name, card_URL, created_at) 
                                            VALUES ('$cardID', '$memberIDs', '$cardName', '$cardURL', '$createdAt')";
                    $query              = mysqli_query($mysqli, $sql);
                }

                //select created_at from db
                $getData            = "SELECT * FROM notification_7plus WHERE id_card='$cardID'";
                $query_exec         = mysqli_query($mysqli, $getData);
                //if data found
                if(mysqli_num_rows($query_exec) > 0){
                    foreach($query_exec as $results){
                        $manyMemberID   = $results['id_member'];

                        //check per IDs
                        //arief
                        if(strpos($manyMemberID, '5959c36a55c57339a774ff8c') !== false){
                            $dataMember = "<@U62TFQL1Y>";
                            temporaryData($dataMember, $cardName);
                        }
                        //azmi
                        if(strpos($manyMemberID, '58b66821144d4b68a5ab1c84') !== false){
                            $dataMember = "<@U5A2WEHEH>";
                            temporaryData($dataMember, $cardName);
                        }
                        //alex
                        if(strpos($manyMemberID, '5b70ef61add7a23d75e11a44') !== false){
                            $dataMember = "<@U9DV9JUET>";
                            temporaryData($dataMember, $cardName);
                        }
                        //leeJenLing
                        if(strpos($manyMemberID, '5c3bf62e0581ed89ee7b6580') !== false){
                            $dataMember = "<@UFCJUGKDH>";
                            temporaryData($dataMember, $cardName);
                        }
                        //priya
                        if(strpos($manyMemberID, '5b70f173191434079ebf6420') !== false){
                            $dataMember = "<@UCGKY4P2T>";
                            temporaryData($dataMember, $cardName);
                        }
                        //jeremy
                        if(strpos($manyMemberID, '559cce487e609257b9b668e8') !== false){
                            $dataMember = "<@U59LLD0JE>";
                            temporaryData($dataMember, $cardName);
                        }
                        //rex
                        if(strpos($manyMemberID, '5c3bf7a26814db37f6b41ff2') !== false){
                            $dataMember = "<@UFC03D588>";
                            temporaryData($dataMember, $cardName);
                        }
                        //leeYing
                        if(strpos($manyMemberID, '590c33ec597ce7d26734f355') !== false){
                            $dataMember = "<@U591PGRJ4>";
                            temporaryData($dataMember, $cardName);
                        }
                        //shah
                        if(strpos($manyMemberID, '58acfc97f4b21ebff852f886') !== false){
                            $dataMember = "<@U59L90G66>";
                            temporaryData($dataMember, $cardName);
                        }
                        //mrs. suhana
                        if(strpos($manyMemberID, '58abdb1a88c1ba56168f5d9e') !== false){
                            $dataMember = "<@U59MSGE12>";
                            temporaryData($dataMember, $cardName);
                        }
                    }
                } else {
                    echo "data not found <br>";
                }
            }
        }
        $i++;
    }
}

//get data from Marketing list
function GetDataMarketingList(){
    $devListID  = '5c3308aa6fe4f68a28703bfa';
    $key        = 'cee15f8c5b0d25a32783f5139b64edb4';
    $token      = '0a9207e773917d4738910c973994011eea5bc955f93e59cdff9d1b7f1ab8776d';
    $APIURI     = 'https://api.trello.com/1/lists/'.$devListID.'/cards?cards=visible&card_member_fields=all&key='.$key.'&token='.$token;

    $init       = curl_init();
    curl_setopt($init, CURLOPT_URL, $APIURI); //Url together with parameters
    curl_setopt($init, CURLOPT_RETURNTRANSFER, 1); //Return data instead printing directly in Browser
    curl_setopt($init, CURLOPT_CONNECTTIMEOUT , 7); //Timeout after 7 seconds
    curl_setopt($init, CURLOPT_USERAGENT , "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
    curl_setopt($init, CURLOPT_HEADER, 0);

    $exec       = curl_exec($init);
    $close      = curl_close($init);

    $decodeJSON = json_decode($exec, true);
    $countJsonArray  = count($decodeJSON);
    
    $i = 0;
    while($i < $countJsonArray){
        // echo "data - ".$i.'<br>';
        foreach($decodeJSON[$i]['labels'] as $labelsData){
            //data  from labels array
            $labelName      = $labelsData['name'];
            $labelID        = $labelsData['id'];
            $labelColor     = $labelsData['color'];
            
            //data from card json return
            $cardName       = $decodeJSON[$i]['name'];
            $cardID         = $decodeJSON[$i]['id'];
            $cardURL        = $decodeJSON[$i]['url'];
            $memberID       = $decodeJSON[$i]['idMembers'];

            //fetch memberID array
            if(count($memberID) == 1){
                $memberIDs  = $memberID[0];
                
            } else {
                $memberIDs1 = $memberID[0];
                $memberIDs2 = $memberID[1];
                $memberIDs3 = $memberID[2];
                $memberIDs4 = $memberID[3];
                $memberIDs5 = $memberID[4];
                $memberIDs6 = $memberID[5];
                $memberIDs7 = $memberID[6];
                $memberIDs8 = $memberID[7];
                $memberIDs9 = $memberID[8];
                $memberIDs10 = $memberID[9];

                $temp       = [$memberIDs1, $memberIDs2, $memberIDs3, $memberIDs4, $memberIDs5, $memberIDs6, $memberIDs7, $memberIDs8, $memberIDs9, $memberIDs10];
                $memberIDs  = implode(', ', $temp);
            }
            
            if(strpos($labelName, 'Day 7+') !== false){
                //date setting
                date_default_timezone_set('Asia/Kuala_Lumpur');
                $createdAt      = date('Y-m-d H:i:s');
                $lastActivityRaw= date('Y-m-d');
                $date_now       = new DateTime();
                
                //connect to database
                global $mysqli;
                $mysqli = mysqli_connect('localhost', 'saraismi_arief', 'kaigan2019', 'saraismi_trelloSlackDayCounter');
                            
                //select data from db
                $sql_select             = "SELECT * FROM notification_7plus WHERE id_card='$cardID'";
                $query_select           = mysqli_query($mysqli, $sql_select);
                //if data found
                if(mysqli_num_rows($query_select) > 0){
                    $sqlUpdate          = "UPDATE notification_7plus SET id_member='$memberIDs', card_name='$cardName', card_URL='$cardURL', created_at='$createdAt' WHERE id_card='$cardID'";
                    $query              = mysqli_query($mysqli, $sqlUpdate);
                } else {
                    $sql                = "INSERT INTO notification_7plus (id_card, id_member, card_name, card_URL, created_at) 
                                            VALUES ('$cardID', '$memberIDs', '$cardName', '$cardURL', '$createdAt')";
                    $query              = mysqli_query($mysqli, $sql);
                }

                //select created_at from db
                $getData            = "SELECT * FROM notification_7plus WHERE id_card='$cardID'";
                $query_exec         = mysqli_query($mysqli, $getData);
                //if data found
                if(mysqli_num_rows($query_exec) > 0){
                    foreach($query_exec as $results){
                        $manyMemberID   = $results['id_member'];

                        //check per IDs
                        //arief
                        if(strpos($manyMemberID, '5959c36a55c57339a774ff8c') !== false){
                            $dataMember = "<@U62TFQL1Y>";
                            temporaryData($dataMember, $cardName);
                        }
                        //azmi
                        if(strpos($manyMemberID, '58b66821144d4b68a5ab1c84') !== false){
                            $dataMember = "<@U5A2WEHEH>";
                            temporaryData($dataMember, $cardName);
                        }
                        //alex
                        if(strpos($manyMemberID, '5b70ef61add7a23d75e11a44') !== false){
                            $dataMember = "<@U9DV9JUET>";
                            temporaryData($dataMember, $cardName);
                        }
                        //leeJenLing
                        if(strpos($manyMemberID, '5c3bf62e0581ed89ee7b6580') !== false){
                            $dataMember = "<@UFCJUGKDH>";
                            temporaryData($dataMember, $cardName);
                        }
                        //priya
                        if(strpos($manyMemberID, '5b70f173191434079ebf6420') !== false){
                            $dataMember = "<@UCGKY4P2T>";
                            temporaryData($dataMember, $cardName);
                        }
                        //jeremy
                        if(strpos($manyMemberID, '559cce487e609257b9b668e8') !== false){
                            $dataMember = "<@U59LLD0JE>";
                            temporaryData($dataMember, $cardName);
                        }
                        //rex
                        if(strpos($manyMemberID, '5c3bf7a26814db37f6b41ff2') !== false){
                            $dataMember = "<@UFC03D588>";
                            temporaryData($dataMember, $cardName);
                        }
                        //leeYing
                        if(strpos($manyMemberID, '590c33ec597ce7d26734f355') !== false){
                            $dataMember = "<@U591PGRJ4>";
                            temporaryData($dataMember, $cardName);
                        }
                        //shah
                        if(strpos($manyMemberID, '58acfc97f4b21ebff852f886') !== false){
                            $dataMember = "<@U59L90G66>";
                            temporaryData($dataMember, $cardName);
                        }
                        //mrs. suhana
                        if(strpos($manyMemberID, '58abdb1a88c1ba56168f5d9e') !== false){
                            $dataMember = "<@U59MSGE12>";
                            temporaryData($dataMember, $cardName);
                        }
                    }
                } else {
                    echo "data not found <br>";
                }
            }
        }
        $i++;
    }
}

function temporaryData($dataMember,$cardName){
    $dataNotification  = $dataMember.' -> '.$cardName;
    // echo $dataNotification;
    callNotification($dataNotification);
}

function callNotification($dataNotification){
    // echo 'data notification is: '.$dataNotification;
    $webhookURL         = 'https://hooks.slack.com/services/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';
    //data for notification
    $channel            = '#automation';
    $bot_name           = 'Kaigan Taskmaster';
    $icon               = ':alien:';
    $attachment         = array([
                            'color'     => '#36a64f',
                            "pretext"   => "*The following persons are spending too much time on some tasks, please assist*",  
                            'text'      => "$dataNotification",
                            'mrkdwn'    =>  true
                        ]);
    
    $data = array(
        'channel'     => $channel,
        'username'    => $bot_name,
        'icon_emoji'  => $icon,
        'attachments' => $attachment
    );
    
    $data_string = json_encode($data);
    
    $ch = curl_init($webhookURL);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($data_string))
    );
    //Execute CURL
    $result = curl_exec($ch);
    curl_close($ch);
}

//main function
function callTheFunctionNow(){
    GetDataDevelopmentList();
    GetDataGameDesignList();
    GetDataWritingList();
    GetDataArtList();
    GetDataSoundList();
    GetDataOperationList();
    GetDataMarketingList();
}

//call main function
callTheFunctionNow();
?>