<?php
error_reporting(0);
//okay ;)
function getDataFromDevelopmentList(){    
    $devListID  = '5c3308c14360d00d5e72d78d';
    $key        = 'cee15f8c5b0d25a32783f5139b64edb4';
    $token      = '0a9207e773917d4738910c973994011eea5bc955f93e59cdff9d1b7f1ab8776d';
    $APIURI     = 'https://api.trello.com/1/lists/'.$devListID.'/cards?cards=visible&card_member_fields=all&key='.$key.'&token='.$token;

    $init       = curl_init();
    curl_setopt($init, CURLOPT_URL, $APIURI); //Url together with parameters
    curl_setopt($init, CURLOPT_RETURNTRANSFER, 1); //Return data instead printing directly in Browser
    curl_setopt($init, CURLOPT_CONNECTTIMEOUT , 7); //Timeout after 7 seconds
    curl_setopt($init, CURLOPT_USERAGENT , "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
    curl_setopt($init, CURLOPT_HEADER, 0);

    $exec       = curl_exec($init);
    $close      = curl_close($init);

    $decodeJSON = json_decode($exec, true);
    $countJsonArray  = count($decodeJSON);
    // echo $countJsonArray;

    for($i=0; $i<$countJsonArray; $i++){
        // echo "data counter -".$i.'<br>';

        foreach($decodeJSON[$i]['labels'] as $labelsData){
            $dataLabel      = $labelsData['name'];
            if(strpos($dataLabel, 'WIP') !== false){
                $cardName   = $decodeJSON[$i]['name'];
                $memberID   = $decodeJSON[$i]['idMembers'];
                
                if(count($memberID) == 1){
                    $dataMembers    = $memberID[0];
                    if(strpos($dataMembers, '5959c36a55c57339a774ff8c') !== false){
                        $data_arief = "arief";
                    }
                    if(strpos($dataMembers, '58b66821144d4b68a5ab1c84') !== false){
                        $data_azmi = "azmi";
                    }
                    if(strpos($dataMembers, '5b70ef61add7a23d75e11a44') !== false){
                        $data_alex = "alex";
                    }
                    if(strpos($dataMembers, '5c3bf62e0581ed89ee7b6580') !== false){
                        $data_leeJenLing = "leeJenLing";
                    }
                } else {
                    $id_member1 = $memberID[0];
                    $id_member2 = $memberID[1];
                    $id_member3 = $memberID[2];
                    $id_member4 = $memberID[3];
                    $id_member5 = $memberID[4];
                    $id_member6 = $memberID[5];
                    $id_member7 = $memberID[6];
                    $id_member8 = $memberID[7];
                    $id_member9 = $memberID[8];
                    $id_member10 = $memberID[9];

                    //arief
                    if($id_member1 == '5959c36a55c57339a774ff8c' || $id_member2 == '5959c36a55c57339a774ff8c' || $id_member3 == '5959c36a55c57339a774ff8c' || $id_member4 == '5959c36a55c57339a774ff8c'
                    || $id_member5 == '5959c36a55c57339a774ff8c' || $id_member6 == '5959c36a55c57339a774ff8c' || $id_member7 == '5959c36a55c57339a774ff8c' || $id_member8 == '5959c36a55c57339a774ff8c'
                    || $id_member9 == '5959c36a55c57339a774ff8c' || $id_member10 == '5959c36a55c57339a774ff8c'){
                        $data_arief = "arief";
                    } 

                    //azmi
                    if($id_member1 == '58b66821144d4b68a5ab1c84' || $id_member2 == '58b66821144d4b68a5ab1c84' || $id_member3 == '58b66821144d4b68a5ab1c84' || $id_member4 == '58b66821144d4b68a5ab1c84'
                    || $id_member5 == '58b66821144d4b68a5ab1c84' || $id_member6 == '58b66821144d4b68a5ab1c84' || $id_member7 == '58b66821144d4b68a5ab1c84' || $id_member8 == '58b66821144d4b68a5ab1c84'
                    || $id_member9 == '58b66821144d4b68a5ab1c84' || $id_member10 == '58b66821144d4b68a5ab1c84'){
                        $data_azmi = "azmi";
                    } 

                    //alex
                    if($id_member1 == '5b70ef61add7a23d75e11a44' || $id_member2 == '5b70ef61add7a23d75e11a44' || $id_member3 == '5b70ef61add7a23d75e11a44' || $id_member4 == '5b70ef61add7a23d75e11a44'
                    || $id_member5 == '5b70ef61add7a23d75e11a44' || $id_member6 == '5b70ef61add7a23d75e11a44' || $id_member7 == '5b70ef61add7a23d75e11a44' || $id_member8 == '5b70ef61add7a23d75e11a44'
                    || $id_member9 == '5b70ef61add7a23d75e11a44' || $id_member10 == '5b70ef61add7a23d75e11a44'){
                        $data_alex = "alex";
                    } 

                    //lee jen ling
                    if($id_member1 == '5c3bf62e0581ed89ee7b6580' || $id_member2 == '5c3bf62e0581ed89ee7b6580' || $id_member3 == '5c3bf62e0581ed89ee7b6580' || $id_member4 == '5c3bf62e0581ed89ee7b6580'
                    || $id_member5 == '5c3bf62e0581ed89ee7b6580' || $id_member6 == '5c3bf62e0581ed89ee7b6580' || $id_member7 == '5c3bf62e0581ed89ee7b6580' || $id_member8 == '5c3bf62e0581ed89ee7b6580'
                    || $id_member9 == '5c3bf62e0581ed89ee7b6580' || $id_member10 == '5c3bf62e0581ed89ee7b6580'){
                        $data_leeJenLing = "leeJenLing";
                    } 
                }
                // echo $cardName.' and '.$memberID[0].'<br>';
            }
            $data1  = $data_arief;
            $data2  = $data_azmi;
            $data3  = $data_alex;
            $data4  = $data_leeJenLing;
        }
    }

    if($data1 != "arief"){
        $to = 'arief';
        $idMember   = '5959c36a55c57339a774ff8c';
        checkMembers($to, $idMember);
    }
    if($data2 != "azmi"){
        $to = 'azmi';
        $idMember   = '58b66821144d4b68a5ab1c84';
        checkMembers($to, $idMember);
    }
    if($data3 != "alex"){
        $to = 'alex';
        $idMember   = '5b70ef61add7a23d75e11a44';
        checkMembers($to, $idMember);
    }
    if($data4 != "leeJenLing"){
        $to = 'leeJenLing';
        $idMember   = '5c3bf62e0581ed89ee7b6580';
        checkMembers($to, $idMember);
    }
    if(($data1 == "arief") && ($data2 == "azmi") && ($data3 == "alex") && ($data4 == "leeJenLing")){
        echo "all dev team working";
    }
}

//okay ;)
function getDataFromGameDesignList(){    
    $devListID  = '5bceba7f62850a31fb5160dc';
    $key        = 'cee15f8c5b0d25a32783f5139b64edb4';
    $token      = '0a9207e773917d4738910c973994011eea5bc955f93e59cdff9d1b7f1ab8776d';
    $APIURI     = 'https://api.trello.com/1/lists/'.$devListID.'/cards?cards=visible&card_member_fields=all&key='.$key.'&token='.$token;

    $init       = curl_init();
    curl_setopt($init, CURLOPT_URL, $APIURI); //Url together with parameters
    curl_setopt($init, CURLOPT_RETURNTRANSFER, 1); //Return data instead printing directly in Browser
    curl_setopt($init, CURLOPT_CONNECTTIMEOUT , 7); //Timeout after 7 seconds
    curl_setopt($init, CURLOPT_USERAGENT , "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
    curl_setopt($init, CURLOPT_HEADER, 0);

    $exec       = curl_exec($init);
    $close      = curl_close($init);

    $decodeJSON = json_decode($exec, true);
    $countJsonArray  = count($decodeJSON);
    // echo $countJsonArray;

    for($i=0; $i<$countJsonArray; $i++){
        // echo "data counter -".$i.'<br>';
        foreach($decodeJSON[$i]['labels'] as $labelsData){
            $dataLabel      = $labelsData['name'];
            if(strpos($dataLabel, 'WIP') !== false){
                $cardName   = $decodeJSON[$i]['name'];
                $memberID   = $decodeJSON[$i]['idMembers'];
                
                if(count($memberID) == 1){
                    $dataMembers    = $memberID[0];
                    if(strpos($dataMembers, '559cce487e609257b9b668e8') !== false){
                        $data_jeremy = "jeremy";
                    }
                }
                // echo $cardName.' and '.$memberID[0].'<br>';
            }
            $data1  = $data_jeremy;            
        }
    }

    if($data1 != "jeremy"){
        $to = 'jeremy';
        $idMember   = '559cce487e609257b9b668e8';
        checkMembers($to, $idMember);
    }
}

//okay ;)
function getDataFromWritingList(){
    $devListID  = '5bceba7f62850a31fb5160de';
    $key        = 'cee15f8c5b0d25a32783f5139b64edb4';
    $token      = '0a9207e773917d4738910c973994011eea5bc955f93e59cdff9d1b7f1ab8776d';
    $APIURI     = 'https://api.trello.com/1/lists/'.$devListID.'/cards?cards=visible&card_member_fields=all&key='.$key.'&token='.$token;

    $init       = curl_init();
    curl_setopt($init, CURLOPT_URL, $APIURI); //Url together with parameters
    curl_setopt($init, CURLOPT_RETURNTRANSFER, 1); //Return data instead printing directly in Browser
    curl_setopt($init, CURLOPT_CONNECTTIMEOUT , 7); //Timeout after 7 seconds
    curl_setopt($init, CURLOPT_USERAGENT , "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
    curl_setopt($init, CURLOPT_HEADER, 0);

    $exec       = curl_exec($init);
    $close      = curl_close($init);

    $decodeJSON = json_decode($exec, true);
    $countJsonArray  = count($decodeJSON);
    // echo $countJsonArray;

    for($i=0; $i<$countJsonArray; $i++){
        // echo "data counter -".$i.'<br>';
        foreach($decodeJSON[$i]['labels'] as $labelsData){
            $dataLabel      = $labelsData['name'];
            if(strpos($dataLabel, 'WIP') !== false){
                $cardName   = $decodeJSON[$i]['name'];
                $memberID   = $decodeJSON[$i]['idMembers'];
                
                if(count($memberID) == 1){
                    $dataMembers    = $memberID[0];
                    if(strpos($dataMembers, '5b70f173191434079ebf6420') !== false){
                        $data_priya = "priya";
                    }
                }
                // echo $cardName.' and '.$memberID[0].'<br>';
            }
            $data1  = $data_priya;            
        }
    }

    if($data1 != "priya"){
        $to = 'priya';
        $idMember   = '5b70f173191434079ebf6420';
        checkMembers($to, $idMember);
    }
}

//okay ;)
function getDataFromArtList(){    
    $devListID  = '5bceba7f62850a31fb5160dd';
    $key        = 'cee15f8c5b0d25a32783f5139b64edb4';
    $token      = '0a9207e773917d4738910c973994011eea5bc955f93e59cdff9d1b7f1ab8776d';
    $APIURI     = 'https://api.trello.com/1/lists/'.$devListID.'/cards?cards=visible&card_member_fields=all&key='.$key.'&token='.$token;

    $init       = curl_init();
    curl_setopt($init, CURLOPT_URL, $APIURI); //Url together with parameters
    curl_setopt($init, CURLOPT_RETURNTRANSFER, 1); //Return data instead printing directly in Browser
    curl_setopt($init, CURLOPT_CONNECTTIMEOUT , 7); //Timeout after 7 seconds
    curl_setopt($init, CURLOPT_USERAGENT , "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
    curl_setopt($init, CURLOPT_HEADER, 0);

    $exec       = curl_exec($init);
    $close      = curl_close($init);

    $decodeJSON = json_decode($exec, true);
    $countJsonArray  = count($decodeJSON);
    // echo $countJsonArray;

    for($i=0; $i<$countJsonArray; $i++){
        // echo "data counter -".$i.'<br>';

        foreach($decodeJSON[$i]['labels'] as $labelsData){
            $dataLabel      = $labelsData['name'];
            if(strpos($dataLabel, 'WIP') !== false){
                $cardName   = $decodeJSON[$i]['name'];
                $memberID   = $decodeJSON[$i]['idMembers'];
                
                if(count($memberID) == 1){
                    $dataMembers    = $memberID[0];
                    if(strpos($dataMembers, '5c3bf7a26814db37f6b41ff2') !== false){
                        $data_rex = "rex";
                    }
                    if(strpos($dataMembers, '590c33ec597ce7d26734f355') !== false){
                        $data_leeYing = "leeYing";
                    }
                } else {
                    $id_member1 = $memberID[0];
                    $id_member2 = $memberID[1];
                    $id_member3 = $memberID[2];
                    $id_member4 = $memberID[3];
                    $id_member5 = $memberID[4];
                    $id_member6 = $memberID[5];
                    $id_member7 = $memberID[6];
                    $id_member8 = $memberID[7];
                    $id_member9 = $memberID[8];
                    $id_member10 = $memberID[9];

                    //rex
                    if($id_member1 == '5c3bf7a26814db37f6b41ff2' || $id_member2 == '5c3bf7a26814db37f6b41ff2' || $id_member3 == '5c3bf7a26814db37f6b41ff2' || $id_member4 == '5c3bf7a26814db37f6b41ff2'
                    || $id_member5 == '5c3bf7a26814db37f6b41ff2' || $id_member6 == '5c3bf7a26814db37f6b41ff2' || $id_member7 == '5c3bf7a26814db37f6b41ff2' || $id_member8 == '5c3bf7a26814db37f6b41ff2'
                    || $id_member9 == '5c3bf7a26814db37f6b41ff2' || $id_member10 == '5c3bf7a26814db37f6b41ff2'){
                        $data_rex = "rex";
                    } 

                    //leeYing
                    if($id_member1 == '590c33ec597ce7d26734f355' || $id_member2 == '590c33ec597ce7d26734f355' || $id_member3 == '590c33ec597ce7d26734f355' || $id_member4 == '590c33ec597ce7d26734f355'
                    || $id_member5 == '590c33ec597ce7d26734f355' || $id_member6 == '590c33ec597ce7d26734f355' || $id_member7 == '590c33ec597ce7d26734f355' || $id_member8 == '590c33ec597ce7d26734f355'
                    || $id_member9 == '590c33ec597ce7d26734f355' || $id_member10 == '590c33ec597ce7d26734f355'){
                        $data_leeYing = "leeYing";
                    } 
                }
                // echo $cardName.' and '.$memberID[0].'<br>';
            }
            $data1  = $data_rex;
            $data2  = $data_leeYing;
        }
    }

    if($data1 != "rex"){
        $to = 'rex';
        $idMember   = '5c3bf7a26814db37f6b41ff2';
        checkMembers($to, $idMember);
    }
    if($data2 != "leeYing"){
        $to = 'leeYing';
        $idMember   = '590c33ec597ce7d26734f355';
        checkMembers($to, $idMember);
    }
    if(($data1 == "rex") && ($data2 == "leeYing")){
        echo "all art team working";
    }
}

//okay ;)
function getDataFromMarketingList(){    
    $devListID  = '5c3308aa6fe4f68a28703bfa';
    $key        = 'cee15f8c5b0d25a32783f5139b64edb4';
    $token      = '0a9207e773917d4738910c973994011eea5bc955f93e59cdff9d1b7f1ab8776d';
    $APIURI     = 'https://api.trello.com/1/lists/'.$devListID.'/cards?cards=visible&card_member_fields=all&key='.$key.'&token='.$token;

    $init       = curl_init();
    curl_setopt($init, CURLOPT_URL, $APIURI); //Url together with parameters
    curl_setopt($init, CURLOPT_RETURNTRANSFER, 1); //Return data instead printing directly in Browser
    curl_setopt($init, CURLOPT_CONNECTTIMEOUT , 7); //Timeout after 7 seconds
    curl_setopt($init, CURLOPT_USERAGENT , "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
    curl_setopt($init, CURLOPT_HEADER, 0);

    $exec       = curl_exec($init);
    $close      = curl_close($init);

    $decodeJSON = json_decode($exec, true);
    $countJsonArray  = count($decodeJSON);
    // echo $countJsonArray;

    for($i=0; $i<$countJsonArray; $i++){
        // echo "data counter -".$i.'<br>';
        foreach($decodeJSON[$i]['labels'] as $labelsData){
            $dataLabel      = $labelsData['name'];
            if(strpos($dataLabel, 'WIP') !== false){
                $cardName   = $decodeJSON[$i]['name'];
                $memberID   = $decodeJSON[$i]['idMembers'];
                
                if(count($memberID) == 1){
                    $dataMembers    = $memberID[0];
                    if(strpos($dataMembers, '58acfc97f4b21ebff852f886') !== false){
                        $data_shah = "shah";
                    }
                }
                // echo $cardName.' and '.$memberID[0].'<br>';
            }
            $data1  = $data_shah;            
        }
    }

    if($data1 != "shah"){
        $to = 'shah';
        $idMember   = '58acfc97f4b21ebff852f886';
        checkMembers($to, $idMember);
    }
}

function checkMembers($to, $idMember){
    // echo $to.'<br>';
    if($to == 'arief'){
        $webhookURL_Arief   = 'https://hooks.slack.com/services/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';
        //data for notification
        $channel            = '#automation';
        $bot_name           = 'Kaigan Taskmaster';
        $icon               = ':alien:';
        $attachments        = array([
            'color'     => '#36a64f',        
            'text'      => "*<@U62TFQL1Y> is not working on anything.*\n_Please choose your next task and assign the WIP label_",
            'mrkdwn'    =>  true
        ]);

        $data = array(
            'channel'     => $channel,
            'username'    => $bot_name,
            'icon_emoji'  => $icon,
            'attachments' => $attachments
        );

        $data_string = json_encode($data);

        $ch = curl_init($webhookURL_Arief);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string))
        );
        //Execute CURL
        $result = curl_exec($ch);
        curl_close($ch);
        // echo "send to arief <br>";
    } else if($to == 'azmi'){
        $webhookURL_Azmi    = 'https://hooks.slack.com/services/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';
        //data for notification
        $channel            = '#automation';
        $bot_name           = 'Kaigan Taskmaster';
        $icon               = ':alien:';
        $attachments        = array([
            'color'    => '#36a64f',
            'text'     => "*<@U5A2WEHEH>, is not working on anything.*\n_Please choose your next task and assign the WIP label_",
        ]);

        $data = array(
            'channel'     => $channel,
            'username'    => $bot_name,
            'icon_emoji'  => $icon,
            'attachments' => $attachments
        );

        $data_string = json_encode($data);
        $ch = curl_init($webhookURL_Azmi);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
            );
        //Execute CURL
        $result = curl_exec($ch);
        curl_close($ch);
        // echo "send to azmi <br>";
    } else if($to == 'alex'){
        $webhookURL_Alex    = 'https://hooks.slack.com/services/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';
        //data for notification
        $channel            = '#automation';
        $bot_name           = 'Kaigan Taskmaster';
        $icon               = ':alien:';
        $attachments        = array([
            'color'    => '#36a64f',
            'text'     => "*<@U9DV9JUET>, is not working on anything.*\n_Please choose your next task and assign the WIP label_",
        ]);

        $data = array(
            'channel'     => $channel,
            'username'    => $bot_name,
            'icon_emoji'  => $icon,
            'attachments' => $attachments
        );

        $data_string = json_encode($data);
        $ch = curl_init($webhookURL_Alex);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
            );
        //Execute CURL
        $result = curl_exec($ch);
        curl_close($ch);
        // echo "send to alex <br>";
    } else if($to == 'leeJenLing'){
        $webhookURL_leeJL    = 'https://hooks.slack.com/services/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';
        //data for notification
        $channel            = '#automation';
        $bot_name           = 'Kaigan Taskmaster';
        $icon               = ':alien:';
        $attachments        = array([
            'color'    => '#36a64f',
            'text'     => "*<@UFCJUGKDH>, is not working on anything.*\n_Please choose your next task and assign the WIP label_",
        ]);

        $data = array(
            'channel'     => $channel,
            'username'    => $bot_name,
            'icon_emoji'  => $icon,
            'attachments' => $attachments
        );

        $data_string = json_encode($data);
        $ch = curl_init($webhookURL_leeJL);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
            );
        //Execute CURL
        $result = curl_exec($ch);
        curl_close($ch);
        // echo "send to lee jen ling <br>";
    } else if($to == 'jeremy'){
        $webhookURL_Jeremy  = 'https://hooks.slack.com/services/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';
        //data for notification
        $channel            = '#automation';
        $bot_name           = 'Kaigan Taskmaster';
        $icon               = ':alien:';
        $attachments        = array([
            'color'    => '#36a64f',
            'text'     => "*<@U59LLD0JE>, is not working on anything.*\n_Please choose your next task and assign the WIP label_",
        ]);

        $data = array(
            'channel'     => $channel,
            'username'    => $bot_name,
            'icon_emoji'  => $icon,
            'attachments' => $attachments
        );

        $data_string = json_encode($data);
        $ch = curl_init($webhookURL_Jeremy);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
            );
        //Execute CURL
        $result = curl_exec($ch);
        curl_close($ch);
        // echo "send to jeremy <br>";
    } else if($to == 'priya'){
        $webhookURL_Priya   = 'https://hooks.slack.com/services/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';
        //data for notification
        $channel            = '#automation';
        $bot_name           = 'Kaigan Taskmaster';
        $icon               = ':alien:';
        $attachments        = array([
            'color'    => '#36a64f',
            'text'     => "*<@UCGKY4P2T>, is not working on anything.*\n_Please choose your next task and assign the WIP label_",
        ]);

        $data = array(
            'channel'     => $channel,
            'username'    => $bot_name,
            'icon_emoji'  => $icon,
            'attachments' => $attachments
        );

        $data_string = json_encode($data);
        $ch = curl_init($webhookURL_Priya);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
            );
        //Execute CURL
        $result = curl_exec($ch);
        curl_close($ch);
        // echo "send to priya <br>";
    } else if($to == 'rex'){
        $webhookURL_Rex     = 'https://hooks.slack.com/services/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';
        //data for notification
        $channel            = '#automation';
        $bot_name           = 'Kaigan Taskmaster';
        $icon               = ':alien:';
        $attachments        = array([
            'color'    => '#36a64f',
            'text'     => "*<@UFC03D588>, is not working on anything.*\n_Please choose your next task and assign the WIP label_",
        ]);

        $data = array(
            'channel'     => $channel,
            'username'    => $bot_name,
            'icon_emoji'  => $icon,
            'attachments' => $attachments
        );

        $data_string = json_encode($data);
        $ch = curl_init($webhookURL_Rex);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
            );
        //Execute CURL
        $result = curl_exec($ch);
        curl_close($ch);
        // echo "send to rex <br>";
    } else if($to == 'leeYing'){
        $webhookURL_LeeYing   = 'https://hooks.slack.com/services/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';
        //data for notification
        $channel            = '#automation';
        $bot_name           = 'Kaigan Taskmaster';
        $icon               = ':alien:';
        $attachments        = array([
            'color'    => '#36a64f',
            'text'     => "*<@U591PGRJ4>, is not working on anything.*\n_Please choose your next task and assign the WIP label_",
        ]);

        $data = array(
            'channel'     => $channel,
            'username'    => $bot_name,
            'icon_emoji'  => $icon,
            'attachments' => $attachments
        );

        $data_string = json_encode($data);
        $ch = curl_init($webhookURL_LeeYing);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
            );
        //Execute CURL
        $result = curl_exec($ch);
        curl_close($ch);
        // echo "send to lee ying <br>";
    } else if($to == 'shah'){
        $webhookURL_Shah    = 'https://hooks.slack.com/services/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';
        //data for notification
        $channel            = '#automation';
        $bot_name           = 'Kaigan Taskmaster';
        $icon               = ':alien:';
        $attachments        = array([
            'color'    => '#36a64f',
            'text'     => "*<@U59L90G66>, is not working on anything.*\n_Please choose your next task and assign the WIP label_",
        ]);

        $data = array(
            'channel'     => $channel,
            'username'    => $bot_name,
            'icon_emoji'  => $icon,
            'attachments' => $attachments
        );

        $data_string = json_encode($data);
        $ch = curl_init($webhookURL_Shah);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
            );
        //Execute CURL
        $result = curl_exec($ch);
        curl_close($ch);
        // echo "send to shah <br>";
    } else if($to == "suhana"){
        // $webhookURL_Suhana   = 'https://hooks.slack.com/services/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';
        // //data for notification
        // $channel            = '#automation';
        // $bot_name           = 'Kaigan Taskmaster';
        // $icon               = ':alien:';
        // $attachments        = array([
        //     'color'    => '#36a64f',
        //     'text'     => "*<@U59MSGE12>, is not working on anything.*\n_Please choose your next task and assign the WIP label_",
        // ]);

        // $data = array(
        //     'channel'     => $channel,
        //     'username'    => $bot_name,
        //     'icon_emoji'  => $icon,
        //     'attachments' => $attachments
        // );

        // $data_string = json_encode($data);
        // $ch = curl_init($webhookURL_Suhana);
        //     curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        //     curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        //     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //     curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        //         'Content-Type: application/json',
        //         'Content-Length: ' . strlen($data_string))
        //     );
        // //Execute CURL
        // $result = curl_exec($ch);
        // curl_close($ch);
        echo "send to mrs. suhana <br>";
    } else {
        echo "who is this?";
    }
}

function mainFunction(){
    getDataFromDevelopmentList();
    getDataFromGameDesignList();
    getDataFromWritingList();
    getDataFromArtList();
    getDataFromMarketingList();
}

//call main function
mainFunction();
?>